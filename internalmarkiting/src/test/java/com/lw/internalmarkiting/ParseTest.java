package com.lw.internalmarkiting;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ParseTest {
    private static final String responseData = "{\"application\":[{\"url\":\"com.softtechnologiz.fakecallerid.prankcall\",\"image\":\"http:\\/\\/oxdeal.pk\\/\\/androidpromo\\/\\/images\\/\\/intertestials\\/\\/1579678626_322.jpg\",\"ad_id\":\"2\"},{\"url\":\"com.lpa.photoeditor.collagemaker\",\"image\":\"http:\\/\\/oxdeal.pk\\/\\/androidpromo\\/\\/images\\/\\/intertestials\\/\\/1579678626_322.jpg\",\"ad_id\":\"3\"}]}";
    private static final String APPLICATION = "application";
    private static final String AD_ID = "ad_id";
    private static final String IMAGE = "image";
    private static final String PACKAGE_NAME = "url";

    @Test
    public void main() throws Exception {
        JSONObject response = new JSONObject(responseData);
        println("Response : " + response);
        JSONArray array = response.getJSONArray(APPLICATION);

        for (int i = 0; i < array.length(); i++) {
            final JSONObject object = array.getJSONObject(i);
            println("JSONObject : " + object);

            String packageName = object.getString(PACKAGE_NAME);
            String imageUrl = object.getString(IMAGE);
            double adId = object.getDouble(AD_ID);

//            InterstitialAdModel data = new InterstitialAdModel(packageName, imageUrl, adId);
//            println(data.toString());
        }
    }

    public void streamTest(){

    }

    private void println(String msg) {
        System.out.println(msg + "\n");
    }
}