package com.lw.internalmarkiting.ui.view;

import com.google.android.gms.ads.formats.UnifiedNativeAd;

public interface NativeAdLoadListener {
    void onNativeAdLoaded(UnifiedNativeAd var1);
}