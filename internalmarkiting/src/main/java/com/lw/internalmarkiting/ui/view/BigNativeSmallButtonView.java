package com.lw.internalmarkiting.ui.view;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.ads.AdLoader;
import com.lw.internalmarkiting.AdsApp;
import com.lw.internalmarkiting.R;
import com.lw.internalmarkiting.ads.AdHelper;
import com.lw.internalmarkiting.ui.utils.AnimationUtils;

import static com.lw.internalmarkiting.BuildConfig.NATIVE_AD_ID;

public class BigNativeSmallButtonView extends FrameLayout {
    public BigNativeSmallButtonView(@NonNull Context context) {
        this(context, null);
    }

    public BigNativeSmallButtonView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BigNativeSmallButtonView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (isInEditMode()) {
            LayoutInflater.from(context).inflate(R.layout.layout_native_small_button, this, true);
            return;
        }
        if (AdsApp.enableAds)
            init(context);
    }

    private void init(Context context) {
        AdLoader adLoader = new AdLoader.Builder(context, NATIVE_AD_ID)
                .forUnifiedNativeAd(unifiedNativeAd -> {

                    View view = inflate(getContext(), R.layout.layout_native_small_button, null);

                    NativeAdTemplateView adTemplateView = view.findViewById(R.id.adView);

                    NativeTemplateStyle styles = new
                            NativeTemplateStyle.Builder().withMainBackgroundColor(
                            new ColorDrawable(Color.TRANSPARENT)
                    ).build();

                    adTemplateView.setStyles(styles);
                    adTemplateView.setNativeAd(unifiedNativeAd);

                    AnimationUtils.addChangeBoundAnimationToParent(this);
                    addView(view);
                }).build();

        adLoader.loadAd(AdHelper.getAdRequest());
    }
}