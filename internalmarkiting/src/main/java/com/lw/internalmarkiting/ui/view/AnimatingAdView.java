package com.lw.internalmarkiting.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.appcompat.widget.AppCompatImageView;

import com.lw.internalmarkiting.R;

public class AnimatingAdView extends AppCompatImageView {

    public AnimatingAdView(Context context) {
        this(context, null);
    }

    public AnimatingAdView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }


    public AnimatingAdView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        Animation zoomIn = AnimationUtils.loadAnimation(context, R.anim.zoomin);
        Animation zoomOut = AnimationUtils.loadAnimation(context, R.anim.zoom_out);
        startAnimation(zoomIn);
        zoomIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                startAnimation(zoomOut);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        zoomOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                startAnimation(zoomIn);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
}
