package com.lw.internalmarkiting.ui.activities

import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import androidx.databinding.ViewDataBinding

abstract class FullScreenActivity<T : ViewDataBinding> : BaseActivity<T>() {

    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        super.onCreate(savedInstanceState)
    }
}