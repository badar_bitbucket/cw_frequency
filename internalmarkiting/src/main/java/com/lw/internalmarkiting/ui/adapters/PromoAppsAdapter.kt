package com.lw.internalmarkiting.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lw.internalmarkiting.data.model.PromoApp
import com.lw.internalmarkiting.databinding.ItemPromoAppBinding
import java.util.*

class PromoAppsAdapter : RecyclerView.Adapter<PromoAppsAdapter.PromoAppViewHolder>() {

    private val list: MutableList<PromoApp> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PromoAppViewHolder {
        return PromoAppViewHolder(ItemPromoAppBinding.inflate(
                LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(promoHolder: PromoAppViewHolder, position: Int) {
        promoHolder.bindView(list[position])
    }

    override fun getItemCount() = list.size

    fun addPromoApp(promoApp: PromoApp) {
        list.apply {
            add(promoApp)
            notifyItemInserted(lastIndex)
        }
    }

    class PromoAppViewHolder(val binding: ItemPromoAppBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bindView(promoApp: PromoApp) {
            binding.promoApp = promoApp
        }
    }
}