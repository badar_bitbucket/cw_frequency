package com.lw.internalmarkiting.ui.activities;

import android.content.Intent;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;
import com.lw.internalmarkiting.AdsApp;
import com.lw.internalmarkiting.R;
import com.lw.internalmarkiting.ads.LwInterstitialHelper;
import com.lw.internalmarkiting.data.model.PromoApp;
import com.lw.internalmarkiting.databinding.ActivityInterstitialBinding;

import org.jetbrains.annotations.NotNull;

public class InterstitialActivity extends BaseActivity<ActivityInterstitialBinding> {

    public static void start() {
        AdsApp.getContext().startActivity(new Intent(AdsApp.getContext(), InterstitialActivity.class));
    }

    @Override
    protected void beforeSettingContent() {
        super.beforeSettingContent();
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setFinishOnTouchOutside(false);
    }

    @Override
    protected int getResId() {
        return R.layout.activity_interstitial;
    }

    @Override
    protected void onReady() {
        binding.setPromoApp(LwInterstitialHelper.getAd());
        binding.setClickHandler(new ClickHandler());
        binding.ivClose.setOnClickListener((View.OnClickListener) v -> close());
    }

    @Override
    public void onBackPressed() {
        close();
    }

    private void close() {
        LwInterstitialHelper.onClose();
        finish();
    }

    public static class ClickHandler {
        public void onClick(@NotNull PromoApp promoApp) {
            promoApp.onClick();
        }

        public void onAccountNameClicked(@NotNull PromoApp promoApp) {
            promoApp.onAccountNameClicked();
        }
    }
}
