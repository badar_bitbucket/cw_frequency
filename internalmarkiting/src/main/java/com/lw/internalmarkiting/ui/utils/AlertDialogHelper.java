package com.lw.internalmarkiting.ui.utils;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.lw.internalmarkiting.R;


public class AlertDialogHelper extends Dialog implements DialogInterface {
    private Context mContext;
    private TextView mTitle;
    private ImageView mIcon;
    private LinearLayout mContentView;
    private Button mPositiveButton;
    private Button mNegativeButton;
    private Button mNaturelButton;
    private LinearLayout mTitleParent;
    private LinearLayout mButtonParent;

    public AlertDialogHelper(Context context) {
        super(context, R.style.myDialogTheme);
        super.setContentView(R.layout.view_common_dialog);
        mContext = context;
        init();
    }

    private void init() {
        mTitleParent = this.findViewById(R.id.comm_dialog_title_parent);
        mButtonParent = this.findViewById(R.id.comm_dialog_bottom);
        mTitle = this.findViewById(R.id.comm_dialog_title);
        mIcon = this.findViewById(R.id.comm_dialog_icon);
        mContentView = this.findViewById(R.id.comm_dialog_content);
        mPositiveButton = this.findViewById(R.id.comm_dialog_positive_button);
        mNegativeButton = this.findViewById(R.id.comm_dialog_negative_button);
        mNaturelButton = this.findViewById(R.id.comm_dialog_naturel_button);
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle.setText(title);
        mTitleParent.setVisibility(View.VISIBLE);
    }

    @Override
    public void setTitle(int titleId) {
        mTitle.setText(titleId);
        mTitleParent.setVisibility(View.VISIBLE);
    }

    @Override
    public void setContentView(int layoutResID) {
        mContentView.removeAllViewsInLayout();
        mContentView.addView(LayoutInflater.from(mContext).inflate(layoutResID, null));
    }

    @Override
    public void setContentView(View view) {
        mContentView.addView(view);

        //mContentView.removeAllViewsInLayout();
        // mContentView.addView( view );
    }

    public void setPositiveButton(int textId,
                                  final OnClickListener listener) {
        mButtonParent.setVisibility(View.VISIBLE);
        mPositiveButton.setVisibility(View.VISIBLE);
        mPositiveButton.setText(textId);
        mPositiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onClick(AlertDialogHelper.this, mPositiveButton.getId());
                }
                AlertDialogHelper.this.dismiss();
            }
        });
    }

    public void setNaturlButton(String text,
                                final OnClickListener listener) {
        mNaturelButton.setVisibility(View.VISIBLE);
        mNaturelButton.setText(text);
        mNaturelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onClick(AlertDialogHelper.this, mPositiveButton.getId());
                }
                AlertDialogHelper.this.dismiss();
            }
        });
    }

    public void setPositiveButton(String textId,
                                  final OnClickListener listener) {
        mButtonParent.setVisibility(View.VISIBLE);
        mPositiveButton.setVisibility(View.VISIBLE);
        mPositiveButton.setText(textId);
        mPositiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onClick(AlertDialogHelper.this, mPositiveButton.getId());
                }
                AlertDialogHelper.this.dismiss();
            }
        });
    }

    public void setNegativeButton(int textId,
                                  final OnClickListener listener) {
        mButtonParent.setVisibility(View.VISIBLE);
        mNegativeButton.setVisibility(View.VISIBLE);
        mNegativeButton.setText(textId);
        mNegativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onClick(AlertDialogHelper.this, mNegativeButton.getId());
                }
                AlertDialogHelper.this.dismiss();
            }
        });
    }

    public void setNegativeButton(String text,
                                  final OnClickListener listener) {
        mButtonParent.setVisibility(View.VISIBLE);
        mNegativeButton.setVisibility(View.VISIBLE);
        mNegativeButton.setText(text);
        mNegativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onClick(AlertDialogHelper.this, mNegativeButton.getId());
                }
                AlertDialogHelper.this.dismiss();
            }
        });
    }

    public void setIcon(int iconId) {
        mIcon.setImageResource(iconId);
        mTitleParent.setVisibility(View.VISIBLE);
    }

    public void setIcon(Bitmap bm) {
        mIcon.setImageBitmap(bm);
        mTitleParent.setVisibility(View.VISIBLE);
    }

    public void setIcon(Drawable drawbale) {
        mIcon.setImageDrawable(drawbale);
        mTitleParent.setVisibility(View.VISIBLE);
    }

    public void setMessage(CharSequence message) {
        TextView textView = (TextView) LayoutInflater.from(mContext).inflate(
                R.layout.view_common_dialog_message, null);
        textView.setText(message);
        textView.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT));
        setContentView(textView);
    }

    public void setMessage(int textId) {
        setMessage(mContext.getResources().getString(textId));
    }

    @Override
    public void show() {
        if (mPositiveButton.getVisibility() == View.VISIBLE
                && mNegativeButton.getVisibility() == View.VISIBLE && mNaturelButton.getVisibility() == View.VISIBLE) {
            mPositiveButton
                    .setBackgroundResource(R.drawable.button_middle_selctor);
            mNegativeButton.setBackgroundResource(R.drawable.common_dlg_leftbtn_selector);
            this.findViewById(R.id.devider_positive_negative).setVisibility(View.VISIBLE);
            mNaturelButton.setBackgroundResource(R.drawable.common_dlg_rightbtn_selector);
            this.findViewById(R.id.devider_negative_naturl).setVisibility(View.VISIBLE);

        } else if (mPositiveButton.getVisibility() == View.VISIBLE
                && mNegativeButton.getVisibility() == View.VISIBLE) {
            mPositiveButton
                    .setBackgroundResource(R.drawable.common_dlg_rightbtn_selector);
            mNegativeButton.setBackgroundResource(R.drawable.common_dlg_leftbtn_selector);
            this.findViewById(R.id.devider_positive_negative).setVisibility(View.VISIBLE);
        } else if (mPositiveButton.getVisibility() == View.VISIBLE) {
            mPositiveButton.setBackgroundResource(R.drawable.common_dlg_fullbtn_selector);
        } else if (mNegativeButton.getVisibility() == View.VISIBLE) {
            mNegativeButton.setBackgroundResource(R.drawable.common_dlg_fullbtn_selector);
        }

        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.width = Resources.getSystem().getDisplayMetrics().widthPixels;
        window.setAttributes(lp);
        super.show();
    }

}
