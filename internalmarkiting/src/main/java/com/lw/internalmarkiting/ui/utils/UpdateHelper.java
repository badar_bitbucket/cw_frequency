package com.lw.internalmarkiting.ui.utils;

import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;

import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.InstallState;
import com.google.android.play.core.install.InstallStateUpdatedListener;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.InstallStatus;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.Task;

// TODO current usage of this helper class
/*UpdateHelper.checkForUpdate(context, new UpdateHelper.CheckUpdateListener() {
    @Override
    public void onUpdateFound() {
        UpdateHelper.startUpdate(activity, new UpdateHelper.TrackListener() {
            @Override
            public void onDownloaded() {
                Snackbar snackbar =
                        Snackbar.make(
                                findViewById(R.id.main_content),
                                "An update has just been downloaded.",
                                Snackbar.LENGTH_INDEFINITE);
                snackbar.setAction("RESTART", v -> UpdateHelper.completeUpdate());
                snackbar.setActionTextColor(
                        getResources().getColor(R.color.black));
                snackbar.show();
            }
        });
    }
});*/
public enum UpdateHelper {
    ;

    private static final int REQUEST_UPDATE = 10019;

    private static AppUpdateManager appUpdateManager;
    private static AppUpdateInfo appUpdateInfo;
    private static InstallUpdateImpl listener;

    public static void checkForUpdate(Context context, CheckUpdateListener listener) {
        if (appUpdateManager == null)
            appUpdateManager = AppUpdateManagerFactory.create(context);

        // Returns an intent object that you use to check for an update.
        Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();

        // Checks that the platform will allow the specified type of update.
        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                    // For a flexible update, use AppUpdateType.FLEXIBLE
                    && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE)) {
                // Request the update.
                UpdateHelper.appUpdateInfo = appUpdateInfo;
                listener.onUpdateFound();
            }
        });
    }

    public static void startUpdate(Activity activity, TrackListener trackListener) {
        try {
            appUpdateManager.startUpdateFlowForResult(
                    appUpdateInfo, AppUpdateType.IMMEDIATE, activity, REQUEST_UPDATE
            );
            registerListener(trackListener);
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
        }
    }

    public static void registerListener(TrackListener trackListener) {
        if (listener == null)
            listener = new InstallUpdateImpl(trackListener);
        appUpdateManager.registerListener(UpdateHelper.listener);
    }

    public static void unregisterListener() {
        // When status updates are no longer needed, unregister the listener.
        appUpdateManager.unregisterListener(listener);
    }

    public static void completeUpdate() {
        appUpdateManager.completeUpdate();

    }

    public interface CheckUpdateListener {
        void onUpdateFound();
    }

    public interface TrackListener {
        void onDownloaded();
    }

    private static class InstallUpdateImpl implements InstallStateUpdatedListener {

        private TrackListener trackListener;

        InstallUpdateImpl(TrackListener trackListener) {
            this.trackListener = trackListener;
        }

        @Override
        public void onStateUpdate(InstallState state) {
            // Show module progress, log state, or install the update.
            if (state.installStatus() == InstallStatus.DOWNLOADED) {
                // After the update is downloaded, show a notification
                // and request user confirmation to restart the app.
                trackListener.onDownloaded();
            } else if (state.installStatus() == InstallStatus.INSTALLED) {
                // After the update is downloaded, show a notification
                // and request user confirmation to restart the app.
                unregisterListener();
            }
        }
    }
}
