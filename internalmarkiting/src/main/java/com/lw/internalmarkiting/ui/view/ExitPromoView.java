package com.lw.internalmarkiting.ui.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.lw.internalmarkiting.AdsApp;
import com.lw.internalmarkiting.R;
import com.lw.internalmarkiting.data.model.PromoApp;
import com.lw.internalmarkiting.task.AdsTask;
import com.lw.internalmarkiting.task.SingleValueCallback;

import java.util.ArrayList;
import java.util.List;

public class ExitPromoView extends FrameLayout {

    private List<PromoApp> promoApps;
    private List<ImageView> imageViews;

    private ViewGroup root;
    private Context context;

    public ExitPromoView(Context context) {
        this(context, null);
    }

    public ExitPromoView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ExitPromoView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        if (isInEditMode()) {
            LayoutInflater.from(context).inflate(R.layout.exit_promo, this, true);
            return;
        }
        imageViews = new ArrayList<>();
        promoApps = new ArrayList<>();
        root = this;
        if (AdsApp.enableAds) {
            init();
        }
    }

    void init() {
        try {
            ExitPromoView smallAdMarketing = (ExitPromoView) LayoutInflater.from(context).inflate(R.layout.exit_promo, root);

            imageViews.add(smallAdMarketing.findViewById(R.id.ad1));
            imageViews.add(smallAdMarketing.findViewById(R.id.ad2));
            imageViews.add(smallAdMarketing.findViewById(R.id.ad3));

//            root.setVisibility(GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }

        AdsTask.loadExitApps(new SingleValueCallback<PromoApp>() {
            @Override
            public void onValue(PromoApp value) {
                promoApps.add(value);
            }

            @Override
            public void onComplete() {
                showExitPromo();
            }
        });
    }

    private void showExitPromo() {
        try {
            for (int i = 0; i < promoApps.size(); i++) {
                final PromoApp promoApp = promoApps.get(i);
                ImageView imageView = imageViews.get(i);
//                Picasso.get().load(promoApp.getImageUrl()).into(imageView, new Callback() {
//                    @Override
//                    public void onSuccess() {
//                        root.setVisibility(VISIBLE);
//                        imageView.setVisibility(VISIBLE);
//                    }
//
//                    @Override
//                    public void onError(Exception ignored) {
//                        imageView.setVisibility(GONE);
//                    }
//                });
                Glide.with(context.getApplicationContext()).load(promoApp.getImageUrl())
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
//                                imageView.setVisibility(GONE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                root.setVisibility(VISIBLE);
                                imageView.setVisibility(VISIBLE);
                                return false;
                            }
                        }).into(imageView);
                imageView.setOnClickListener(v -> promoApp.onClick());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
