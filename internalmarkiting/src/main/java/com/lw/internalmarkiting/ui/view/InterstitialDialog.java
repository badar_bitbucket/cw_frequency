package com.lw.internalmarkiting.ui.view;

import android.app.Dialog;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;

import com.lw.internalmarkiting.AdsApp;
import com.lw.internalmarkiting.BuildConfig;
import com.lw.internalmarkiting.OnAdCloseListener;
import com.lw.internalmarkiting.R;
import com.lw.internalmarkiting.data.model.PromoApp;
import com.lw.internalmarkiting.databinding.DialogInterstitialBinding;
import com.lw.internalmarkiting.task.AdsTask;
import com.lw.internalmarkiting.task.SingleValueCallback;
import com.lw.internalmarkiting.ui.Common;

import org.jetbrains.annotations.NotNull;

import timber.log.Timber;

public enum InterstitialDialog implements LifecycleObserver {
    INSTANCE;

    private Dialog mDialog;

    public void onClose(OnAdCloseListener closeListener) {
        dismissDialog();
        if (closeListener != null) closeListener.onAdClosed();
    }

    public void dismissDialog() {
        try {
            if (null != mDialog && isShowing())
                mDialog.dismiss();
            mDialog = null;
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
        }
    }

    public boolean isShowing() {
        return ((null != mDialog) && mDialog.isShowing());
    }

    public void showAd(AppCompatActivity activity, OnAdCloseListener listener) {
        if (!AdsApp.enableAds) {
            onClose(listener);
            return;
        }
        activity.getLifecycle().addObserver(INSTANCE);
        AdsTask.loadInterstitialPromoApps(new SingleValueCallback<PromoApp>() {
            @Override
            public void onValue(PromoApp promoApp) {
                Timber.i("Ads received : %s", promoApp);
                InterstitialDialog.this.show(activity, listener, promoApp);
            }

            @Override
            public void onComplete() {
                if (!isShowing())
                    onClose(listener);
            }
        });
    }

    private void show(AppCompatActivity activity, OnAdCloseListener onAdCloseListener, @NotNull PromoApp promoApp) {
        if (Common.isAppInstalled(promoApp.getPackageName())) {
            Timber.i("App is already installed");
            if (BuildConfig.DEBUG)
                Toast.makeText(activity, promoApp.getAppName() + " is installed", Toast.LENGTH_SHORT).show();
            return;
        }
        if (isShowing())
            return;
//        closeListener = onAdCloseListener;
        if (AdsApp.enableAds && !activity.isFinishing()) {
            dismissDialog();
            activity.getLifecycle().addObserver(INSTANCE);
            mDialog = new Dialog(activity, R.style.actionSheetTheme);
            DialogInterstitialBinding binding = DialogInterstitialBinding
                    .inflate(LayoutInflater.from(activity));
            mDialog.setContentView(binding.getRoot());
            binding.setPromoApp(promoApp);
            binding.ivClose.setOnClickListener(v -> onClose(onAdCloseListener));
            mDialog.setOnKeyListener((dialog, keyCode, event) -> {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    onClose(onAdCloseListener);
                    return true;
                }
                return false;
            });
            mDialog.setCancelable(true);
            mDialog.show();
        } else {
            onClose(onAdCloseListener);
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    public void onDestroy() {
        dismissDialog();
    }
}
