package com.lw.internalmarkiting.ui.view;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Window;
import android.widget.FrameLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;

import com.lw.internalmarkiting.AdsApp;
import com.lw.internalmarkiting.data.model.PromoApp;
import com.lw.internalmarkiting.databinding.DialogStartPromoBinding;
import com.lw.internalmarkiting.task.AdsTask;
import com.lw.internalmarkiting.task.SingleValueCallback;
import com.lw.internalmarkiting.ui.Common;
import com.pixplicity.easyprefs.library.Prefs;

import org.jetbrains.annotations.NotNull;

import timber.log.Timber;

public class StartPromoDialog implements LifecycleObserver {

    private static final String KEY_SHOW_PROMO = "KEY_SHOW_PROMO";

    private static Dialog dialog;
    private static StartPromoDialog instance;

    public static void show(AppCompatActivity activity) {
        if (AdsApp.enableAds) {
            if (Prefs.getBoolean(KEY_SHOW_PROMO, true)) {
                Prefs.putBoolean(KEY_SHOW_PROMO,false);
                activity.getLifecycle().addObserver(getInstance());
                AdsTask.loadStartAd(value -> {
                    show(activity, value);
                });
            }
        }
    }

    @NotNull
    private static StartPromoDialog getInstance() {
        if (instance == null)
            instance = new StartPromoDialog();
        return instance;
    }

    private static void dismiss() {
        if (isShowing()) dialog.dismiss();
    }

    private static void show(Context context, @NotNull PromoApp promoApp) {
        // TODO always show this in debug
//        if (!BuildConfig.DEBUG) {
        if (Common.isAppInstalled(promoApp.getPackageName())) {
            Timber.i("App is already installed");
            return;
        }
//        }
        if (isShowing())
            return;
        dialog = new Dialog(context);
        DialogStartPromoBinding binding = DialogStartPromoBinding.inflate(LayoutInflater.from(context));
        binding.setStartPromo(promoApp);
        dialog.setContentView(binding.getRoot());

        dialog.setOnKeyListener((dialog, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                dialog.cancel();
                return true;
            }
            return false;
        });
        binding.cancel.setOnClickListener(view -> dialog.dismiss());
        dialog.setCancelable(false);

        DisplayMetrics displayMetrics = Resources.getSystem().getDisplayMetrics();
        int widthLcl = (int) (displayMetrics.widthPixels * 0.8f);
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) binding.mainLayout.getLayoutParams();
        params.width = widthLcl;
        params.gravity = Gravity.CENTER;
        if (!((Activity) context).isFinishing())
            dialog.show();
        binding.mainLayout.setLayoutParams(params);
        Window window = dialog.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    private static boolean isShowing() {
        return dialog != null && dialog.isShowing();
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    public void onDestroy() {
        dismiss();
    }
}
