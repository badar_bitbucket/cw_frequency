package com.lw.internalmarkiting.ui.activities;

import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import timber.log.Timber;

public abstract class BindingActivity<DataBinding extends ViewDataBinding> extends BaseActivity {

    protected DataBinding binding;
    Handler handler = new Handler();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        beforeSettingContent();
        binding = DataBindingUtil.setContentView(activity, getResId());
        if (binding != null)
            binding.setLifecycleOwner(activity);
        onReady();
    }

    protected void showToast(String message) {
        Timber.i(message);
    }

    protected void beforeSettingContent() {
    }

    protected abstract void onReady();

    protected abstract int getResId();
}
