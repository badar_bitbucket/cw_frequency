package com.lw.internalmarkiting.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.lw.internalmarkiting.AdsApp;
import com.lw.internalmarkiting.ads.AdHelper;

import org.jetbrains.annotations.NotNull;

import static com.lw.internalmarkiting.BuildConfig.BANNER_AD_ID;

public class AdaptiveBannerView extends FrameLayout {
    public AdaptiveBannerView(@NonNull Context context) {
        this(context, null);
    }

    public AdaptiveBannerView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AdaptiveBannerView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        if (AdsApp.enableAds) {
            AdView mAdView = new AdView(context);
            mAdView.setAdSize(getAdSize());
            mAdView.setAdUnitId(BANNER_AD_ID);
            mAdView.loadAd(AdHelper.getAdRequest());
            addView(mAdView);
        }
    }

    private AdSize getAdSize() {
        return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(getContext(), getAdWidth());
    }

    private int getAdWidth() {
        DisplayMetrics outMetrics = getDisplayMetrics();
        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;
        return (int) (widthPixels / density);
    }

    @NotNull
    private DisplayMetrics getDisplayMetrics() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics;
    }

    private WindowManager getWindowManager() {
        return (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
    }
}
