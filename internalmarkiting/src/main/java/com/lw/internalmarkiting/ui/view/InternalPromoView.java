package com.lw.internalmarkiting.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.lw.internalmarkiting.AdsApp;
import com.lw.internalmarkiting.data.model.PromoApp;
import com.lw.internalmarkiting.databinding.LayoutInternalPromoBinding;
import com.lw.internalmarkiting.task.AdsTask;
import com.lw.internalmarkiting.task.SingleValueCallback;
import com.lw.internalmarkiting.ui.Common;

import org.jetbrains.annotations.NotNull;

import java.util.HashSet;
import java.util.Set;

import timber.log.Timber;

public class InternalPromoView extends FrameLayout {

    private static Set<PromoApp> promoApps;

    public InternalPromoView(Context context) {
        this(context, null);
    }

    public InternalPromoView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public InternalPromoView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public static PromoApp getAd() {
        PromoApp promoApp = null;
        for (PromoApp adModel : promoApps) {
            if (promoApp == null) {
                promoApp = adModel;
            } else {
                if (Common.isAppInstalled(adModel.getPackageName()))
                    promoApp = adModel;
            }
        }
        return promoApp;
    }

    private void init() {
        if (AdsApp.enableAds) {
            promoApps = new HashSet<>();
            AdsTask.loadInterstitialPromoApps(new SingleValueCallback<PromoApp>() {
                @Override
                public void onValue(PromoApp promoApp) {
                    Timber.i("Promo received : %s", promoApp);
                    promoApps.add(promoApp);
                }

                @Override
                public void onComplete() {
                    showAd(getAd());
                }
            });
        }
    }

    private void showAd(PromoApp promoApp) {
        LayoutInternalPromoBinding binding = LayoutInternalPromoBinding.inflate(LayoutInflater.from(getContext()), this, true);
        binding.setPromoApp(promoApp);
        binding.setClickHandler(new ClickHandler());
    }

    public static class ClickHandler {
        public void onClick(@NotNull PromoApp promoApp) {
            promoApp.onClick();
        }

        public void onAccountNameClicked(@NotNull PromoApp promoApp) {
            promoApp.onAccountNameClicked();
        }
    }
}
