package com.lw.internalmarkiting.ui.utils;

import android.content.Context;
import android.os.RemoteException;

import com.android.installreferrer.api.InstallReferrerClient;
import com.android.installreferrer.api.InstallReferrerStateListener;
import com.android.installreferrer.api.ReferrerDetails;
import com.pixplicity.easyprefs.library.Prefs;

import static com.lw.internalmarkiting.ui.utils.AdUtils.INTERNAL_MARKETING;

public class InstallReferrerHelper {

    public static final String KEY_LOGGED = "KEY_LOGGED";

    public static void logInstallReferrer(Context context) {
        boolean logged = Prefs.getBoolean(KEY_LOGGED,false);
        if (logged) {
            return;
        }
        InstallReferrerClient referrerClient = InstallReferrerClient.newBuilder(context).build();
        referrerClient.startConnection(new InstallReferrerStateListener() {
            @Override
            public void onInstallReferrerSetupFinished(int responseCode) {
                Prefs.putBoolean(KEY_LOGGED,true);
                switch (responseCode) {
                    case InstallReferrerClient.InstallReferrerResponse.OK:
                        // Connection established.
                        ReferrerDetails response;
                        try {
                            response = referrerClient.getInstallReferrer();
                            String referrerUrl = response.getInstallReferrer();
                            if(referrerUrl.contains(INTERNAL_MARKETING)){
                                int index = referrerUrl.lastIndexOf("%3D") + 3;
                                String from = referrerUrl.substring(index);
                                System.out.println("From : " + from);
                                Analytics.logReferrerEvent(context.getPackageName(),from);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                        break;
                    case InstallReferrerClient.InstallReferrerResponse.FEATURE_NOT_SUPPORTED:
                        // API not available on the current Play Store app.
                        break;
                    case InstallReferrerClient.InstallReferrerResponse.SERVICE_UNAVAILABLE:
                        // Connection couldn't be established.
                        break;
                }
            }

            @Override
            public void onInstallReferrerServiceDisconnected() {
            }
        });
    }
}
