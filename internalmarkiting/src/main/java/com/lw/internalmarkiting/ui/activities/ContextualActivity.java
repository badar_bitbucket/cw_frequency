package com.lw.internalmarkiting.ui.activities;

import android.content.Context;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.zeugmasolutions.localehelper.LocaleAwareCompatActivity;

import org.jetbrains.annotations.Nullable;

public abstract class ContextualActivity extends LocaleAwareCompatActivity {

    protected AppCompatActivity activity = this;
    protected Context context = this;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
