package com.lw.internalmarkiting.ui.activities

import androidx.databinding.ViewDataBinding

abstract class AbsSplashActivity<T : ViewDataBinding>  : FullScreenActivity<T>() {
    abstract val delay: Long

    abstract val action: Runnable

    override fun onResume() {
        super.onResume()
        handler.postDelayed(action, delay)
    }

    override fun onPause() {
        super.onPause()
        handler.removeCallbacks(action)
    }
}
