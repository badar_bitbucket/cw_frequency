package com.lw.internalmarkiting.ui.utils;

import android.view.View;
import android.view.ViewGroup;

import androidx.transition.ChangeBounds;
import androidx.transition.TransitionManager;

public class AnimationUtils {
    public static void addChangeBoundAnimationToParent(View view){
        try {
            ViewGroup parent = (ViewGroup) view.getParent();
            ChangeBounds transition = new ChangeBounds();
            transition.setDuration(400);
            TransitionManager.beginDelayedTransition( parent, transition);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
