package com.lw.internalmarkiting.ui.utils;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateUtils {

    private static SimpleDateFormat sdfDate = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US);//dd/MM/yyyy

    @NotNull
    public static String getFormattedCurrentTime() {
        Date now = new Date();
        return sdfDate.format(now);
    }
}
