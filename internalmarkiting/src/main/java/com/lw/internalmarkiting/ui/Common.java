package com.lw.internalmarkiting.ui;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.StringDef;
import androidx.core.app.ShareCompat;

import com.lw.internalmarkiting.AdsApp;
import com.lw.internalmarkiting.R;

import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.annotation.Retention;
import java.nio.charset.Charset;

import timber.log.Timber;

import static java.lang.annotation.RetentionPolicy.SOURCE;

public class Common {

    public static final String LOGIC_WORMS_APPS = "Logic+Worms+Apps";
    public static final String CHRIS_WILLIAMS_APPS = "Chris+Williams";
    public static final String LASH_PASH_APPS = "LashPash+Apps";
    public static final String SMART_GORILLA_APPS = "Smart+Gorilla";
    public static final String BLUE_RAY_APPS = "Blue+Ray+Apps";

    private static final int DURATION = 500;
    public static final String PLAY_DETAILS_LINK = "https://play.google.com/store/apps/details?id=";

    public static void gotoPlayStore(@NonNull Context context, String pkg) {
        Timber.i("Package name : %s", pkg);
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + pkg)));
        } catch (android.content.ActivityNotFoundException ignored) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + pkg)));
        }
    }

    @NotNull
    public static String getStoreLink(String pkg) {
        Timber.i("Package name : %s", pkg);
        return "https://play.google.com/store/apps/details?id=" + pkg;
    }

    public static boolean isOnline(@NotNull Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm == null)
            return false;
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return (netInfo != null && netInfo.isConnected());
    }

    public static void showPolicy(Context context) {
        try {
            final Dialog dialog = new Dialog(context, android.R.style.Theme_DeviceDefault_Light_Dialog_MinWidth);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.privacypolicy);
            dialog.setCancelable(true);
            Button gotIt = dialog.findViewById(R.id.gotIt);
            TextView textView = dialog.findViewById(R.id.textView);

            String policy = readFromAssets(context);

            String accountName = PlayAppsChecker.getAccountName(context.getPackageName());
            String privacyPolicy = String.format(policy, accountName, PlayAppsChecker.getAccountEmail(accountName));
            textView.setText(Html.fromHtml(privacyPolicy));
            gotIt.setOnClickListener(v -> dialog.cancel());
            if (!((Activity) context).isFinishing())
                dialog.show();
        } catch (IOException ignored) {

        }
    }

    public static boolean isAppInstalled(String packageName) {
        PackageManager pm = AdsApp.getContext().getPackageManager();
        try {
            pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException ignored) {
        }
        return false;
    }

    @NotNull
    private static String readFromAssets(@NotNull Context context) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(context.getAssets()
                .open("html.txt"), Charset.forName("UTF-16")));
        // do reading, usually loop until end of file reading
        StringBuilder sb = new StringBuilder();
        String mLine = reader.readLine();
        while (mLine != null) {
            sb.append(mLine); // process line
            mLine = reader.readLine();
        }
        reader.close();
        return sb.toString();
    }

    public static void share(@NotNull Context context, @NotNull String message) {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_TITLE, getAppName(context));
        i.putExtra(Intent.EXTRA_TEXT, buildMessage(context, message));
        context.startActivity(Intent.createChooser(i, "Choose one"));
    }

    @NotNull
    private static String buildMessage(@NotNull Context context, @NotNull String message) {
        return message + " " + getAppLink(context);
    }

    @NotNull
    private static String getAppLink(@NotNull Context context) {
        return PLAY_DETAILS_LINK + context.getPackageName();
    }

    @NotNull
    private static String getAppName(@NotNull Context context) {
        int applicationNameId = context.getApplicationInfo().labelRes;
        return context.getString(applicationNameId);
    }

    public static void rateUs(Context context) {
        gotoPlayStore(context, context.getPackageName());
    }

    public static void moreApps(@NotNull Context context, @DeveloperName String developerName) {
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("market://search?q=pub:" + developerName)));
        } catch (android.content.ActivityNotFoundException ignored) {
            context.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/developer?id=" + developerName)));
        }
    }

    public static void openPlayStore(@NonNull Context context, String packageName) {
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("market://details?id=" + packageName)));
        } catch (android.content.ActivityNotFoundException ignored) {
            context.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=" + packageName)));
        }
    }

    public static void animateHeight(@NotNull final View v, int targetHeight) {
        int prevHeight = v.getHeight();
        ValueAnimator valueAnimator = ValueAnimator.ofInt(prevHeight, targetHeight);
        valueAnimator.setInterpolator(new DecelerateInterpolator());
        valueAnimator.addUpdateListener(animation -> {
            v.getLayoutParams().height = (int) animation.getAnimatedValue();
            v.requestLayout();
        });
        valueAnimator.setInterpolator(new DecelerateInterpolator());
        valueAnimator.setDuration(DURATION);
        valueAnimator.start();
    }

    public static int convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return (int) (dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    @Retention(SOURCE)
    @StringDef({
            BLUE_RAY_APPS, LOGIC_WORMS_APPS, CHRIS_WILLIAMS_APPS, LASH_PASH_APPS,
            SMART_GORILLA_APPS
    })
    public @interface DeveloperName {
    }
}
