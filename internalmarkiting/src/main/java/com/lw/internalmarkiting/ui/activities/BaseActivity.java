package com.lw.internalmarkiting.ui.activities;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import java.util.Locale;

public abstract class BaseActivity<DataBinding extends ViewDataBinding> extends ContextualActivity {

    protected DataBinding binding;
    protected Handler handler = new Handler();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        beforeSettingContent();
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(activity, getResId());
        if (binding != null)
            binding.setLifecycleOwner(activity);
        onReady();
        onReady(savedInstanceState);
    }
    public void showText(String message){
        Toast.makeText(context,message,Toast.LENGTH_LONG).show();
    }

    protected void enableToolbarBack(){
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            supportActionBar.setDisplayShowHomeEnabled(true);
        }
    }

    // call before setContentView
    private void setLocale(String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    protected void showToast(String message) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
    }

    protected void beforeSettingContent() {
    }

    protected abstract void onReady();

    protected void onReady(@Nullable Bundle savedInstanceState){

    }

    protected abstract int getResId();
}
