package com.lw.internalmarkiting.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import androidx.annotation.ColorRes;
import androidx.core.content.ContextCompat;

import com.lw.internalmarkiting.AdsApp;
import com.lw.internalmarkiting.R;
import com.lw.internalmarkiting.data.model.PromoApp;
import com.lw.internalmarkiting.databinding.ItemNavAdBinding;
import com.lw.internalmarkiting.task.AdsTask;

import org.jetbrains.annotations.NotNull;

public class NavPromoView extends FrameLayout {

    private static final boolean ATTACH_TO_ROOT = true;

    private ItemNavAdBinding binding;

    public NavPromoView(Context context) {
        this(context, null);
    }

    public NavPromoView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NavPromoView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (isInEditMode()) {
            View view = getInflater().inflate(R.layout.item_nav_ad, this, true);
            view.setVisibility(View.VISIBLE);
        } else {
            if (AdsApp.enableAds) {
                binding = ItemNavAdBinding.inflate(getInflater(), this, ATTACH_TO_ROOT);
                loadPromo();
            }
        }
    }

    private LayoutInflater getInflater() {
        return LayoutInflater.from(getContext());
    }

    private void loadPromo() {
        AdsTask.loadNavAd(this::showPromo);
    }

    private void showPromo(@NotNull PromoApp promoApp) {
        if (promoApp.isAppNotInstalled()) {
            binding.setPromoApp(promoApp);
            binding.root.setVisibility(View.VISIBLE);
            binding.popAdView.updateContent(promoApp.getImageUrl());
        }
    }

    public void setTypeFace(int style) {
        binding.textViewAppName.setTypeface(binding.textViewAppName.getTypeface(), style);
    }

    public void setColor(@ColorRes int colorRes) {
        int color = ContextCompat.getColor(getContext(), colorRes);
        binding.textViewAppName.setTextColor(color);
    }
}
