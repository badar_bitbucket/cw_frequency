package com.lw.internalmarkiting.ui.view;


import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.lw.internalmarkiting.R;

import org.jetbrains.annotations.NotNull;

public class PopAdView extends FrameLayout {

    private final Animation popOut;
    private final Animation popIn;
    private ImageView imageView;
    private View rootAd;

    public PopAdView(Context context) {
        this(context, null);
    }

    public PopAdView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PopAdView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater.from(context).inflate(R.layout.layout_pop_ad, this, true);
        imageView = findViewById(R.id.imageView);
        rootAd = findViewById(R.id.rootAd);
        if (isInEditMode())
            rootAd.setVisibility(View.VISIBLE);

        popOut = AnimationUtils.loadAnimation(context, R.anim.pop_out);
        popIn = AnimationUtils.loadAnimation(context, R.anim.pop_in);
    }

    private void animateAd() {
        if (popOut != null) {
            startAnimation(popOut);
            popOut.setInterpolator(new AccelerateDecelerateInterpolator());
            popOut.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    startAnimation(popIn);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        }
        if (popIn != null) {
            popIn.setInterpolator(new AccelerateDecelerateInterpolator());
            popIn.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    startAnimation(popOut);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        }
    }

    public void updateContent(@NotNull String imageUrl) {
        GlideHelper.load(getContext(), imageUrl, imageView, new GlideHelper.CallBack() {
            @Override
            public void onFailed() {
                rootAd.setVisibility(GONE);
            }

            @Override
            public void onSuccess() {
                rootAd.setVisibility(VISIBLE);
            }
        });
    }
}
