package com.lw.internalmarkiting.ui.view;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import androidx.annotation.ColorInt;
import androidx.annotation.ColorRes;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;

import com.lw.internalmarkiting.AdsApp;
import com.lw.internalmarkiting.BuildConfig;
import com.lw.internalmarkiting.R;
import com.lw.internalmarkiting.ads.InterstitialHelper;
import com.lw.internalmarkiting.data.model.PromoApp;
import com.lw.internalmarkiting.databinding.LayoutHotPromoBinding;
import com.lw.internalmarkiting.task.AdsTask;
import com.lw.internalmarkiting.task.SingleValueCallback;
import com.lw.internalmarkiting.ui.activities.HotAppsActivity;

import org.jetbrains.annotations.NotNull;

public class HotPromoView extends FrameLayout {

    private static final boolean ATTACH_TO_ROOT = true;

    protected boolean showIntersAd;
    protected boolean showVideoAd;

    private LayoutHotPromoBinding binding;

    public HotPromoView(Context context) {
        this(context, null);
    }

    public HotPromoView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public HotPromoView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (isInEditMode()) {
            View view = LayoutInflater.from(context).inflate(R.layout.layout_hot_promo, this, true);
            view.setVisibility(View.VISIBLE);
        } else {
            binding = LayoutHotPromoBinding.inflate(LayoutInflater.from(context), this, ATTACH_TO_ROOT);
            if (AdsApp.enableAds) {
                AdsTask.loadHotApps(new SingleValueCallback<PromoApp>() {
                    @Override
                    public void onValue(PromoApp promoApp) {
                    }

                    @Override
                    public void onComplete() {
                        loadCount();
                    }
                });
            }
        }
    }

    public void setShowIntersAd(boolean showIntersAd) {
        this.showIntersAd = showIntersAd;
    }

    public void setShowVideoAd(boolean showVideoAd) {
        if (AdsApp.enableAds)
            this.showVideoAd = showVideoAd;
    }

    private void loadCount() {
        AdsTask.getHotPromoCount(this::showPromoCount);
        setClickEvent();
    }

    private void showPromoCount(int promoCount) {
        if (promoCount > 0) {
            binding.setPromoCount(promoCount);
            binding.root.setVisibility(VISIBLE);
        } else binding.root.setVisibility(GONE);
    }

    public void showInterstitialAd(boolean showAd) {
        setShowIntersAd(showAd);
        if (AdsApp.enableAds) {
            setClickEvent();
        }
    }

    protected View.OnClickListener clickListener = new OnClickListener() {
        @Override
        public void onClick(@NotNull View v) {
            Context context = v.getContext();
            if (showIntersAd)
                InterstitialHelper.showAd(() ->
                        HotAppsActivity.start(context, showVideoAd));
            else {
                HotAppsActivity.start(context, showVideoAd);
            }
        }
    };

    protected void setClickEvent() {
        binding.root.setOnClickListener(clickListener);
    }

    public void setTextColor(@ColorRes int colorRes) {
        int color = ContextCompat.getColor(getContext(), colorRes);
        binding.tvHotApps.setTextColor(color);
        tintViewDrawable(binding.tvHotApps, color);
    }

    public void setIconColor(@ColorRes int colorRes) {
        int color = ContextCompat.getColor(getContext(), colorRes);
        tintViewDrawable(binding.tvHotApps, color);
    }

    public void setTypeFace(int style) {
        binding.tvHotApps.setTypeface(binding.tvHotApps.getTypeface(), style);
    }

    private void tintViewDrawable(@NotNull AppCompatTextView view, @ColorInt int color) {
        Drawable[] drawables = view.getCompoundDrawables();
        for (Drawable drawable : drawables) {
            if (drawable != null) {
                drawable.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
            }
        }
    }
}
