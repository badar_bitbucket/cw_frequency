package com.lw.internalmarkiting.ui.view;

import android.app.Dialog;
import android.content.Context;

import com.lw.internalmarkiting.R;

public enum ExitDialog {
    INSTANCE;

    private Dialog mDialog;

    public void dismissDialog() {
        //due to crash in 4
        try {
            if (null != mDialog && isShowing())
                mDialog.dismiss();
            mDialog = null;
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
        }
    }

    public boolean isShowing() {
        return ((null != mDialog) && mDialog.isShowing());
    }

    public void showBackPressDialog(Context context, ClickListener clickListener) {
        dismissDialog();
        mDialog = new Dialog(context, R.style.actionSheetTheme);
        mDialog.setContentView(R.layout.dialog_back_press);

        mDialog.findViewById(R.id.btnYes).setOnClickListener(v -> {
            dismissDialog();
            clickListener.onClick();
        });
        mDialog.findViewById(R.id.btnCancel).setOnClickListener(v -> dismissDialog());

        mDialog.setCancelable(true);
        mDialog.show();
    }

    public interface ClickListener {
        void onClick();
    }


}
