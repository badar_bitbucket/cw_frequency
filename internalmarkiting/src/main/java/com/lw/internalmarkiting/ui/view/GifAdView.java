package com.lw.internalmarkiting.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.lw.internalmarkiting.R;
import com.lw.internalmarkiting.ads.InterstitialHelper;

public class GifAdView extends FrameLayout {

    private Context context;
    private ImageView imageView;

    public GifAdView(Context context) {
        this(context, null);
    }

    public GifAdView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public GifAdView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        this.context = context;

        View view = inflate(getContext(), R.layout.layout_gif_ad, null);
        addView(view);

        imageView = view.findViewById(R.id.imageView);

        view.setOnClickListener(v -> InterstitialHelper.showAd());
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();

        GlideHelper.load(context, R.raw.gif_ad, imageView);
    }
}
