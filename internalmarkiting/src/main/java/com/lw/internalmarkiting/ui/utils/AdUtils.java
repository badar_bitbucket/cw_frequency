package com.lw.internalmarkiting.ui.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

import com.lw.internalmarkiting.AdsApp;

import org.jetbrains.annotations.NotNull;

public class AdUtils {

    public static final String INTERNAL_MARKETING = "internalmarketing";
    private static final String PLAY_GOOGLE = "https://play.google.com/store/apps/details?id=";
    private static final String UTM_SOURCE = "&referrer=utm_source%3D" + INTERNAL_MARKETING;
    private static final String UTM_CAMPAIGN = "%26utm_campaign%3D";

    public static void openPlayStore(@NotNull Context context, String packageName) {
        try {
            // https://play.google.com/store/apps/details?id=com.tm.radiation.meetr&referrer=utm_source%3Dinternalmarketing%26utm_campaign%3DSG_Unseen
//            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName));
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            context.startActivity(intent);
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(PLAY_GOOGLE + packageName + UTM_SOURCE + UTM_CAMPAIGN + context.getPackageName()));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        } catch (android.content.ActivityNotFoundException ignored) {
//            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(PLAY_GOOGLE + packageName));
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            context.startActivity(intent);
            Toast.makeText(context, "Cannot open the link", Toast.LENGTH_SHORT).show();
        }
    }

    public static void openPlayStore(String packageName) {
        openPlayStore(AdsApp.getContext(), packageName);
    }

    public static void openAccount(String accountName) {
        openAccount(AdsApp.getContext(), accountName);
    }

    public static void openAccount(@NotNull Context context, String accountName) {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://search?q=pub:" + accountName));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        } catch (android.content.ActivityNotFoundException ignored) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/search?q=pub:=" + accountName));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
    }
}