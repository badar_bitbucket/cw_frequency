package com.lw.internalmarkiting.ui.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.annotation.IntDef
import com.lw.internalmarkiting.R
import com.lw.internalmarkiting.ui.Common.rateUs
import kotlinx.android.synthetic.main.activity_exit.*

class ExitActivity : ContextualActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_exit)

        viewRateUs.setOnClickListener(this)
        viewCancel.setOnClickListener(this)
        viewQuit.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.viewRateUs -> rateUs(context)
            R.id.viewCancel -> finish()
            R.id.viewQuit -> {
                setResult(Activity.RESULT_OK)
                finish()
            }
        }
    }

    @Retention(AnnotationRetention.SOURCE)
    @IntDef(CODE_EXIT)
    internal annotation class RequestCode

    companion object {
        const val CODE_EXIT = 302

        @JvmStatic
        @JvmOverloads
        fun startActivityForResult(activity: Activity, @RequestCode code: Int = CODE_EXIT) {
            activity.startActivityForResult(Intent(activity, ExitActivity::class.java), code)
        }

//        @JvmStatic
//        fun startActivityForResult(activity: Activity) {
//            activity.startActivityForResult(Intent(activity, ExitActivity::class.java), CODE_EXIT)
//        }
    }
}
