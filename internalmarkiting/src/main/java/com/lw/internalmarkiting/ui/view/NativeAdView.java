package com.lw.internalmarkiting.ui.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.IntDef;
import androidx.annotation.Nullable;
import androidx.transition.ChangeBounds;
import androidx.transition.TransitionManager;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.VideoOptions;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.lw.internalmarkiting.AdsApp;
import com.lw.internalmarkiting.R;
import com.lw.internalmarkiting.ads.AdHelper;
import com.lw.internalmarkiting.ui.utils.AnimationUtils;

import org.jetbrains.annotations.NotNull;

import java.lang.annotation.Retention;

import static com.lw.internalmarkiting.BuildConfig.NATIVE_AD_ID;
import static java.lang.annotation.RetentionPolicy.SOURCE;

public class NativeAdView extends FrameLayout {

    private int layoutId;

    public NativeAdView(Context context) {
        this(context, null);
    }

    public NativeAdView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NativeAdView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setupAd(context, attrs);
    }

    void setupAd(Context context, AttributeSet attrs) {
        getAttributes(context, attrs);
        if (isInEditMode()) {
            LayoutInflater.from(context).inflate(layoutId, this, true);
            return;
        }
        if (AdsApp.enableAds)
            refreshAd(context);
    }

    private void getAttributes(@NotNull Context context, AttributeSet attrs) {
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.NativeAdView, 0, 0);
        int viewType;
        try {
            viewType = typedArray.getInt(R.styleable.NativeAdView_view_type, ViewType.MEDIUM);
        } finally {
            typedArray.recycle();
        }
        switch (viewType) {
            case ViewType.SMALL:
                layoutId = R.layout.ad_small;
                break;
            case ViewType.MEDIUM:
                layoutId = R.layout.ad_medium;
                break;
            case ViewType.LARGE:
                layoutId = R.layout.ad_medium;
                break;
        }
    }

    private void populateUnifiedNativeAdView(@NotNull UnifiedNativeAd nativeAd,
                                             @NotNull UnifiedNativeAdView adView) {
        // Set the media view. Media content will be automatically populated in the media view once
        // adView.setNativeAd() is called.

        // Set other ad assets.
        adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
        adView.setBodyView(adView.findViewById(R.id.ad_body));
        adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
        adView.setIconView(adView.findViewById(R.id.ad_app_icon));
        adView.setPriceView(adView.findViewById(R.id.ad_price));
        adView.setStarRatingView(adView.findViewById(R.id.ad_stars));
        adView.setStoreView(adView.findViewById(R.id.ad_store));
        adView.setAdvertiserView(adView.findViewById(R.id.ad_advertiser));
        // The headline is guaranteed to be in every UnifiedNativeAd.
        TextView headlineView = (TextView) adView.getHeadlineView();
        headlineView.setText(nativeAd.getHeadline());

        // These assets aren't guaranteed to be in every UnifiedNativeAd, so it's important to
        // check before trying to display them.
        if (nativeAd.getBody() == null) {
            adView.getBodyView().setVisibility(View.INVISIBLE);
        } else {
            adView.getBodyView().setVisibility(View.VISIBLE);
            ((TextView) adView.getBodyView()).setText(nativeAd.getBody());
        }

        if (nativeAd.getCallToAction() == null) {
            adView.getCallToActionView().setVisibility(View.INVISIBLE);
        } else {
            adView.getCallToActionView().setVisibility(View.VISIBLE);
            ((Button) adView.getCallToActionView()).setText(nativeAd.getCallToAction());
        }

        if (nativeAd.getIcon() == null) {
            adView.getIconView().setVisibility(View.GONE);
        } else {
            ((ImageView) adView.getIconView()).setImageDrawable(
                    nativeAd.getIcon().getDrawable());
            adView.getIconView().setVisibility(View.VISIBLE);
        }

      /*  if (nativeAd.getPrice() == null) {
            adView.getPriceView().setVisibility(View.INVISIBLE);
        } else {
            adView.getPriceView().setVisibility(View.VISIBLE);
            ((TextView) adView.getPriceView()).setText(nativeAd.getPrice());
        }*/

      /*  if (nativeAd.getStore() == null) {
            adView.getStoreView().setVisibility(View.INVISIBLE);
        } else {
            adView.getStoreView().setVisibility(View.VISIBLE);
            ((TextView) adView.getStoreView()).setText(nativeAd.getStore());
        }*/

        if (nativeAd.getStarRating() == null) {
            adView.getStarRatingView().setVisibility(View.GONE);
        } else {
            ((RatingBar) adView.getStarRatingView())
                    .setRating(nativeAd.getStarRating().floatValue());
            adView.getStarRatingView().setVisibility(View.VISIBLE);
        }

        if (nativeAd.getAdvertiser() == null) {
            adView.getAdvertiserView().setVisibility(View.GONE);
        } else {
            ((TextView) adView.getAdvertiserView()).setText(nativeAd.getAdvertiser());
            adView.getAdvertiserView().setVisibility(View.VISIBLE);
        }

        // This method tells the Google Mobile Ads SDK that you have finished populating your
        // native ad view with this native ad. The SDK will populate the adView's MediaView
        // with the media content from this native ad.
        adView.setNativeAd(nativeAd);

        // Get the video controller for the ad. One will always be provided, even if the ad doesn't
        // have a video asset.
        VideoController vc = nativeAd.getVideoController();

        // Updates the UI to say whether or not this ad has a video asset.
        if (vc.hasVideoContent()) {


            // Create a new VideoLifecycleCallbacks object and pass it to the VideoController. The
            // VideoController will call methods on this object when events occur in the video
            // lifecycle.
            vc.setVideoLifecycleCallbacks(new VideoController.VideoLifecycleCallbacks() {
                @Override
                public void onVideoEnd() {
                    // Publishers should allow native ads to complete video playback before
                    // refreshing or replacing them with another ad in the same UI location.

                    super.onVideoEnd();
                }
            });
        } else {

        }
    }

    private void refreshAd(Context context) {
        // refresh.setEnabled(false);
        AdLoader.Builder builder = new AdLoader.Builder(context, NATIVE_AD_ID);

        builder.forUnifiedNativeAd(unifiedNativeAd -> {
            UnifiedNativeAdView adView = (UnifiedNativeAdView) LayoutInflater.from(context)
                    .inflate(layoutId, null);
            populateUnifiedNativeAdView(unifiedNativeAd, adView);
            removeAllViews();

            AnimationUtils.addChangeBoundAnimationToParent(this);

            addView(adView);
        });

        VideoOptions videoOptions = new VideoOptions.Builder()
                .setStartMuted(true)
                .build();

        NativeAdOptions adOptions = new NativeAdOptions.Builder()
                .setVideoOptions(videoOptions)
                .build();

        builder.withNativeAdOptions(adOptions);

        AdLoader adLoader = builder.withAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int errorCode) {

                //   refresh.setEnabled(true);
            }
        }).build();
        adLoader.loadAd(AdHelper.getAdRequest());

        // videoStatus.setText("");
    }

    interface ViewType {
        int SMALL = 1;
        int MEDIUM = 2;
        int LARGE = 3;

        @Retention(SOURCE)
        @IntDef({SMALL, MEDIUM, LARGE})
        @interface ViewTypes {
        }
    }
}
