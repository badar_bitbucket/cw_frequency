package com.lw.internalmarkiting.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.VideoOptions;
import com.google.android.gms.ads.formats.MediaView;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.lw.internalmarkiting.AdsApp;
import com.lw.internalmarkiting.R;
import com.lw.internalmarkiting.ads.AdHelper;
import com.lw.internalmarkiting.ui.utils.AnimationUtils;

import org.jetbrains.annotations.NotNull;

import static com.lw.internalmarkiting.BuildConfig.NATIVE_AD_ID;

public class NativeAdMediaView2 extends FrameLayout {

    public NativeAdMediaView2(Context context) {
        this(context, null);
    }

    public NativeAdMediaView2(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NativeAdMediaView2(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setupAd(context);
    }

    private void setupAd(Context context) {
        if (AdsApp.enableAds)
            refreshAd(context);
    }

    private void refreshAd(Context context) {
        AdLoader.Builder builder = new AdLoader.Builder(context, NATIVE_AD_ID);
        builder.forUnifiedNativeAd(unifiedNativeAd -> {
            View root = LayoutInflater.from(context).inflate(R.layout.native_ad2, null);
            UnifiedNativeAdView adView = root.findViewById(R.id.ad_view);
            populateUnifiedNativeAdView(unifiedNativeAd, adView);
            removeAllViews();
            AnimationUtils.addChangeBoundAnimationToParent(this);
            addView(root);
        });

        VideoOptions videoOptions = new VideoOptions.Builder()
                .setStartMuted(true)
                .build();

        NativeAdOptions adOptions = new NativeAdOptions.Builder()
                .setVideoOptions(videoOptions)
                .build();

        builder.withNativeAdOptions(adOptions);
        AdLoader adLoader = builder.withAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int errorCode) {
//                ToastUtil.showErrorLog(TAG,"Native Ad error code = "+errorCode);
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
            }

            @Override
            public void onAdClicked() {
                super.onAdClicked();
            }

            @Override
            public void onAdImpression() {
                super.onAdImpression();
            }
        }).build();
        adLoader.loadAd(AdHelper.getAdRequest());
    }

    private void populateUnifiedNativeAdView(@NotNull UnifiedNativeAd nativeAd,
                                             @NotNull UnifiedNativeAdView adView) {
        adView.setVisibility(View.VISIBLE);

        MediaView mediaView = adView.findViewById(R.id.ad_media);

        adView.setMediaView(mediaView);

        // Register the view used for each individual asset.
        adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
        adView.setBodyView(adView.findViewById(R.id.ad_body));
        adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
        adView.setIconView(adView.findViewById(R.id.ad_app_icon));
        adView.setPriceView(adView.findViewById(R.id.ad_price));
        adView.setStarRatingView(adView.findViewById(R.id.ad_stars));
        adView.setStoreView(adView.findViewById(R.id.ad_store));
        adView.setAdvertiserView(adView.findViewById(R.id.ad_advertiser));

        // The headline is guaranteed to be in every UnifiedNativeAd.
        ((TextView) adView.getHeadlineView()).setText(nativeAd.getHeadline());
        adView.getHeadlineView().setSelected(true);

        // These assets aren't guaranteed to be in every UnifiedNativeAd, so it's important to
        // check before trying to display them.
        if (nativeAd.getBody() == null) {
            adView.getBodyView().setVisibility(View.INVISIBLE);
        } else {
            adView.getBodyView().setVisibility(View.VISIBLE);
            ((TextView) adView.getBodyView()).setText(nativeAd.getBody());
        }

        if (nativeAd.getAdvertiser() == null) {
            adView.getAdvertiserView().setVisibility(View.GONE);
        } else {
            ((TextView) adView.getAdvertiserView()).setText(nativeAd.getAdvertiser());
            adView.getAdvertiserView().setVisibility(View.VISIBLE);
        }

        if (nativeAd.getStarRating() == null) {
            adView.getStarRatingView().setVisibility(View.GONE);
        } else {
            ((RatingBar) adView.getStarRatingView())
                    .setRating(nativeAd.getStarRating().floatValue());
            adView.getStarRatingView().setVisibility(View.VISIBLE);
        }

        if (nativeAd.getCallToAction() == null) {
            adView.getCallToActionView().setVisibility(View.INVISIBLE);
        } else {
            adView.getCallToActionView().setVisibility(View.VISIBLE);
            ((Button) adView.getCallToActionView()).setText(nativeAd.getCallToAction());
        }

        if (nativeAd.getIcon() == null) {
            adView.findViewById(R.id.cvIcon).setVisibility(View.GONE);
            adView.getIconView().setVisibility(View.GONE);
        } else {
            ((ImageView) adView.getIconView()).setImageDrawable(
                    nativeAd.getIcon().getDrawable());
            adView.findViewById(R.id.cvIcon).setVisibility(View.VISIBLE);
            adView.getIconView().setVisibility(View.VISIBLE);
        }

        if (nativeAd.getPrice() == null) {
            adView.getPriceView().setVisibility(View.INVISIBLE);
        } else {
            adView.getPriceView().setVisibility(View.VISIBLE);
            ((TextView) adView.getPriceView()).setText(nativeAd.getPrice());
        }

        if (nativeAd.getStore() == null) {
            adView.getStoreView().setVisibility(View.INVISIBLE);
        } else {
            adView.getStoreView().setVisibility(View.VISIBLE);
            ((TextView) adView.getStoreView()).setText(nativeAd.getStore());
        }

        // This method tells the Google Mobile Ads SDK that you have finished populating your
        // native ad view with this native ad. The SDK will populate the adView's MediaView
        // with the media content from this native ad.
        adView.setNativeAd(nativeAd);

        // Get the video controller for the ad. One will always be provided, even if the ad doesn't
        // have a video asset.
        VideoController vc = nativeAd.getVideoController();

        // Updates the UI to say whether or not this ad has a video asset.

        vc.setVideoLifecycleCallbacks(new VideoController.VideoLifecycleCallbacks() {
            @Override
            public void onVideoEnd() {
                // Publishers should allow native ads to complete video playback before
                // refreshing or replacing them with another ad in the same UI location.
//                    refresh.setEnabled(true);
//                    ToastUtil.showErrorLog(TAG,"Video status: Video Playback has ended.");
                super.onVideoEnd();
            }
        });

    }
}
