package com.lw.internalmarkiting.ui.view;

import android.content.Context;
import android.util.AttributeSet;

import com.lw.internalmarkiting.AdsApp;
import com.lw.internalmarkiting.BuildConfig;
import com.lw.internalmarkiting.data.model.PromoApp;
import com.lw.internalmarkiting.task.AdsTask;

import org.jetbrains.annotations.NotNull;

import timber.log.Timber;

public class CornerPromoView extends PopAdView {

    public CornerPromoView(Context context) {
        this(context, null);
    }

    public CornerPromoView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CornerPromoView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (AdsApp.enableAds) {
            AdsTask.loadCornerAd(this::showPromo);
        }
    }

    private void showPromo(@NotNull PromoApp promoApp) {
        if (BuildConfig.DEBUG)
            Timber.i(promoApp.toString());
        if (promoApp.isAppNotInstalled()) {
            updateContent(promoApp.getImageUrl());
            setOnClickListener(v -> promoApp.onClick());
        }
    }
}