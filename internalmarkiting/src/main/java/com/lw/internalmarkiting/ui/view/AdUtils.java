package com.lw.internalmarkiting.ui.view;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.VideoOptions;
import com.google.android.gms.ads.formats.MediaView;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.lw.internalmarkiting.AdsApp;
import com.lw.internalmarkiting.R;
import com.lw.internalmarkiting.ads.AdHelper;

import static com.lw.internalmarkiting.BuildConfig.NATIVE_AD_ID;

public class AdUtils {
    private static AdUtils instance;
    private UnifiedNativeAd nativeAd;

    public static AdUtils getInstance() {
        if (instance == null) {
            instance = new AdUtils();
        }
        return instance;
    }

    public void showNativeAd(final UnifiedNativeAdView adView) {
        if (AdUtils.getInstance().getNativeAd() != null) {
            AdUtils.getInstance().populateUnifiedNativeAdView(AdUtils.getInstance().getNativeAd(), adView);
        } else {
            adView.setVisibility(View.GONE);
            AdUtils.getInstance().loadNativeAd(nativeAd -> AdUtils.getInstance().populateUnifiedNativeAdView(nativeAd, adView));
        }
    }

    public void loadNativeAd(final NativeAdLoadListener listener) {
        AdLoader.Builder builder = new AdLoader.Builder(AdsApp.getContext(), NATIVE_AD_ID);
        builder.forUnifiedNativeAd(unifiedNativeAd -> {
            if (listener != null) {
                listener.onNativeAdLoaded(unifiedNativeAd);
            }
            nativeAd = unifiedNativeAd;
        });

        VideoOptions videoOptions = new VideoOptions.Builder()
                .setStartMuted(true)
                .build();

        NativeAdOptions adOptions = new NativeAdOptions.Builder()
                .setVideoOptions(videoOptions)
                .build();

        builder.withNativeAdOptions(adOptions);
        AdLoader adLoader = builder.withAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int errorCode) {
//                ToastUtil.showErrorLog(TAG,"Native Ad error code = "+errorCode);
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
            }

            @Override
            public void onAdClicked() {
                super.onAdClicked();
            }

            @Override
            public void onAdImpression() {
                super.onAdImpression();
            }
        }).build();
        if (!adLoader.isLoading()) {
            adLoader.loadAd(AdHelper.getAdRequest());
        }
    }

    public UnifiedNativeAd getNativeAd() {
        return nativeAd;
    }

    public void populateUnifiedNativeAdView(UnifiedNativeAd nativeAd, UnifiedNativeAdView adView) {
        if (adView == null) return;
        adView.setVisibility(View.VISIBLE);

        MediaView mediaView = adView.findViewById(R.id.ad_media);

        adView.setMediaView(mediaView);

        // Register the view used for each individual asset.
        adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
        adView.setBodyView(adView.findViewById(R.id.ad_body));
        adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
        adView.setIconView(adView.findViewById(R.id.ad_app_icon));
        adView.setPriceView(adView.findViewById(R.id.ad_price));
//        adView.setStarRatingView(adView.findViewById(R.id.ad_stars));
        adView.setStoreView(adView.findViewById(R.id.ad_store));
//        adView.setAdvertiserView(adView.findViewById(R.id.ad_advertiser));

        // The headline is guaranteed to be in every UnifiedNativeAd.
        ((TextView) adView.getHeadlineView()).setText(nativeAd.getHeadline());
        adView.getHeadlineView().setSelected(true);

        // These assets aren't guaranteed to be in every UnifiedNativeAd, so it's important to
        // check before trying to display them.
        if (nativeAd.getBody() == null) {
            adView.getBodyView().setVisibility(View.INVISIBLE);
        } else {
            adView.getBodyView().setVisibility(View.VISIBLE);
            ((TextView) adView.getBodyView()).setText(nativeAd.getBody());
        }

        if (nativeAd.getCallToAction() == null) {
            adView.getCallToActionView().setVisibility(View.INVISIBLE);
        } else {
            adView.getCallToActionView().setVisibility(View.VISIBLE);
            ((Button) adView.getCallToActionView()).setText(nativeAd.getCallToAction());
        }

        if (nativeAd.getIcon() == null) {
            adView.findViewById(R.id.cvIcon).setVisibility(View.GONE);
//            adView.getIconView().setVisibility(View.GONE);
        } else {
            ((ImageView) adView.getIconView()).setImageDrawable(
                    nativeAd.getIcon().getDrawable());
            adView.findViewById(R.id.cvIcon).setVisibility(View.VISIBLE);
//            adView.getIconView().setVisibility(View.VISIBLE);
        }

        if (nativeAd.getPrice() == null) {
            adView.getPriceView().setVisibility(View.INVISIBLE);
        } else {
            adView.getPriceView().setVisibility(View.VISIBLE);
            ((TextView) adView.getPriceView()).setText(nativeAd.getPrice());
        }

        if (nativeAd.getStore() == null) {
            adView.getStoreView().setVisibility(View.INVISIBLE);
        } else {
            adView.getStoreView().setVisibility(View.VISIBLE);
            ((TextView) adView.getStoreView()).setText(nativeAd.getStore());
        }

        // This method tells the Google Mobile Ads SDK that you have finished populating your
        // native ad view with this native ad. The SDK will populate the adView's MediaView
        // with the media content from this native ad.
        adView.setNativeAd(nativeAd);

        // Get the video controller for the ad. One will always be provided, even if the ad doesn't
        // have a video asset.
        VideoController vc = nativeAd.getVideoController();

        // Updates the UI to say whether or not this ad has a video asset.

        vc.setVideoLifecycleCallbacks(new VideoController.VideoLifecycleCallbacks() {
            @Override
            public void onVideoEnd() {
                // Publishers should allow native ads to complete video playback before
                // refreshing or replacing them with another ad in the same UI location.
//                    refresh.setEnabled(true);
//                    ToastUtil.showErrorLog(TAG,"Video status: Video Playback has ended.");
                super.onVideoEnd();
            }
        });
    }
}
