package com.lw.internalmarkiting.ui.utils;

import android.os.Bundle;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.lw.internalmarkiting.AdsApp;

import static com.lw.internalmarkiting.ui.utils.AdUtils.INTERNAL_MARKETING;

public class Analytics {

    private static final String PROMO_CLICK = "PROMO_CLICK";
    public static final String CAMPAIGN = "campaign";
    public static final String PACKAGE_NAME = "app_package_name";

    private static FirebaseAnalytics analytics;

    public static void init() {
        analytics = FirebaseAnalytics.getInstance(AdsApp.getContext());
    }

    public static void logEvent(String packageName, String appName) {
        Bundle params = new Bundle();
        params.putString(packageName, appName);
        analytics.logEvent(PROMO_CLICK, params);
    }

    public static void logReferrerEvent(String packageName, String from) {
        Bundle params = new Bundle();
        params.putString(CAMPAIGN, from);
        params.putString(PACKAGE_NAME, packageName);
        analytics.logEvent(INTERNAL_MARKETING, params);
    }

    public static void logTypeEvent(String packageName,String appName, String type) {
        Bundle params = new Bundle();
        params.putString(packageName, appName);
        analytics.logEvent(type, params);
    }
}
