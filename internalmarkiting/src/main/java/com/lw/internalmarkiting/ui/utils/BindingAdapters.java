package com.lw.internalmarkiting.ui.utils;

import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;
import com.lw.internalmarkiting.AdsApp;

public class BindingAdapters {

    @BindingAdapter("icon")
    public static void setIcon(ImageView view, String url) {
        Glide.with(AdsApp.getContext()).load(url).into(view);
    }
}
