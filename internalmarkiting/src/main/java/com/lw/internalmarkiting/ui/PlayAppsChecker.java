package com.lw.internalmarkiting.ui;

import com.lw.internalmarkiting.exceptions.AccountNotFoundException;
import com.lw.internalmarkiting.exceptions.EmailNotFoundException;

import org.jetbrains.annotations.NotNull;

@SuppressWarnings("ALL")
public class PlayAppsChecker {

    public static final String LOGIC_WORMS_APPS = "Logic Worms Apps";
    public static final String LASH_PASH_APPS = "Lash Pash Apps";
    public static final String CHRIS_WILLIAMS = "Chris Williams";
    public static final String SMART_GORILLA = "Smart Gorilla";
    public static final String BLUE_RAY_APPS = "Blue Ray Apps";
    public static final String SOFT_TECHNOLOGIZ = "Soft Technologiz";
    public static final String TECHNOLOGICS = "Technologics";
    public static final String NONE = "";

    private static boolean isLogicWormApp(@NotNull String packageName) {
        switch (packageName) {
            case "com.tapslide.slideshowmaker.photoslideshow.photovideomaker":
            case "com.tm.radiation.meetr":
            case "com.tm.flashlight.call.and.sms":
            case "com.logicworms.liedetector":
            case "com.tm.barcode.reader":
            case "com.logicworms.toolkit.toolsbox.alltools.smarttools":
            case "logicwarms.com.netspeedtest":
            case "com.logicworms.quizzer.testskills.learn.quizapp":
            case "com.tm.secure.call":
            case "com.food.chicken.cooking.KitchenChiefRecipes":
            case "com.tm.caller.name.announcer":
            case "com.logicworms.freevpn.supervpn.turbovpn.proxymaster":
            case "com.uv.light.simulator.camera.affect.bulb":
                return true;
        }
        return false;
    }

    private static boolean isLashPashApp(@NotNull String packageName) {
        switch (packageName) {
            case "com.lpa.screencallerid.callertheme":
            case "com.lpa.luncher.s7.edge":
            case "com.lpa.whatsdownload.unseen":
            case "com.lpa.secure.call":
            case "com.LashPashApps.DontTouchMyPhone.AntiTheftAlarm":
            case "com.lpa.noisedetector.soundmeter.dbmeter":
            case "com.lpa.flash.notification":
            case "com.lpa.photoeditor.collagemaker":
            case "com.lpa.cna":
            case "com.lpa.luncher.s6.edge":
            case "com.lpa.flashlight.alert.call.and.sms":
            case "com.lpa.cartoonphotoeditor.pencilsketch.pencilart.sketchapp":
            case "com.lpa.samsung.galaxy.s8.edge.launcher.theme":
            case "com.lpa.video.thumbnail.maker.fb.cover.photo.creator":
                return true;
        }
        return false;
    }

    private static boolean isChrisWilliamsApp(@NotNull String packageName) {
        switch (packageName) {
            case "com.chriswilliams.frequencysound.frequencygenerator":
            case "com.chrisswilliams.fake.callerid.fakecall.prankcall.app":
            case "com.chrisswilliams.uv.light.simulator.uv.camera.uv.lamp.light":
            case "com.chrisswilliams.hiddenchat.nolastseen.hidestatus.unseen":
            case "com.gwnt.flashlight.alert.call.and.sms":
            case "com.gwnt.name.announcer":
            case "com.gwnt.secure.applock":
                return true;
        }
        return false;
    }

    private static boolean isSmartGorillaApp(@NotNull String packageName) {
        switch (packageName) {
            case "com.sg.share.all":
            case "dev.wahid.quotesforu":
            case "com.sg.flash.on.call.and.sms":
            case "com.sg.whatsdowanload.unseen":
            case "com.homeworkout.fitness.exercise.stayhome.stayfit.app":
            case "com.smartgorilla.drs.deletedphotorecovery.videorecovery.datarecoverysoftware":
                return true;
        }
        return false;
    }

    private static boolean isBlueRayApp(@NotNull String packageName) {
        switch (packageName) {
            case "com.br.whatsdowanload.unseen":
            case "com.br.flashlight.flashalert.call.and.sms":
            case "com.blueray.fakecalls.prankcall.fakecallerid":
            case "com.br.spiritlevel.bubblelevel.preciselevel":
            case "com.br.samsung.galaxy.s7.edge.launcher.theme":
            case "com.br.screencallerid.callertheme.callscreen":
            case "com.blueray.junkcleaner.booster.cpucooler":
            case "com.br.freesticker.cutestickers.stickers":
            case "com.br.metaldetector.goldfinder.silverfinder":
            case "com.br.call.secure.applock":
            case "com.br.samsung.galaxy.j7.prime.launcher.theme":
            case "com.br.caller.name.announcer":
            case "com.statussaver.videosaver":
            case "com.postermaker.flyermaker.desginmaker.graphicdesign":
            case "com.oldage.face.oldfacemaker.faceapp":
                return true;
        }
        return false;
    }

    private static boolean isSoftTechnologizApp(@NotNull String packageName) {
        switch (packageName) {
            case "com.softtechnologiz.fakecallerid.prankcall":
            case "com.softtechnologiz.radiationmeter.radiationdetector":
            case "com.softtechnologiz.altameter":
            case "com.logicworms.barcodereader":
            case "com.softtechnologiz.metaldetector.goldfinder":
            case "com.softtechnologiz.dbmeter.deciblemeter.soundmeter":
                return true;
        }
        return false;
    }

    private static boolean isTechnologicsApp(@NotNull String packageName) {
        switch (packageName) {
            case "com.allformat.video.player.app":
            case "com.musicplayer.equalizer.bassbooster.audioplayer":
            case "com.technologics.ultraviolet.light.simulation.app":
            case "com.technologic.supercleaner.phonebooster.junkcleaner":
            case "com.technologics.social.statussaver.storysaver.downloader":
            case "com.technologics.fakecallerid.prankcall.fakecall":
                return true;
        }
        return false;
    }

    @NotNull
    static String getAccountName(@NotNull String packageName) {
        if (isLogicWormApp(packageName)) {
            return LOGIC_WORMS_APPS;
        }
        if (isLashPashApp(packageName)) {
            return LASH_PASH_APPS;
        }
        if (isChrisWilliamsApp(packageName)) {
            return CHRIS_WILLIAMS;
        }
        if (isSmartGorillaApp(packageName)) {
            return SMART_GORILLA;
        }
        if (isBlueRayApp(packageName)) {
            return BLUE_RAY_APPS;
        }
        if (isSoftTechnologizApp(packageName)) {
            return SOFT_TECHNOLOGIZ;
        }
        if (isTechnologicsApp(packageName)) {
            return TECHNOLOGICS;
        }
        throw new AccountNotFoundException("Account Name for this package name is not found. If this is a new app then add its package name to the specific method for generating Required Account Name.");
    }

    @NotNull
    static String getAccountEmail(@NotNull String accountName) {
        switch (accountName) {
            default:
                throw new EmailNotFoundException("Email for this account is not found. If this is a new account then add new case for this account.");
            case LOGIC_WORMS_APPS:
                return "10milliondownloads@gmail.com";
            case LASH_PASH_APPS:
                return "lashpashapps@gmail.com";
            case CHRIS_WILLIAMS:
                return "chris.williams20144@gmail.com";
            case SMART_GORILLA:
                return "smartgorillaapps@gmail.com";
            case BLUE_RAY_APPS:
                return "blue.ray088@gmail.com";
            case SOFT_TECHNOLOGIZ:
                return "softtechnologiz@gmail.com";
            case TECHNOLOGICS:
                return "allvideodownloader636@gmail.com";
        }
    }
}