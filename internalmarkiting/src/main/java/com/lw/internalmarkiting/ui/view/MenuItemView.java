package com.lw.internalmarkiting.ui.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.lw.internalmarkiting.R;

public class MenuItemView extends FrameLayout {

    public static final boolean ATTACH_TO_ROOT = false;
    private TextView textView;

    private View.OnClickListener listener;
    private MenuItemView root;

    public MenuItemView(Context context) {
        this(context, null);
    }

    public MenuItemView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MenuItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        root = this;

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.MenuItemView, defStyleAttr, 0);

        int menuDrawable = a.getResourceId(R.styleable.MenuItemView_menuDrawable, R.drawable.ic_notifications);
        String menuText = a.getString(R.styleable.MenuItemView_menuText);
        a.recycle();

        View view = LayoutInflater.from(context).inflate( R.layout.menu_item, root, ATTACH_TO_ROOT);
        addView(view);

        ImageView imageViewMenu = view.findViewById(R.id.imageViewMenu);
        imageViewMenu.setImageResource(menuDrawable);
        textView = view.findViewById(R.id.textView);
        textView.setText(menuText);

        view.setOnClickListener(v -> {
            if (listener != null)
                listener.onClick(root);
        });
    }

    public void setText(String text) {
        if (textView != null && text != null) {
            textView.setText(text);
        }
    }

    @Override
    public void setOnClickListener(@Nullable OnClickListener l) {
        listener = l;
    }
}
