package com.lw.internalmarkiting.ui.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.target.Target;

import org.jetbrains.annotations.NotNull;

import java.util.concurrent.ExecutionException;

public enum GlideHelper {
    ;

    public static void load(Context context, int res, ImageView target, CallBack listener) {
        Glide.with(context.getApplicationContext()).load(res).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                listener.onFailed();
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                listener.onSuccess();
                return false;
            }
        }).into(target);
    }

    public static void load(Context context, String res, ImageView target, CallBack listener) {
        Glide.with(context.getApplicationContext()).load(res).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                listener.onFailed();
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                listener.onSuccess();
                return false;
            }
        }).into(target);
    }

    public static void load(Context context, int res, ImageView target) {
        Glide.with(context.getApplicationContext()).load(res).into(target);
    }

    public interface BitmapLoadedListener{
        void onBitmapLoaded(Bitmap bitmap);
    }
    public static void loadImgWithout(Context context,String path, ImageView target){
        Glide.with(context)
                .load(path)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(target);
    }

    public static void loadImgWithout(Context context, String path, CustomTarget<Bitmap> callBack){

        Glide.with(context)
                .asBitmap()
                .load(path)
                .into(callBack);

    }
    public static void loadImg(Context context,String path, ImageView target){
        Glide.with(context)
                .load(path)
                .into(target);
    }




    public interface CallBack {
        void onFailed();

        void onSuccess();
    }
}
