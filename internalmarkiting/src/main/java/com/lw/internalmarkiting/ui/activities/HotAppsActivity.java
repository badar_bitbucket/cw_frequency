package com.lw.internalmarkiting.ui.activities;

import android.content.Context;
import android.content.Intent;

import androidx.appcompat.app.ActionBar;

import com.lw.internalmarkiting.BuildConfig;
import com.lw.internalmarkiting.R;
import com.lw.internalmarkiting.data.model.PromoApp;
import com.lw.internalmarkiting.databinding.ActivityHotAppsBinding;
import com.lw.internalmarkiting.task.AdsTask;
import com.lw.internalmarkiting.task.SingleValueCallback;
import com.lw.internalmarkiting.ui.adapters.PromoAppsAdapter;

import org.jetbrains.annotations.NotNull;

import timber.log.Timber;

import static android.view.View.GONE;

public class HotAppsActivity extends BaseActivity<ActivityHotAppsBinding> {
    protected static final String EXTRA_VIDEO_AD = "EXTRA_VIDEO_AD";
    private boolean showVideoAd;

    public static void start(@NotNull Context context, boolean showVideoAd) {
        Intent intent = new Intent(context, HotAppsActivity.class);
        intent.putExtra(EXTRA_VIDEO_AD, showVideoAd);
        context.startActivity(intent);
    }

    @Override
    protected int getResId() {
        return R.layout.activity_hot_apps;
    }

    @Override
    protected void onReady() {
        boolean showVideoAd = getIntent().getBooleanExtra(EXTRA_VIDEO_AD, false);
        setSupportActionBar(binding.toolbar);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            supportActionBar.setDisplayShowHomeEnabled(true);
        }
        PromoAppsAdapter adapter = new PromoAppsAdapter();
        binding.setAdapter(adapter);
        binding.setShowVideoAd(showVideoAd);
        AdsTask.loadHotApps(new SingleValueCallback<PromoApp>() {
            @Override
            public void onValue(PromoApp promoApp) {
                Timber.i("Promo Apps : %s", promoApp);
                if (promoApp.isAppNotInstalled())
                    adapter.addPromoApp(promoApp);
                binding.progressBar.setVisibility(GONE);
            }

            @Override
            public void onComplete() {
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}