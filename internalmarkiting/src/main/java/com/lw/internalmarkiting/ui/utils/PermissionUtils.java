package com.lw.internalmarkiting.ui.utils;

import android.Manifest;
import android.app.Activity;
import android.app.AppOpsManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Process;

import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;

import com.lw.internalmarkiting.AdsApp;

import org.jetbrains.annotations.NotNull;

import static android.app.AppOpsManager.MODE_ALLOWED;
import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class PermissionUtils {

    public static final int REQUEST_WRITE_EXTERNAL_STORAGE = 4_000;
    public static final int REQUEST_READ_CONTACTS = 4_001;
    public static final int REQUEST_CAMERA = 4_002;

    private static final String ACTION_NOTIFICATION_LISTENER_SETTINGS = "android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS";
    private static final int REQUEST_CODE_NOTIFICATION = 93;
    private static final String ACTION_USAGE_ACCESS_SETTINGS = "android.settings.USAGE_ACCESS_SETTINGS";

    public static boolean hasStoragePermission() {
        return ContextCompat.checkSelfPermission(AdsApp.getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED;
    }

    public static void showStoragePermission(Activity activity) {
        ActivityCompat.requestPermissions(activity,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                REQUEST_WRITE_EXTERNAL_STORAGE);
    }

    public static boolean hasCameraPermission() {
        return ContextCompat.checkSelfPermission(AdsApp.getContext(), Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED;
    }

    public static void showCameraPermission(Activity activity) {
        ActivityCompat.requestPermissions(activity,
                new String[]{Manifest.permission.CAMERA},
                REQUEST_CAMERA);
    }

    public static boolean hasReadContactsPermission() {
        return ContextCompat.checkSelfPermission(AdsApp.getContext(), Manifest.permission.READ_CONTACTS)
                == PackageManager.PERMISSION_GRANTED;
    }

    public static void showReadContactsPermission(Activity activity) {
        ActivityCompat.requestPermissions(activity,
                new String[]{Manifest.permission.READ_CONTACTS},
                REQUEST_READ_CONTACTS);
    }

    public static boolean hasNotificationAccess(Context context) {
        return NotificationManagerCompat.getEnabledListenerPackages(context).contains(context.getPackageName());
    }

    public static void openNotificationAccess(@NotNull Activity activity) {
        Intent intent = new Intent(ACTION_NOTIFICATION_LISTENER_SETTINGS);
        intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
        activity.startActivityForResult(intent, REQUEST_CODE_NOTIFICATION);
    }

    public static boolean hasUsageDataAccess() {
        AppOpsManager appOps = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            appOps = (AppOpsManager) AdsApp.getContext().getSystemService(Context.APP_OPS_SERVICE);
        }
        int mode = 0;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (appOps != null) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    mode = appOps.unsafeCheckOpNoThrow(AppOpsManager.OPSTR_GET_USAGE_STATS,
                            Process.myUid(), AdsApp.getContext().getPackageName());
                } else {
                    mode = appOps.checkOpNoThrow(AppOpsManager.OPSTR_GET_USAGE_STATS,
                            Process.myUid(), AdsApp.getContext().getPackageName());
                }
            }
            return mode == MODE_ALLOWED;
        } else return true;
    }

    public static void openUsageDataAccess() {
        Intent intent = new Intent(ACTION_USAGE_ACCESS_SETTINGS);
        intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
        AdsApp.getContext().startActivity(intent);
    }
}
