package com.lw.internalmarkiting.ads;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.lifecycle.DefaultLifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ProcessLifecycleOwner;

import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.appopen.AppOpenAd;
import com.lw.internalmarkiting.AdsApp;
import com.lw.internalmarkiting.BuildConfig;

import java.util.Date;

import timber.log.Timber;

/**
 * Prefetches App Open Ads.
 */
public class AppOpenManager implements Application.ActivityLifecycleCallbacks {

    private static boolean isShowingAd = false;
    private final AdsApp myApplication;
    private AppOpenAd appOpenAd = null;
    private AppOpenAd.AppOpenAdLoadCallback loadCallback;
    private Activity currentActivity;
    private long loadTime = 0;

    /**
     * Constructor
     */
    public AppOpenManager(AdsApp myApplication) {
        Timber.i("AppOpenManager Constructor");
        this.myApplication = myApplication;
        this.myApplication.registerActivityLifecycleCallbacks(this);
        ProcessLifecycleOwner.get().getLifecycle().addObserver(new DefaultLifecycleObserver() {
            /** LifecycleObserver methods */
            @Override
            public void onStart(@NonNull LifecycleOwner owner) {
                Timber.i("onStart");
                showAdIfAvailable();
            }
        });
    }

    /**
     * Utility method to check if ad was loaded more than n hours ago.
     */
    private boolean wasLoadTimeLessThanNHoursAgo(long numHours) {
        long dateDifference = (new Date()).getTime() - this.loadTime;
        long numMilliSecondsPerHour = 3600000;
        return (dateDifference < (numMilliSecondsPerHour * numHours));
    }

    /**
     * Utility method that checks if ad exists and can be shown.
     */
    public boolean isAdAvailable() {
        Timber.i("isAdAvailable");
        int fourHours = 4;
        return appOpenAd != null && wasLoadTimeLessThanNHoursAgo(fourHours);
    }

    /**
     * Request an ad
     */
    public void fetchAd() {
        Timber.i("fetchAd");

        if (!AdsApp.enableAds)
            return;

        // Have unused ad, no need to fetch another.
        if (isAdAvailable()) {
            return;
        }

        loadCallback = new AppOpenAd.AppOpenAdLoadCallback() {
            /**
             * Called when an app open ad has loaded.
             *
             * @param ad the loaded app open ad.
             */
            @Override
            public void onAppOpenAdLoaded(AppOpenAd ad) {
                Timber.i("onAppOpenAdLoaded");
                AppOpenManager.this.appOpenAd = ad;
                AppOpenManager.this.loadTime = (new Date()).getTime();
            }

            /**
             * Called when an app open ad has failed to load.
             *
             * @param loadAdError the error.
             */
            @Override
            public void onAppOpenAdFailedToLoad(LoadAdError loadAdError) {
                // Handle the error.
                Timber.i("onAppOpenAdFailedToLoad");
            }

        };
        AdRequest request = AdHelper.getAdRequest();
        AppOpenAd.load(myApplication, BuildConfig.OPEN_APP_AD_ID, request,
                AppOpenAd.APP_OPEN_AD_ORIENTATION_PORTRAIT, loadCallback);
    }

    /**
     * Shows the ad if one isn't already showing.
     */
    public void showAdIfAvailable() {
        Timber.i("showAdIfAvailable");
        if (!AdsApp.enableAds)
            return;
        // Only show ad if there is not already an app open ad currently showing
        // and an ad is available.
        if (!isShowingAd && isAdAvailable()) {
            Timber.i("Will show ad.");

            FullScreenContentCallback fullScreenContentCallback =
                    new FullScreenContentCallback() {
                        @Override
                        public void onAdDismissedFullScreenContent() {
                            Timber.i("onAdDismissedFullScreenContent.");
                            // Set the reference to null so isAdAvailable() returns false.
                            AppOpenManager.this.appOpenAd = null;
                            isShowingAd = false;
                            fetchAd();
                        }

                        @Override
                        public void onAdFailedToShowFullScreenContent(AdError adError) {
                            Timber.i("onAdFailedToShowFullScreenContent.");
                        }

                        @Override
                        public void onAdShowedFullScreenContent() {
                            Timber.i("onAdShowedFullScreenContent.");
                            isShowingAd = true;
                        }
                    };

            appOpenAd.show(currentActivity, fullScreenContentCallback);

        } else {
            Timber.i("Can not show ad.");
            fetchAd();
        }
    }

    /**
     * ActivityLifecycleCallback methods
     */
    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        Timber.i("onActivityCreated : %s", activity.getClass().getSimpleName());
    }

    @Override
    public void onActivityStarted(Activity activity) {
        Timber.i("onActivityStarted : %s", activity.getClass().getSimpleName());
        currentActivity = activity;
    }

    @Override
    public void onActivityResumed(Activity activity) {
        Timber.i("onActivityResumed : %s", activity.getClass().getSimpleName());
        currentActivity = activity;
    }

    @Override
    public void onActivityStopped(Activity activity) {
        Timber.i("onActivityStopped : %s", activity.getClass().getSimpleName());
    }

    @Override
    public void onActivityPaused(Activity activity) {
        Timber.i("onActivityPaused : %s", activity.getClass().getSimpleName());
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        Timber.i("onActivitySaveInstanceState : %s", activity.getClass().getSimpleName());
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        Timber.i("onActivityDestroyed : %s", activity.getClass().getSimpleName());
        currentActivity = null;
    }
}
