package com.lw.internalmarkiting.ads;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.RequestConfiguration;
import com.lw.internalmarkiting.BuildConfig;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class AdHelper {

    public static final String TEST_DEVICE_J3 = "AACDC053ADA7A4473AAF9712C39A767F";
    public static final String TEST_DEVICE_J3_RELEASE = "9A9B13231E911C38B61A620F1751B885";
    public static final String TEST_UMER_J7_MAX = "18AC18D00C30123D38021338B58EC6E6";
    public static final String TEST_DEVICE_BADAR_BHAI = "EEAF5F69F60EBF12C057A08176C73C2C";

    private static final List<String> TEST_IDs = new ArrayList<>();

    private static AdRequest request;

    static {
        TEST_IDs.add(TEST_DEVICE_J3);
        TEST_IDs.add(TEST_DEVICE_J3_RELEASE);
        TEST_IDs.add(TEST_UMER_J7_MAX);
        TEST_IDs.add(TEST_DEVICE_BADAR_BHAI);
    }

    @NotNull
    public static AdRequest getAdRequest() {
        if (request == null) {
            request = new AdRequest.Builder().build();
        }
        return request;
    }

    public static RequestConfiguration getConfiguration() {
        RequestConfiguration.Builder builder = new RequestConfiguration.Builder();
        if (BuildConfig.DEBUG)
            builder.setTestDeviceIds(TEST_IDs);
        return builder.build();
    }
}