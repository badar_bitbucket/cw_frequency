package com.lw.internalmarkiting.ads;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.SOURCE;

public interface AccountIds {
    int NONE = 1;
    int SMART_GORILLA = 2;
    int BLUE_RAY = 3;
    int CHRIS_WILLIAMS = 4;
    int LASH_PASH = 5;
    int LOGIC_WORMS = 6;
    int SOFT_TECH = 7;
    int TECHNO_LOGICS = 8;

    @Retention(SOURCE)
    @IntDef({NONE, SMART_GORILLA, BLUE_RAY, CHRIS_WILLIAMS, LASH_PASH, LOGIC_WORMS, SOFT_TECH, TECHNO_LOGICS})
    public @interface AccountId {
    }
}
