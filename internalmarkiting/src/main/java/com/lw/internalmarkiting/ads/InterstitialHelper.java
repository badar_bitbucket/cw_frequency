package com.lw.internalmarkiting.ads;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.InterstitialAd;
import com.lw.internalmarkiting.AdsApp;
import com.lw.internalmarkiting.OnAdCloseListener;

import static com.lw.internalmarkiting.BuildConfig.INTERSTITIAL_AD_ID;

public enum InterstitialHelper {
    ;

    private static InterstitialAd interstitialAd;

    public static void init() {
        if (AdsApp.enableAds) {
            if (interstitialAd == null) {
                interstitialAd = new InterstitialAd(AdsApp.getContext());
                interstitialAd.setAdUnitId(INTERSTITIAL_AD_ID);
            }
            loadAd();
        }
    }

    private static void loadAd() {
        if (AdsApp.enableAds && interstitialAd != null && !interstitialAd.isLoaded()) {
            interstitialAd.loadAd(AdHelper.getAdRequest());
        }
    }

    public static void showAd(OnAdCloseListener listener) {
        if (AdsApp.enableAds && interstitialAd != null && interstitialAd.isLoaded()) {
            interstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdClosed() {
                    loadAd();
                    if (listener != null) listener.onAdClosed();
                }
            });
            interstitialAd.show();
        } else {
            loadAd();
            if (listener != null) listener.onAdClosed();
        }
    }

    public static void showAd() {
        showAd(null);
    }
}