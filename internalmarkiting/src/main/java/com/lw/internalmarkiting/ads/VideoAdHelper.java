package com.lw.internalmarkiting.ads;

import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.lw.internalmarkiting.AdsApp;
import com.lw.internalmarkiting.OnAdCloseListener;

import static com.lw.internalmarkiting.BuildConfig.VIDEO_AD_ID;

public enum VideoAdHelper {
    ;

    private static RewardedVideoAd videoAd;

    public static void init() {
        if (AdsApp.enableAds) {
            if (videoAd == null)
                videoAd = MobileAds.getRewardedVideoAdInstance(AdsApp.getContext());
            loadAd();
        }
    }

    private static void loadAd() {
        if (AdsApp.enableAds && videoAd != null && !videoAd.isLoaded()) {
            videoAd.loadAd(VIDEO_AD_ID, AdHelper.getAdRequest());
        }
    }

    public static void showAd(OnAdCloseListener listener) {
        if (AdsApp.enableAds && videoAd != null && videoAd.isLoaded()) {
            videoAd.setRewardedVideoAdListener(new VideoAdListener() {
                @Override
                public void onRewardedVideoAdClosed() {
                    loadAd();
                    if (listener != null) listener.onAdClosed();
                }

                @Override
                public void onRewarded(RewardItem rewardItem) {
                    if (listener != null) listener.onRewarded();
                }
            });
            videoAd.show();
        } else {
            loadAd();
            if (listener != null) listener.onAdClosed();
        }
    }

    public static void showAd() {
        showAd(null);
    }
}