package com.lw.internalmarkiting.ads;

import com.lw.internalmarkiting.AdsApp;
import com.lw.internalmarkiting.OnAdCloseListener;
import com.lw.internalmarkiting.data.model.PromoApp;
import com.lw.internalmarkiting.task.AdsTask;
import com.lw.internalmarkiting.ui.Common;
import com.lw.internalmarkiting.ui.activities.InterstitialActivity;

import java.util.HashSet;
import java.util.Set;

import timber.log.Timber;

public enum LwInterstitialHelper {
    ;

    private static boolean isLoaded;
    private static Set<PromoApp> promoApps;
    private static OnAdCloseListener closeListener;

    public static void init() {
        if (!AdsApp.enableAds)
            return;
        Timber.i("init");
        promoApps = new HashSet<>();
        loadAd();
    }

    private static void loadAd() {
        if (AdsApp.enableAds) {
            if (!isLoaded) {
                AdsTask.loadInterstitialPromoApps(promoApp -> {
                    isLoaded = true;
                    Timber.i("Promo received : %s", promoApp);
                    promoApps.add(promoApp);
                });
            }
        }
    }

    public static void showAd(OnAdCloseListener listener) {
        closeListener = listener;
        if (AdsApp.enableAds && isLoaded && promoApps.size() > 0) {
            InterstitialActivity.start();
        } else {
            onClose();
        }
    }

    public static void showAd() {
        showAd(null);
    }

    public static void onClose() {
        if (closeListener != null) closeListener.onAdClosed();
    }

    public static PromoApp getAd() {
        PromoApp promoApp = null;
        for (PromoApp adModel : promoApps) {
            if (promoApp == null) {
                promoApp = adModel;
            } else {
                if (Common.isAppInstalled(adModel.getPackageName()))
                    promoApp = adModel;
            }
        }
        return promoApp;
    }
}
