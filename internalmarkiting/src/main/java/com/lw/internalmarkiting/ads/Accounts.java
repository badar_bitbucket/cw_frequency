package com.lw.internalmarkiting.ads;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

public enum Accounts {
    NONE(AccountIds.NONE),
    SMART_GORILLA(AccountIds.SMART_GORILLA),
    BLUE_RAY(AccountIds.BLUE_RAY),
    CHRIS_WILLIAMS(AccountIds.CHRIS_WILLIAMS),
    LASH_PASH(AccountIds.LASH_PASH),
    LOGIC_WORMS(AccountIds.LOGIC_WORMS),
    SOFT_TECH(AccountIds.SOFT_TECH),
    TECHNO_LOGICS(AccountIds.TECHNO_LOGICS);

    int accountId;

    @Contract(pure = true)
    Accounts(@AccountIds.AccountId int accountId) {
        this.accountId = accountId;
    }

    @Contract(pure = true)
    @NotNull
    public static String getAccountName(@NotNull String accountName) {
        switch (accountName) {
            case "Blue Ray Apps":
                return "Blue+Ray+Apps";
            case "Logic Worms Apps":
                return "Logic+Worms+Apps";
            case "LashPash Apps":
                return "LashPash+Apps";
            case "Soft Technologiz":
                return "Soft+Technologiz";
            case "Smart Gorilla":
                return "Smart+Gorilla";
            case "Chris Williams":
                return "Chris+Williams";
            case "Technologics":
                return "Technologics";
            default:
                return "account name";
        }
    }
}
