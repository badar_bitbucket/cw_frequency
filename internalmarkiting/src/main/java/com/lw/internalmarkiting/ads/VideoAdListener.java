package com.lw.internalmarkiting.ads;

import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

import timber.log.Timber;

public class VideoAdListener implements RewardedVideoAdListener {
    @Override
    public void onRewardedVideoAdLoaded() {
        Timber.i("onRewardedVideoAdLoaded");
    }

    @Override
    public void onRewardedVideoAdOpened() {
        Timber.i("onRewardedVideoAdOpened");
    }

    @Override
    public void onRewardedVideoStarted() {
        Timber.i("onRewardedVideoStarted");
    }

    @Override
    public void onRewardedVideoAdClosed() {
        Timber.i("onRewardedVideoAdClosed");
    }

    @Override
    public void onRewarded(RewardItem rewardItem) {
        Timber.i("onRewarded");
    }

    @Override
    public void onRewardedVideoAdLeftApplication() {
        Timber.i("onRewardedVideoAdLeftApplication");
    }

    @Override
    public void onRewardedVideoAdFailedToLoad(int i) {
        Timber.i("onRewardedVideoAdFailedToLoad");
    }

    @Override
    public void onRewardedVideoCompleted() {
        Timber.i("onRewardedVideoCompleted");
    }
}
