package com.lw.internalmarkiting.data.room;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.lw.internalmarkiting.data.model.PromoApp;
import com.lw.internalmarkiting.data.model.PromoType;

import java.util.List;

@Dao
public interface PromoAppDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long addPromo(PromoApp promoApp);

    @Update
    void update(PromoApp promoApp);

    @Delete
    void delete(PromoApp promoApp);

    @Query("SELECT COUNT(packageName) FROM PromoApp WHERE type= :type")
    int getCount(@PromoType.PromoAppType String type);

    @Query("SELECT COUNT(packageName) FROM PromoApp")
    int getAllPromoCount();

    @Query("SELECT * FROM PromoApp WHERE type= :type")
    List<PromoApp> getAllPromo(String type);

    @Query("DELETE FROM PromoApp WHERE type= :type")
    void clearApps(String type);
}
