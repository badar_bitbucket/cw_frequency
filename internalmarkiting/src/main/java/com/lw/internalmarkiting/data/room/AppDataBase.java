package com.lw.internalmarkiting.data.room;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.lw.internalmarkiting.data.model.PromoApp;

import org.jetbrains.annotations.NotNull;


@Database(entities = {PromoApp.class}, version = 4)
public abstract class AppDataBase extends RoomDatabase {

    private static final String DB_NAME = "AdManager";

    private static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(@NotNull SupportSQLiteDatabase database) {
            database.execSQL(
                    "CREATE TABLE IF NOT EXISTS `PromoApp` (`packageName` TEXT NOT NULL, `type` TEXT NOT NULL, `adId` INTEGER NOT NULL, `appName` TEXT, `appDescription` TEXT, `imageUrl` TEXT, `featureUrl` TEXT, PRIMARY KEY(`packageName`, `type`))"
            );
        }
    };

    private static final Migration MIGRATION_2_3 = new Migration(2, 3) {
        @Override
        public void migrate(@NotNull SupportSQLiteDatabase database) {
            database.execSQL(
                    "ALTER TABLE `PromoApp` ADD COLUMN 'accountName' TEXT NOT NULL DEFAULT ''"
            );
            database.execSQL("DROP TABLE `HotApp`");
        }
    };

    @NonNull
    static AppDataBase getDatabase(@NonNull Context context) {
        return Room.databaseBuilder(context.getApplicationContext(),
                AppDataBase.class, DB_NAME)
                .fallbackToDestructiveMigration()
                .build();
    }

    public abstract PromoAppDao promoAppDao();
}
