package com.lw.internalmarkiting.data.room;

import android.content.Context;
import android.os.Build;

import com.lw.internalmarkiting.data.model.PromoApp;
import com.lw.internalmarkiting.data.model.PromoType;
import com.lw.internalmarkiting.task.CountCallback;
import com.lw.internalmarkiting.task.PromoCallback;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import timber.log.Timber;

public class RepositoryImpl implements Repository {

    private final AppDataBase dataBase;

    public RepositoryImpl(Context context) {
        dataBase = AppDataBase.getDatabase(context);
    }

    @Override
    public void addPromoApp(PromoApp promoApp) {
        Timber.i("Saving : %s", promoApp);
        dataBase.promoAppDao().addPromo(promoApp);
    }

    @Override
    public void getPromoCount(@PromoType.PromoAppType String type, @NotNull CountCallback callback) {
        callback.onValue(getInstallableApps(type));
    }

    private int getInstallableApps(@PromoType.PromoAppType String type) {
        return (int) dataBase.promoAppDao()
                .getAllPromo(type)
                .stream().filter(PromoApp::isAppNotInstalled)
                .count();
    }

    public void clearApps(@PromoType.PromoAppType String type){
        dataBase.promoAppDao().clearApps(type);
    }


    public void loadPromoApp(@PromoType.PromoAppType String type, @NotNull PromoCallback callback) {
        int count = dataBase.promoAppDao().getCount(type);
        callback.promoCount(count);
        List<PromoApp> promoApps = dataBase.promoAppDao().getAllPromo(type);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            promoApps.forEach(callback::onNext);
        } else {
            for (PromoApp promoApp : promoApps) {
                callback.onNext(promoApp);
            }
        }
        callback.onComplete();
    }
}