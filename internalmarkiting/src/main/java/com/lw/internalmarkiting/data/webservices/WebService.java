package com.lw.internalmarkiting.data.webservices;

import com.lw.internalmarkiting.data.model.PromoType;
import com.lw.internalmarkiting.task.PromoDownloadCallback;

public interface WebService {

    void onAllAppsClick(int adId);

    void onHotExitAppsClick(int adId);

    void downloadPromoApps(@PromoType.PromoAppType String type, PromoDownloadCallback callback);
}
