package com.lw.internalmarkiting.data.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;

import com.lw.internalmarkiting.ads.Accounts;
import com.lw.internalmarkiting.task.AdsTask;
import com.lw.internalmarkiting.ui.Common;
import com.lw.internalmarkiting.ui.utils.Analytics;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import static com.lw.internalmarkiting.data.model.PromoType.NONE;
import static com.lw.internalmarkiting.ui.utils.AdUtils.openAccount;
import static com.lw.internalmarkiting.ui.utils.AdUtils.openPlayStore;

@SuppressWarnings("NullableProblems")
@Entity(primaryKeys = {"packageName", "type"})
public class PromoApp {

    @NotNull
    private String packageName = "";
    @NotNull
    private String accountName = "";
    @NotNull
    @PromoType.PromoAppType
    private String type = NONE;
    private int adId;
    private String appName;
    private String appDescription;
    private String imageUrl;
    private String featureUrl;

    @Contract(pure = true)
    public PromoApp() {
    }

    @Contract(pure = true)
    @Ignore
    public PromoApp(String appName, String appDescription, @NotNull String packageName,
                    String imageUrl, String featureUrl, int adId,
                    @NotNull @PromoType.PromoAppType String type) {
        this.appName = appName;
        this.appDescription = appDescription;
        this.packageName = packageName;
        this.imageUrl = imageUrl;
        this.featureUrl = featureUrl;
        this.adId = adId;
        this.type = type;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public int getAdId() {
        return adId;
    }

    public void setAdId(int adId) {
        this.adId = adId;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppDescription() {
        return appDescription;
    }

    public void setAppDescription(String appDescription) {
        this.appDescription = appDescription;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getFeatureUrl() {
        return featureUrl;
    }

    public void setFeatureUrl(String featureUrl) {
        this.featureUrl = featureUrl;
    }

    @Contract(value = "null -> false", pure = true)
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PromoApp promoApp = (PromoApp) o;

        return getPackageName().equals(promoApp.getPackageName());
    }

    @Override
    public int hashCode() {
        return getPackageName().hashCode();
    }

    @NotNull
    @Override
    public String toString() {
        return "PromoApp{" +
                "packageName='" + packageName + '\'' +
                ", adId=" + adId +
                ", appName='" + appName + '\'' +
                ", appDescription='" + appDescription + '\'' +
                ", type='" + type + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", featureUrl='" + featureUrl + '\'' +
                '}';
    }

    public void onClick() {
        openPlayStore(getPackageName());
        AdsTask.promoAppClick(type, adId);
        Analytics.logEvent(getPackageName(), getAppName());
        Analytics.logTypeEvent(getPackageName(), getAppName(), type);
    }

    public void onAccountNameClicked() {
        openAccount(Accounts.getAccountName(accountName));
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(@NotNull String accountName) {
        this.accountName = accountName;
    }

    public boolean isAppInstalled() {
        return Common.isAppInstalled(packageName);
    }

    public boolean isAppNotInstalled() {
        return !Common.isAppInstalled(packageName);
    }
}
