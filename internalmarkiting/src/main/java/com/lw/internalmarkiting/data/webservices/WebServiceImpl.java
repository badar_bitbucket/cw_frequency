package com.lw.internalmarkiting.data.webservices;

import android.content.Context;
import android.os.Build;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.RetryPolicy;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.lw.internalmarkiting.BuildConfig;
import com.lw.internalmarkiting.RequestProvider;
import com.lw.internalmarkiting.data.model.PromoApp;
import com.lw.internalmarkiting.data.model.PromoType;
import com.lw.internalmarkiting.task.BaseObserver;
import com.lw.internalmarkiting.task.PromoDownloadCallback;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

import static com.lw.internalmarkiting.data.model.PromoType.EXIT;
import static com.lw.internalmarkiting.data.model.PromoType.HOT;

public class WebServiceImpl implements WebService {

    private static final int TIMEOUT = 30000;

    private static final String PACKAGE_NAME = "url";
    private static final String IMAGE_URL = "image";
    private static final String FEATURE_URL = "featured";
    private static final String AD_ID = "ad_id";
    private static final String APP_NAME = "app_name";
    private static final String APP_DESC = "app_description";
    private static final String LABEL = "label";
    private static final String ACCOUNT = "account name";
    private static final String BASE_URL = BuildConfig.BASE_URL;
    private static final String ALL_ADS = BASE_URL + BuildConfig.ALL_ADS;
    private static final String ALL_ADS_CLICK = BASE_URL + BuildConfig.ALL_ADS_CLICK;
    private static final String HOT_EXIT_ADS = BASE_URL + BuildConfig.HOT_EXIT_ADS;
    private static final String HOT_EXIT_CLICK = BASE_URL + BuildConfig.HOT_EXIT_CLICK;
    private final Context context;

    @Contract(pure = true)
    public WebServiceImpl(Context context) {
        this.context = context;
    }

    @Override
    public void onAllAppsClick(int adId) {
        handleClickRequest(ALL_ADS_CLICK + adId);
    }

    @Override
    public void onHotExitAppsClick(int adId) {
        handleClickRequest(HOT_EXIT_CLICK + adId);
    }

    private PromoApp parseHotExitJson(@NotNull JSONObject object, @NotNull @PromoType.PromoAppType String type) {
        PromoApp promoApp;
        try {
            promoApp = new PromoApp();
            promoApp.setPackageName(object.getString(PACKAGE_NAME));
            promoApp.setImageUrl(object.getString(IMAGE_URL));
            promoApp.setAppName(object.getString(LABEL));
            promoApp.setAdId(object.getInt(AD_ID));
            promoApp.setAccountName(object.getString(ACCOUNT));
            promoApp.setType(type);
        } catch (JSONException e) {
            e.printStackTrace();
            promoApp = null;
        }
        return promoApp;
    }

    private List<PromoApp> getHotExitPromo(@NotNull @PromoType.PromoAppType String type, @NotNull JSONObject response) throws JSONException {
        JSONArray array = response.getJSONArray("application");
        return IntStream.range(0, array.length())
                .mapToObj(value -> getJsonObject(array, value))
                .filter(Objects::nonNull)
                .map(jsonObject -> parseHotExitJson(jsonObject, type))
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    private JSONObject getJsonObject(@NotNull JSONArray array, int position) {
        JSONObject jsonObject = null;
        try {
            jsonObject = array.getJSONObject(position);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    private PromoApp parseAllPromoJson(@NotNull JSONObject object, @NotNull @PromoType.PromoAppType String type) {
        PromoApp promoApp;
        try {
            promoApp = new PromoApp();
            promoApp.setAppName(object.getString(APP_NAME));
            promoApp.setAppDescription(object.getString(APP_DESC));
            promoApp.setImageUrl(object.getString(IMAGE_URL));
            promoApp.setFeatureUrl(object.getString(FEATURE_URL));
            promoApp.setPackageName(object.getString(PACKAGE_NAME));
            promoApp.setAccountName(object.getString(ACCOUNT));
            promoApp.setAdId(object.getInt(AD_ID));
            promoApp.setType(type);
        } catch (JSONException e) {
            e.printStackTrace();
            promoApp = null;
        }
        return promoApp;
    }

    @NotNull
    private List<PromoApp> getAllPromo(@NotNull @PromoType.PromoAppType String type, @NotNull JSONObject response) throws JSONException {
        JSONArray array = response.getJSONArray("application");
        return IntStream.range(0, array.length())
                .mapToObj(value -> getJsonObject(array, value))
                .filter(Objects::nonNull)
                .map(jsonObject -> parseAllPromoJson(jsonObject, type))
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    private void handleClickRequest(String clickUrl) {
        StringRequest request = new StringRequest(Request.Method.GET, clickUrl, response -> {
        }, error -> {
        });
        RequestProvider.getRequest(context).add(request);
    }

    @Contract(pure = true)
    @NotNull
    private String getUrl(@NotNull @PromoType.PromoAppType String type) {
        String url;
        switch (type) {
            case HOT:
            case EXIT:
                url = HOT_EXIT_ADS + type;
                break;
            default:
                url = ALL_ADS + type;
        }
        return url;
    }

    private List<PromoApp> getPromoApps(@NotNull @PromoType.PromoAppType String type, JSONObject response) throws JSONException {
        switch (type) {
            case HOT:
            case EXIT:
                return getHotExitPromo(type, response);
            default:
                return getAllPromo(type, response);
        }
    }

    @Override
    public void downloadPromoApps(@PromoType.PromoAppType String type, PromoDownloadCallback callback) {
        final RequestQueue requestQueue = RequestProvider.getRequest(context);
        String url = getUrl(type);
        Timber.i("Promo url : %s", url);
        Timber.i("Promo type : %s", type);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, null,
                response -> Observable.create((ObservableOnSubscribe<PromoApp>) emitter -> {
                    Timber.i("Response : %s", response);
                    List<PromoApp> list = WebServiceImpl.this.getPromoApps(type, response);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        list.forEach(promoApp -> {
                            Timber.i("Downloaded promo : %s", promoApp);
                            callback.onNext(promoApp);
                        });
                    } else {
                        for (PromoApp promoApp : list) {
                            Timber.i("Downloaded promo : %s", promoApp);
                            callback.onNext(promoApp);
                        }
                    }
                    callback.onComplete();
                }).subscribeOn(Schedulers.io()).subscribe(new BaseObserver<>()), error -> {
        });
        requestQueue.getCache().clear();
        requestQueue.cancelAll(request);
        RetryPolicy policy = new DefaultRetryPolicy(TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        requestQueue.add(request);
    }


}