package com.lw.internalmarkiting.data.preferences;

import com.lw.internalmarkiting.data.model.PromoType;

public interface PrefsHelper{
    boolean isHotAppDownloadToday();
    boolean isPromoDownloadedToday();
    void updateDownloadDate();
    void updateDownloadDate(@PromoType.PromoAppType String type);
    boolean isDownloadedToday(@PromoType.PromoAppType String type);
}
