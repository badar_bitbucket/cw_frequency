package com.lw.internalmarkiting.data;

import android.content.Context;

import com.lw.internalmarkiting.data.model.PromoApp;
import com.lw.internalmarkiting.data.preferences.PrefsHelper;
import com.lw.internalmarkiting.data.preferences.PrefsHelperImpl;
import com.lw.internalmarkiting.data.room.Repository;
import com.lw.internalmarkiting.data.room.RepositoryImpl;
import com.lw.internalmarkiting.data.webservices.WebService;
import com.lw.internalmarkiting.data.webservices.WebServiceImpl;
import com.lw.internalmarkiting.task.CountCallback;
import com.lw.internalmarkiting.task.PromoCallback;
import com.lw.internalmarkiting.task.PromoDownloadCallback;
import com.lw.internalmarkiting.task.SingleValueCallback;

import timber.log.Timber;

public class DataManagerImpl implements DataManager {
    private static DataManager instance;
    private final Repository repository;
    private final PrefsHelper prefsHelper;
    private final WebService webService;

    private DataManagerImpl(Context context) {
        repository = new RepositoryImpl(context);
        prefsHelper = new PrefsHelperImpl(context);
        webService = new WebServiceImpl(context);
    }

    public static DataManager getInstance(Context context) {
        if (instance == null) {
            instance = new DataManagerImpl(context);
        }
        return instance;
    }

    @Override
    public void downloadPromoApps(String type, PromoDownloadCallback callback) {
        repository.clearApps(type);
        webService.downloadPromoApps(type, new PromoDownloadCallback() {
            @Override
            public void onNext(PromoApp promoApp) {
                repository.addPromoApp(promoApp);
                callback.onNext(promoApp);
            }

            @Override
            public void onComplete() {
                updateDownloadDate(type);
                callback.onComplete();
            }
        });
    }

    @Override
    public void updateDownloadDate(String type) {
        prefsHelper.updateDownloadDate(type);
    }

    @Override
    public boolean isDownloadedToday(String type) {
        return prefsHelper.isDownloadedToday(type);
    }

    @Override
    public void loadPromoApps(String type, SingleValueCallback<PromoApp> callback) {
        if (!isDownloadedToday(type)) {
            Timber.i("Downloading from server : %s", type);
            downloadPromoApps(type, new PromoDownloadCallback() {
                @Override
                public void onNext(PromoApp promoApp) {
                    callback.onValue(promoApp);
                }

                @Override
                public void onComplete() {
                    callback.onComplete();
                }
            });
        } else {
            Timber.i("These are download today, loading from cache : %s", type);
            repository.getPromoCount(type, promoCount ->
                    repository.loadPromoApp(type, new PromoCallback() {
                        @Override
                        public void promoCount(int count) {

                        }

                        @Override
                        public void onNext(PromoApp promoApp) {
                            callback.onValue(promoApp);
                        }

                        @Override
                        public void onComplete() {
                            callback.onComplete();
                        }
                    }));
        }
    }

    @Override
    public void getPromoCount(String type, CountCallback callback) {
        repository.getPromoCount(type, callback);
    }

    @Override
    public void onAllAppsClick(int adId) {
        webService.onAllAppsClick(adId);
    }

    @Override
    public void onHotExitAppsClick(int adId) {
        webService.onHotExitAppsClick(adId);
    }

    @Override
    public boolean isHotAppDownloadToday() {
        return prefsHelper.isHotAppDownloadToday();
    }

    @Override
    public boolean isPromoDownloadedToday() {
        return prefsHelper.isPromoDownloadedToday();
    }

    @Override
    public void updateDownloadDate() {
        prefsHelper.updateDownloadDate();
    }
}
