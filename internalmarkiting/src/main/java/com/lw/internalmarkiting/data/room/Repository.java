package com.lw.internalmarkiting.data.room;

import com.lw.internalmarkiting.data.model.PromoApp;
import com.lw.internalmarkiting.data.model.PromoType;
import com.lw.internalmarkiting.task.CountCallback;
import com.lw.internalmarkiting.task.PromoCallback;

import org.jetbrains.annotations.NotNull;

public interface Repository {

    void addPromoApp(PromoApp promoApp);

    void clearApps(@PromoType.PromoAppType String type);

    void loadPromoApp(@PromoType.PromoAppType String type, @NotNull PromoCallback callback);

    void getPromoCount(@PromoType.PromoAppType String type, CountCallback callback);
}
