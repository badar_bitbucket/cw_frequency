package com.lw.internalmarkiting.data.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.lw.internalmarkiting.data.model.PromoType;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class PrefsHelperImpl implements PrefsHelper {

    public static final String DATE_FORMAT = "yyyyMMdd";
    private static final String NONE = "";
    private static final String HOT_APP_DOWNLOAD_DATE = "HOT_APP_DOWNLOAD_DATE";
    private static final String PROMO_DOWNLOAD_DATE = "PROMO_DOWNLOAD_DATE";
    private static final String RANDOM_APP_DOWNLOAD_DATE = "RANDOM_APP_DOWNLOAD_DATE ";
    private static final String PROMO = "PROMO ";
    private static final long ONE_DAY_MILLIS = 24 * 60 * 60 * 1000;
    private final SharedPreferences preferences;

    public PrefsHelperImpl(Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    @Override
    public boolean isHotAppDownloadToday() {
        String downloadDate = preferences.getString(HOT_APP_DOWNLOAD_DATE, NONE);
        String currentDate = new SimpleDateFormat(DATE_FORMAT, Locale.US)
                .format(Calendar.getInstance().getTime());
        return downloadDate.equals(currentDate);
    }

    @Override
    public boolean isPromoDownloadedToday() {
        String downloadDate = preferences.getString(PROMO_DOWNLOAD_DATE, NONE);
        String currentDate = new SimpleDateFormat(DATE_FORMAT, Locale.US)
                .format(Calendar.getInstance().getTime());
        return downloadDate.equals(currentDate);
    }

    @Override
    public void updateDownloadDate() {
        String currentDate = new SimpleDateFormat(DATE_FORMAT, Locale.US)
                .format(Calendar.getInstance().getTime());
        preferences.edit().putString(PROMO_DOWNLOAD_DATE, currentDate).apply();
    }

    @Override
    public void updateDownloadDate(@PromoType.PromoAppType String type) {
        preferences.edit().putLong(PROMO + type, System.currentTimeMillis()).apply();
    }

    @Override
    public boolean isDownloadedToday(String type) {
        long lastChecked = preferences.getLong(PROMO + type, -1);
        if(lastChecked == -1)
            return false;
        long diff = System.currentTimeMillis() - lastChecked;
        return diff <= ONE_DAY_MILLIS;
    }
}