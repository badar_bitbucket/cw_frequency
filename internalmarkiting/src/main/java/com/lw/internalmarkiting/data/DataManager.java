package com.lw.internalmarkiting.data;

import com.lw.internalmarkiting.data.model.PromoApp;
import com.lw.internalmarkiting.data.model.PromoType;
import com.lw.internalmarkiting.data.preferences.PrefsHelper;
import com.lw.internalmarkiting.data.webservices.WebService;
import com.lw.internalmarkiting.task.CountCallback;
import com.lw.internalmarkiting.task.SingleValueCallback;

public interface DataManager extends PrefsHelper, WebService {


    void loadPromoApps(@PromoType.PromoAppType String type, final SingleValueCallback<PromoApp> callback);

    void getPromoCount(@PromoType.PromoAppType String type, CountCallback callback);
}
