package com.lw.internalmarkiting.data.model;

import androidx.annotation.StringDef;

import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.SOURCE;

public interface PromoType {

    String NONE = "none";
    String DIALOG = "dialog";
    String CORNER = "corner";
    String NAV = "nav";
    String INTER = "inter";
    String HOT = "hot";
    String EXIT = "exit";


    @Retention(SOURCE)
    @StringDef({NONE, DIALOG, CORNER, NAV, INTER, HOT, EXIT})
    public @interface PromoAppType {
    }
}
