package com.lw.internalmarkiting.task;

@FunctionalInterface
public interface CountCallback {
    void onValue(int promoCount);
}
