package com.lw.internalmarkiting.task;

import timber.log.Timber;

@FunctionalInterface
public interface SingleValueCallback<T> {
    void onValue(T value);

    default void onComplete() {
        Timber.i("This is default");
    }
}
