package com.lw.internalmarkiting.task;

import com.lw.internalmarkiting.data.model.PromoApp;

import timber.log.Timber;

public interface PromoDownloadCallback {
    void onNext(PromoApp promoApp);

    default void onComplete() {
        Timber.i("This is default");
    }
}
