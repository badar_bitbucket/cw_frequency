package com.lw.internalmarkiting.task;

import com.lw.internalmarkiting.AdsApp;
import com.lw.internalmarkiting.data.model.PromoApp;
import com.lw.internalmarkiting.data.model.PromoType;

import org.jetbrains.annotations.NotNull;

import io.reactivex.ObservableOnSubscribe;

import static com.lw.internalmarkiting.data.model.PromoType.CORNER;
import static com.lw.internalmarkiting.data.model.PromoType.DIALOG;
import static com.lw.internalmarkiting.data.model.PromoType.EXIT;
import static com.lw.internalmarkiting.data.model.PromoType.HOT;
import static com.lw.internalmarkiting.data.model.PromoType.INTER;
import static com.lw.internalmarkiting.data.model.PromoType.NAV;

public class AdsTask {

    public static void loadInterstitialPromoApps(final SingleValueCallback<PromoApp> callback) {
        loadPromoApps(INTER, callback);
    }

    public static void loadStartAd(final SingleValueCallback<PromoApp> callback) {
        loadPromoApps(DIALOG, callback);
    }

    public static void loadNavAd(final SingleValueCallback<PromoApp> callback) {
        loadPromoApps(NAV, callback);
    }

    public static void loadCornerAd(final SingleValueCallback<PromoApp> callback) {
        loadPromoApps(CORNER, callback);
    }

    public static void loadHotApps(final SingleValueCallback<PromoApp> callback) {
        loadPromoApps(HOT, callback);
    }

    public static void loadExitApps(final SingleValueCallback<PromoApp> callback) {
        loadPromoApps(EXIT, callback);
    }

    private static void loadPromoApps(@PromoType.PromoAppType String type, final SingleValueCallback<PromoApp> callback) {
        ObservableProvider.getSmartObservable((ObservableOnSubscribe<PromoApp>) emitter -> {
            AdsApp.getDataManager().loadPromoApps(type, new SingleValueCallback<PromoApp>() {
                @Override
                public void onValue(PromoApp value) {
                    emitter.onNext(value);
                }

                @Override
                public void onComplete() {
                    emitter.onComplete();
                }
            });
        }).subscribe(new BaseObserver<PromoApp>() {
            @Override
            public void onNext(@NotNull PromoApp promoApp) {
                callback.onValue(promoApp);
            }

            @Override
            public void onError(@NotNull Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onComplete() {
                callback.onComplete();
            }
        });
    }


    public static void promoAppClick(@NotNull @PromoType.PromoAppType String type, final int adId) {
        switch (type) {
            case HOT:
            case EXIT:
                AdsApp.getDataManager().onHotExitAppsClick(adId);
                break;
            default:
                AdsApp.getDataManager().onAllAppsClick(adId);
        }
    }

    public static void getHotPromoCount(CountCallback callback) {
        getPromoCount(HOT, callback);
    }

    @SuppressWarnings("unused")
    public static void getExitPromoCount(CountCallback callback) {
        getPromoCount(EXIT, callback);
    }

    private static void getPromoCount(@PromoType.PromoAppType String type, CountCallback callback) {
        ObservableProvider.getSmartObservable((ObservableOnSubscribe<Integer>) emitter ->
                AdsApp.getDataManager().getPromoCount(type, emitter::onNext)
        ).subscribe(new BaseObserver<Integer>() {
            @Override
            public void onNext(@NotNull Integer promoCount) {
                callback.onValue(promoCount);
            }
        });
    }
}