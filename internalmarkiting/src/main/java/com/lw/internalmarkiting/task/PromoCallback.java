package com.lw.internalmarkiting.task;

public interface PromoCallback extends PromoDownloadCallback {

    void promoCount(int count);
}
