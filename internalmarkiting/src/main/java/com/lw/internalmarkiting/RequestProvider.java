package com.lw.internalmarkiting;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class RequestProvider {
    private static RequestQueue queue;

    public static RequestQueue getRequest(Context context) {
        if (queue == null) {
            queue = Volley.newRequestQueue(context);
            return queue;
        } else return queue;
    }
}
