package com.lw.internalmarkiting;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.ContextWrapper;
import androidx.multidex.MultiDexApplication;
import com.google.android.gms.ads.MobileAds;
import com.lw.internalmarkiting.ads.AdHelper;
import com.lw.internalmarkiting.ads.AppOpenManager;
import com.lw.internalmarkiting.ads.InterstitialHelper;
import com.lw.internalmarkiting.ads.LwInterstitialHelper;
import com.lw.internalmarkiting.ads.VideoAdHelper;
import com.lw.internalmarkiting.data.DataManager;
import com.lw.internalmarkiting.data.DataManagerImpl;
import com.lw.internalmarkiting.ui.utils.Analytics;
import com.lw.internalmarkiting.ui.utils.InstallReferrerHelper;
import com.pixplicity.easyprefs.library.Prefs;
import org.jetbrains.annotations.Contract;
import timber.log.Timber;
import static com.lw.internalmarkiting.BuildConfig.BANNER_AD_ID;
import static com.lw.internalmarkiting.BuildConfig.INTERSTITIAL_AD_ID;
import static com.lw.internalmarkiting.BuildConfig.NATIVE_AD_ID;
import static com.lw.internalmarkiting.BuildConfig.VIDEO_AD_ID;
public class AdsApp extends MultiDexApplication {
    public static volatile boolean enableAds = true;
    @SuppressLint("StaticFieldLeak")
    private static Context context;
    private static DataManager dataManager;
    private AppOpenManager appOpenManager;

    @Contract(pure = true)
    public static Context getContext() {
        return context;
    }

    public static DataManager getDataManager() {
        return dataManager;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;

        boolean debug = BuildConfig.DEBUG;

//        if (debug)
        Timber.plant(new Timber.DebugTree());

        MobileAds.setRequestConfiguration(AdHelper.getConfiguration());

        Timber.i("Banner ad id : %s", BANNER_AD_ID);
        Timber.i("Native ad id : %s", NATIVE_AD_ID);
        Timber.i("Video ad id : %s", VIDEO_AD_ID);
        Timber.i("Interstitial ad id : %s", INTERSTITIAL_AD_ID);

        initPrefs();
        dataManager = DataManagerImpl.getInstance(context);
        InterstitialHelper.init();
        VideoAdHelper.init();
        Analytics.init();
        LwInterstitialHelper.init();

        InstallReferrerHelper.logInstallReferrer(context);

        // Check if app open ad should be displayed
        if (enableAppOpenAd())
            appOpenManager = new AppOpenManager(this);
    }

    /**
     * @return true if app open ad should be display <br> false if app open ad should not be display
     */
    protected boolean enableAppOpenAd() {
        return false;
    }

    private void initPrefs() {
        new Prefs.Builder()
                .setContext(this)
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName(getPackageName())
                .setUseDefaultSharedPreference(true)
                .build();
    }

}