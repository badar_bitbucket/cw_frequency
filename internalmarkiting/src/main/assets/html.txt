<h1> <font color=#388E40>PRIVACY POLICY</font></h1><br/>
    <p>%1$s("Service","%1$s", "we," "us," or
    <p>%1$s("Service","%1$s", "we," "us," or
        "our") collects information from you in order to provide better service and a better
        user experience. Your privacy is a top priority to us, and this Privacy Policy explains
        how we collect, use, and protect your information.
    </p>
    <h3><font color=#388E40>What information we use and how we use it:</font></h3>
    <h3><font color=#388E40>Read phone status and identity</font>
    </h3>
    <p>This permission allows %1$s to recognize an incoming
        / outgoing call and to switch between the User System interface and call interface or
        announces / shows the caller name while some one is calling, you will identify it
        without looking to your smart phone.
    </p>
    <h3><font color=#388E40>Read your text messages (SMS or MMS)</font></h3>
    <p>We do not save or upload your messages. We use this
        permission to read your text messages when you search messages in search bar or announce
        the sender name while some one is send you a message, you will identify it without
        looking to your smart phone.
    </p>
    <h3><font color=#388E40>Take pictures and videos</font></h3>
    <p>This permission allows %1$s to use your device"s
        camera to take photos / videos and turn ON/OFF Camera Flash. We do not save or upload
        your photos/videos.
    </p>
    <h3><font color=#388E40>Approximate location (network-based)</font></h3>
    <p>This permission allows %1$s to identify and display
        your location on map or apps installed by anonymous surrounding users and to recommend
        popular apps based on users" location. This information is untraceable.
    </p>
    <h3><font color=#388E40>Read your contacts</font</h3>
    <p>%1$s does not save or upload your contacts.
        Permission to access contact information is used when you search contacts in %1$s search bar.
    </p>
    <h3><font color=#388E40>Read calendar events plus confidential information</font>
    </h3>
    <p>This information is used only in the %1$s to remind
        you of your latest calendar events. We do not save or upload any of your calendar
        information.
    </p>
    <h3><font color=#388E40>Modify or delete the contents of your USB storage</font>
    </h3>
    <p>This permission allows %1$s to delete app
        data(image, text, audio etc..) stored on their SD card.
    </p>
    <h3><font color=#388E40>Read the contents of your USB storage</font></h3>
    <p>This permission allows %1$s to view app data(image,
        text, audio etc..) stored on their SD card.
    </p>
    <h3><font color=#388E40>Set an alarm</font></h3>
    <p>This permission allows users to access the device's alarm
        through %1$s clock widget.
    </p>
    <h3><font color=#388E40>Find accounts on the devices</font></h3>
    <p>%1$s does not recognize or save your account
        information for any programs. %1$s only detects whether users have a Google
        account linked with the device which helps us confirm the state of Google Service and
        provide users with appropriate application download and update methods.
    </p>
    <h3><font color=#388E40>Read Google service configuration</font>
    </h3>
    <p>This information is used to acquire advertising ID. LASHPASH
        APPS provide users with better advertising service by using such anonymous ID.
    </p>
    <h3><font color=#388E40>Change network connectivity</font></h3>
    <p>This permission is used in %1$s settings and
        notification toolbar, in order to change/check network connectivity.
    </p>
    <h3><font color=#388E40>Connect and disconnect from Wi-Fi</font>
    </h3>
    <p>This permission is used in %1$s settings and
        notification toolbar in order to connect and disconnect from Wi-Fi.
    </p>
    <h3><font color=#388E40>Full network access</font></h3>
    <p>This permission is used to access the device"s network for
        certain functions including receiving update notifications or accessing app
        classification labels.
    </p>
    <h3><font color=#388E40>View network connections</font></h3>
    <p>This permission is used in %1$s settings and
        notification toolbar, in order to view network connections.
    </p>
    <h3><font color=#388E40>View Wi-Fi connections</font></h3>
    <p>This permission is used in %1$s settings and
        notification toolbar, in order to view Wi-Fi connections.
    </p>
    <h3><font color=#388E40>Access Bluetooth settings</font></h3>
    <p>This permission is used in %1$s settings, in order
        to turn on and off the Bluetooth.
    </p>
    <h3><font color=#388E40>Pair with Bluetooth devices</font></font></h3>
    <p>This permission is used in %1$s settings, in order
        to pair with Bluetooth devices.
    </p>
    <h3><font color=#388E40>Close other apps</font></h3>
    <p>This permission is used in %1$s Boost in order to
        turn off the back-end apps and make the phone run faster.
    </p>
    <h3><font color=#388E40>Retrieve running apps</font></h3>
    <p>This permission is used in %1$s Boost in order to
        view the running apps.
    </p>
    <h3><font color=#388E40>Run as startup</font></h3>
    <p>%1$s will run as startup to provide users with a
        themeLauncher service.
    </p>
    <h3><font color=#388E40>Draw over other apps</font></h3>
    <p>This permission allows the %1$s Free Swipe tab to be
        displayed as a floating window on other apps.
    </p>
    <h3><font color=#388E40>Control vibration</font></h3>
    <p>This permission allows %1$s to make the phone
        vibrate once after users set the device on vibrate, confirming the setting has been
        turned on.
    </p>
    <h3><font color=#388E40>Adjust your wallpaper size</h3>
    <p>This permission allows users to crop %1$s wallpaper
        to the desired size.
    </p>
    <h3><font color=#388E40>Set wallpaper</font></h3>
    <p>This permission allows users to set a selected %1$s
        image as their wallpaper.
    </p>
    <h3><font color=#388E40>Read sync settings</font></h3>
    <p>This permission is used in %1$s power"s save mode,
        and allows %1$s to recognize whether sync settings are turned on or off.
    </p>
    <h3><font color=#388E40>Toggle sync on and off</font></h3>
    <p>This permission is used in %1$s power"s save mode,
        and allows %1$s to synchronize with the user's sync settings.
    </p>
    <h3><font color=#388E40>Expand/collapse status bar</font></h3>
    <p>This permission is used for the gesture feature of %1$s
     User System to expand and collapse the status bar.
    </p>
    <h3><font color=#388E40>Install shortcuts</font></h3>
    <p>This permission is used to install shortcuts on other
        launchers, so users can continue to use %1$s related functions.
    </p>
    <h3><font color=#388E40>Measure app storage space</font></h3>
    <p>This permission is used to acquire the amount of storage
        space used by an application.
    </p>
    <h3><font color=#388E40>Modify system settings</font></h3>
    <p>This permission is used in %1$s settings, in order
        to switch or adjust ringtone, vibration and brightness level of the screen.
    </p>
    <h3><font color=#388E40>Read Home settings and shortcuts
    </font></h3>
    <p>This permission is used to acquire the user"s Home settings
        and shortcuts, in order to maintain user preferences.
    </p>
    <h3><font color=#388E40>Uninstall shortcuts</font></h3>
    <p>This permission allows %1$s to delete the shortcuts
        created by %1$s in other Android launchers.
    </p>
    <h3><font color=#388E40>Photos/Media/Files</font></h3>
    <p>Modify or delete the contents of your USB storage</p>
    <p>Test access to protected storage</p>
    <p>When user recommends friends to download %1$s by
        sharing to social network, we need to save an introduction picture to be shared on SD
        card. We won"t access other file on SD card.
    </p>
    <h3><font color=#388E40>Device &amp; app history</font></h3>
    <p>Retrieve running apps</p>
    <p>We use this information to boost your Android phone
        effectively, clean your device's RAM and make your phone speed up in depth.
    </p>
    <h3><font color=#388E40>How we protect your information</font></h3>
    <p>We have implemented commercially reasonable technical and
        organizational measures to protect your personal information from accidental loss and
        from loss, misuse and unauthorized access, disclosure, alteration and destruction.
        However, please note that although we take reasonable measures to protect your
        information, no app, website, Internet transmission, computer system or wireless
        connection is completely secure.
    </p>
    <h3><font color=#388E40>How we share your information</font></h3>
    <p>We may retain third parties to perform services on our behalf
        and we may collaborate with third parties with respect to particular products or
        services. These third parties may be provided with access to the information needed to
        perform their functions, as long as they agree to keep the information confidential. We
        reserve the right to disclose your information as required by law, when we believe
        disclosure is necessary to comply with a regulatory requirement, judicial proceeding,
        court order, or legal process served on us, or to protect the safety, rights, or
        property of our users, the public or %1$s.
    </p>
    <h3><font color=#388E40>Children's Privacy</font></h3>
    <p>We are committed to protecting the privacy of children. We do
        not knowingly collect personal information from children under the age of 13.
    </p>
    <h3><font color=#388E40>Changes to the Privacy Policy</font></h3>
    <p>%1$s may, in its sole discretion, change this
        Privacy Policy from time to time, any and all changes will be reflected here and the
        date new versions are posted will be stated at the top of this Privacy Policy, so please
        review it periodically.
    </p>
    <h3><font color=#388E40>Contact us</font></h3>
    <p>If you have any suggestions, feedback or requests, email us
        at <b>%2$s</b>
    </p>
