package com.chriswilliams.frequencysound.frequencygenerator.ScannerPdf.ui.GrabDataActivities;

import static com.chriswilliams.frequencysound.frequencygenerator.ScannerPdf.utils.FileSaveHelper.storagePermission;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.camera.core.Camera;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageCaptureException;
import androidx.camera.core.Preview;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.camera.view.PreviewView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.LifecycleOwner;

import com.chriswilliams.frequencysound.frequencygenerator.R;
import com.chriswilliams.frequencysound.frequencygenerator.ScannerPdf.ui.activities.othersActivity.AllTheImagesActivity;
import com.chriswilliams.frequencysound.frequencygenerator.ScannerPdf.utils.ScannerConstants;
import com.chriswilliams.frequencysound.frequencygenerator.databinding.ActivityCamera2Binding;
import com.google.common.util.concurrent.ListenableFuture;

import com.lw.internalmarkiting.ui.activities.BaseActivity;
import com.zxy.tiny.Tiny;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class cameraActivityCardInfo extends BaseActivity<ActivityCamera2Binding> {
    private static final String EXTRA_SOURCE = "source";
    private static final int PICK_REQUEST = 53;
    private final String[] REQUIRED_PERMISSIONS = new String[]{"android.permission.CAMERA", storagePermission};
    private final Executor executor = Executors.newSingleThreadExecutor();
    private final int REQUEST_CODE_PERMISSIONS = 1001;
    PreviewView mPreviewView;
    AppCompatImageView captureImage;
    ArrayList<String> files;
    int counter = 0;
    boolean batch = false;
    String extractedText = "";
    int PICK_IMAGE_MULTIPLE = 1;
    ImageCapture.Builder builder;
    private boolean imagesPicked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int getResId() {
        return R.layout.activity_camera2;
    }

    @Override
    protected void onReady() {
        mPreviewView = binding.camera;
        captureImage = binding.capture;
        files = new ArrayList<>();
        builder = new ImageCapture.Builder();
        binding.home.setOnClickListener(v -> onBackPressed());
        if (getIntent().getIntExtra(EXTRA_SOURCE, -1) == 1) {
            binding.singleMsg.setVisibility(View.GONE);
            binding.multipagesIcon.setVisibility(View.INVISIBLE);
            binding.galleryIcon.setVisibility(View.INVISIBLE);
        } else if (getIntent().getIntExtra(EXTRA_SOURCE, -1) == 3) {
            batch = true;
            binding.singleMsg.setVisibility(View.GONE);
            binding.multipagesIcon.setClickable(false);
            binding.imageCounter.setVisibility(View.VISIBLE);
        } else {
            binding.multipagesIcon.setOnClickListener(v -> {
                files.clear();
                batch = !batch;
                if (batch) {
                    binding.multipagesIcon.setText(R.string.multipl);
                    binding.imageCounter.setVisibility(View.VISIBLE);
                    binding.singleMsg.setVisibility(View.GONE);
                    binding.multiMsg.setVisibility(View.VISIBLE);
                    new Handler(Looper.myLooper()).postDelayed(() -> binding.multiMsg.setVisibility(View.GONE), 2000);

                } else {
                    binding.multipagesIcon.setText(R.string.single);
                    binding.next.setVisibility(View.GONE);
                    counter = 0;
                    binding.imageCounter.setText(counter + "");
                    binding.imageCounter.setVisibility(View.GONE);
                    binding.multiMsg.setVisibility(View.GONE);
                    binding.singleMsg.setVisibility(View.VISIBLE);
                    new Handler(Looper.myLooper()).postDelayed(() -> binding.singleMsg.setVisibility(View.GONE), 2000);
                }
            });
            new Handler(Looper.myLooper()).postDelayed(() -> binding.singleMsg.setVisibility(View.GONE), 2000);

        }

        binding.next.setOnClickListener(v -> {
            Intent i = new Intent(context, AllTheImagesActivity.class);
            i.putStringArrayListExtra("list", files);
            startActivity(i);
        });

        if (allPermissionsGranted()) {
            startCamera();
        } else {
            ActivityCompat.requestPermissions(this, REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS);
        }


        binding.galleryIcon.setOnClickListener(v -> {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_REQUEST);
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_MULTIPLE && resultCode == RESULT_OK && null != data) {
            if (data.getClipData() != null) {
                int cout = data.getClipData().getItemCount();
                for (int i = 0; i < cout; i++) {
                    Uri imageurl = data.getClipData().getItemAt(i).getUri();
                    try {
                        InputStream input = getContentResolver().openInputStream(imageurl);
                        try {
                            File file = new File(getBatchDirectoryName(), System.currentTimeMillis() + ".png");
                            files.add(file.getAbsolutePath());
                            try (OutputStream output = new FileOutputStream(file)) {
                                byte[] buffer = new byte[4 * 1024]; // or other buffer size
                                int read;

                                while ((read = input.read(buffer)) != -1) {
                                    output.write(buffer, 0, read);
                                }

                                output.flush();
                            }
                        } finally {
                            input.close();
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            } else {
                Uri imageurl = data.getData();
                try {
                    InputStream input = getContentResolver().openInputStream(imageurl);
                    try {
                        File file = new File(getBatchDirectoryName(), System.currentTimeMillis() + ".png");
                        files.add(file.getAbsolutePath());
                        try (OutputStream output = new FileOutputStream(file)) {
                            byte[] buffer = new byte[4 * 1024]; // or other buffer size
                            int read;

                            while ((read = input.read(buffer)) != -1) {
                                output.write(buffer, 0, read);
                            }

                            output.flush();
                        }
                    } finally {
                        input.close();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else if (requestCode == PICK_REQUEST && resultCode == RESULT_OK && null != data) {
            Uri uri = data.getData();
            newcropingActivity.start(context, uri.toString(), true, true);
        } else {
            // show this if no image is selected

            Toast.makeText(this, "You haven't picked Image", Toast.LENGTH_LONG).show();
        }

        if (imagesPicked) {
            startActivity(new Intent(context, AllTheImagesActivity.class).putStringArrayListExtra("list", files));
            for (int i = 0; i < files.size(); i++) {
                files.remove(i);
            }
        }

    }

    private void startCamera() {

        final ListenableFuture<ProcessCameraProvider> cameraProviderFuture = ProcessCameraProvider.getInstance(this);
        cameraProviderFuture.addListener(() -> {
            try {

                ProcessCameraProvider cameraProvider = cameraProviderFuture.get();
                bindPreview(cameraProvider);

            } catch (ExecutionException | InterruptedException e) {
                Toast.makeText(context, e.toString(), Toast.LENGTH_LONG).show();
            }
        }, ContextCompat.getMainExecutor(this));
    }

    @Override
    protected void onResume() {
        super.onResume();
        binding.capture.setClickable(true);
    }

    void bindPreview(@NonNull ProcessCameraProvider cameraProvider) {

        Preview preview = new Preview.Builder()
                .build();

        CameraSelector cameraSelector = new CameraSelector.Builder()
                .requireLensFacing(CameraSelector.LENS_FACING_BACK)
                .build();

        ImageAnalysis imageAnalysis = new ImageAnalysis.Builder()

                .build();
        ImageCapture.Builder builder = new ImageCapture.Builder().setFlashMode(ImageCapture.FLASH_MODE_AUTO);

        final ImageCapture imageCapture = builder
                .setTargetRotation(this.getWindowManager().getDefaultDisplay().getRotation())
                .build();

        preview.setSurfaceProvider(mPreviewView.getSurfaceProvider());

        Camera camera = cameraProvider.bindToLifecycle((LifecycleOwner) this, cameraSelector, preview, imageAnalysis, imageCapture);

        captureImage.setOnClickListener(v -> {
            binding.captureAnimation.setVisibility(View.VISIBLE);
            binding.capture.setClickable(false);
            ScannerConstants.selectedImageBitmap = null;
            SimpleDateFormat mDateFormat = new SimpleDateFormat("yyyyMMddHHmmss", Locale.US);
            File file = new File(getBatchDirectoryName(), mDateFormat.format(new Date()) + ".jpg");
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }

            ImageCapture.OutputFileOptions outputFileOptions = new ImageCapture.OutputFileOptions.Builder(file).build();
            imageCapture.takePicture(outputFileOptions, executor, new ImageCapture.OnImageSavedCallback() {
                @Override
                public void onImageSaved(@NonNull ImageCapture.OutputFileResults outputFileResults) {
                    new Handler(Looper.getMainLooper()).post(() -> {

                        if (getIntent().getIntExtra(EXTRA_SOURCE, -1) == 1) {
                            binding.captureAnimation.setVisibility(View.GONE);
                            binding.progressBar.setVisibility(View.VISIBLE);

                        } else {
                            binding.captureAnimation.setVisibility(View.GONE);
                            Tiny.FileCompressOptions options = new Tiny.FileCompressOptions();
                            Tiny.getInstance().source(file.getAbsolutePath()).asFile().withOptions(options).compress((isSuccess, outfile) -> {

                                if (!batch) {
                         newcropingActivity.start(context, outfile, true, false);
                                } else {

                                    counter++;
                                    binding.imageCounter.setText(counter + "");
                                    binding.imageCounterLarge.setText(counter + "");
                                    binding.imageCounterLarge.setVisibility(View.VISIBLE);
                                    binding.capture.setClickable(true);
                                    new Handler(Looper.myLooper()).postDelayed(() -> binding.imageCounterLarge.setVisibility(View.GONE), 500);

                                    if (counter > 0) {
                                        binding.next.setVisibility(View.VISIBLE);
                                    }
                                    files.add(outfile);
                                }
                            });
                        }
                    });


                }

                @Override
                public void onError(@NonNull ImageCaptureException error) {
                    error.printStackTrace();
                }
            });
        });
    }

    public String getBatchDirectoryName() {

        String app_folder_path;
        app_folder_path = getFilesDir().getPath() + "/images";
        File dir = new File(app_folder_path);
        if (!dir.exists() && !dir.mkdirs()) {

        }

        return app_folder_path;
    }

    private boolean allPermissionsGranted() {

        for (String permission : REQUIRED_PERMISSIONS) {
            if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_PERMISSIONS) {
            if (allPermissionsGranted()) {
                startCamera();
            } else {
                Toast.makeText(this, "Permissions not granted by the user.", Toast.LENGTH_SHORT).show();
                this.finish();
            }
        }
    }
}