package com.chriswilliams.frequencysound.frequencygenerator.ScannerPdf.ui.activities.othersActivity;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.SeekBar;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.chriswilliams.frequencysound.frequencygenerator.ScannerPdf.ui.activities.draw_sign_Activity.SimpleDrawingView;
import com.chriswilliams.frequencysound.frequencygenerator.ScannerPdf.ui.adapters.FiltersAdapter;
import com.chriswilliams.frequencysound.frequencygenerator.ScannerPdf.ui.bottomSaving_Activity.BottomSheetForSaving;
import com.chriswilliams.frequencysound.frequencygenerator.ScannerPdf.ui.fragments.TextEditorDialogFragment;
import com.chriswilliams.frequencysound.frequencygenerator.ScannerPdf.utils.FileSaveHelper;
import com.chriswilliams.frequencysound.frequencygenerator.ScannerPdf.utils.ScannerConstants;
import com.googlecode.leptonica.android.GrayQuant;
import com.googlecode.leptonica.android.Pix;
import com.itextpdf.text.Document;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.chriswilliams.frequencysound.frequencygenerator.App;
import com.chriswilliams.frequencysound.frequencygenerator.R;
import com.chriswilliams.frequencysound.frequencygenerator.databinding.ActivityEditingBinding;
import com.chriswilliams.frequencysound.frequencygenerator.databinding.AddSignDialogBinding;
import com.chriswilliams.frequencysound.frequencygenerator.databinding.AddTextDialogBinding;
import com.chriswilliams.frequencysound.frequencygenerator.databinding.DiscurdBoxBinding;
import com.lw.internalmarkiting.ui.activities.BaseActivity;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;
import ja.burhanrashid52.photoeditor.OnPhotoEditorListener;
import ja.burhanrashid52.photoeditor.OnSaveBitmap;
import ja.burhanrashid52.photoeditor.PhotoEditor;
import ja.burhanrashid52.photoeditor.PhotoEditorView;
import ja.burhanrashid52.photoeditor.PhotoFilter;
import ja.burhanrashid52.photoeditor.TextStyleBuilder;
import ja.burhanrashid52.photoeditor.ViewType;
import timber.log.Timber;
import yuku.ambilwarna.AmbilWarnaDialog;
public class EditingActivity extends BaseActivity<ActivityEditingBinding> implements
        BottomSheetForSaving.SavingInterface, OnPhotoEditorListener, DatePickerDialog.OnDateSetListener {
    public static final String EXTRA_FILE = "extra_file";
    public static final String EXTRA_SAVE_OUTPUT = "extra_save_output";
    public static final String EXTRA_URI_PATH = "extra_uri_path";
    private static final String EXTRA_SOURCE = "source";
    private static final String EXTRA_TEXT = "text";
    private final int PICK_REQUEST = 110;
    boolean saveOutput;
    ByteArrayOutputStream stream = null;
    byte[] byteArray = null;
    Pix pix;
    PhotoEditor mPhotoEditor;
    DatePickerDialog picker;
    int mDefaultColor = Color.parseColor("#000000");
    SimpleDrawingView simpleDrawingView;
    private String filePath;
    private Bitmap cropImage, SavedCropImage;
    private PhotoEditorView mPhotoEditorView;
    private boolean isInverted;
    private FileSaveHelper mSaveFileHelper;
    private Uri savedImageUri;
    private PhotoFilter photoFilter = PhotoFilter.NONE;

    public static void start(Context context, String filePath, boolean saveOutput) {
        Intent starter = new Intent(context, EditingActivity.class);
        starter.putExtra(EXTRA_FILE, filePath);
        starter.putExtra(EXTRA_SAVE_OUTPUT, saveOutput);
        context.startActivity(starter);
    }

    public static void start(Context context, int source, String text) {
        Intent starter = new Intent(context, EditingActivity.class);
        starter.putExtra(EXTRA_SOURCE, source);
        starter.putExtra(EXTRA_TEXT, text);
        context.startActivity(starter);
    }

    public static File createFolder(String subFolderName) {
        File dest;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            dest = new File(App.getContext().getExternalFilesDir(""), subFolderName);
        } else {
            dest = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), subFolderName);
        }
        if (!dest.exists() && dest.mkdirs())
            Timber.i("Directories created");
        return dest;
    }

    public Bitmap addBorderToBitmap(Bitmap srcBitmap, int borderWidth, int borderColor) {
        Bitmap dstBitmap = Bitmap.createBitmap(
                srcBitmap.getWidth() + borderWidth * 2, // Width
                srcBitmap.getHeight() + borderWidth * 2, // Height
                Bitmap.Config.ARGB_8888 // Config
        );
        Canvas canvas = new Canvas(dstBitmap);
        Paint paint = new Paint();
        paint.setColor(borderColor);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(borderWidth);
        paint.setAntiAlias(true);
        Rect rect = new Rect(
                borderWidth / 2,
                borderWidth / 2,
                canvas.getWidth() - borderWidth / 2,
                canvas.getHeight() - borderWidth / 2
        );
        canvas.drawRect(rect, paint);
        canvas.drawBitmap(srcBitmap, borderWidth, borderWidth, null);
        srcBitmap.recycle();
        return dstBitmap;
    }

    @Override
    protected int getResId() {
        return R.layout.activity_editing;
    }

    @Override
    public void onReady() {
        simpleDrawingView = new SimpleDrawingView(context);
        if (getIntent().getIntExtra(EXTRA_SOURCE, -1) == 1) {
            Bitmap bitmap = Bitmap.createBitmap(960, 1280, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            Paint paint = new Paint();
            paint.setColor(Color.GREEN);
            bitmap = addBorderToBitmap(bitmap, 1, Color.BLACK);
            saveOutput = true;
            canvas.drawRect(0F, 0F, (float) 960, (float) 1280, paint);
            cropImage = bitmap;
        } else {
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            filePath = getIntent().getStringExtra(EXTRA_FILE);
            saveOutput = getIntent().getBooleanExtra(EXTRA_SAVE_OUTPUT, true);
            cropImage = BitmapFactory.decodeFile(filePath, bmOptions);
        }
        SavedCropImage = cropImage;
        binding.preview.getSource().setImageBitmap(cropImage);

        isInverted = false;

        binding.selection.setOnClickListener(v -> {
            CroppingActivity.start(context, filePath, true, false);
            finish();
        });

        binding.seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                cropImage = com.googlecode.leptonica.android.WriteFile.writeBitmap(
                        GrayQuant.pixThresholdToBinary(pix, ((254 * seekBar.getProgress()) / 50))
                );
                binding.preview.getSource().setImageBitmap(cropImage);
            }
        });

        if (cropImage == null) {
            Toast.makeText(this, ScannerConstants.imageError, Toast.LENGTH_LONG).show();
            finish();
        }


        FiltersAdapter adapter = new FiltersAdapter(context, photoFilter -> {
            mPhotoEditor.setFilterEffect(photoFilter);
            this.photoFilter = photoFilter;
        });

        binding.filterView.setAdapter(adapter);
        mPhotoEditorView = binding.preview;

        Typeface helvetica = ResourcesCompat.getFont(this, R.font.helvetica);

        mPhotoEditor = new PhotoEditor.Builder(this, mPhotoEditorView)
                .setDefaultTextTypeface(helvetica)
                .setPinchTextScalable(true)
                .build(); // build photo editor sdk

        mPhotoEditor.setOnPhotoEditorListener(this);

        if (getIntent().getIntExtra(EXTRA_SOURCE, -1) == 1) {
            binding.selection.setClickable(false);
            //   binding.filterId.setClickable(false);
            TextEditorDialogFragment textEditorDialogFragment = TextEditorDialogFragment.show(
                    EditingActivity.this, getIntent().getStringExtra(EXTRA_TEXT), Color.BLACK);
            textEditorDialogFragment.setOnTextEditorListener((inputText, colorCode) -> {
                final TextStyleBuilder styleBuilder = new TextStyleBuilder();
                styleBuilder.withTextColor(Color.BLACK);
                styleBuilder.withTextSize(8.0f);
                mPhotoEditor.addText(inputText, styleBuilder);
                showText("Make sure to add text between the upper and lower bounds");
            });
        }
        // Rotated Code
        binding.changeId.setOnClickListener(v -> {
            binding.bottomButtons.setVisibility(View.INVISIBLE);
            binding.rotatedId.setVisibility(View.VISIBLE);
        });
        binding.Signature.setOnClickListener(v -> {
            binding.bottomButtons.setVisibility(View.INVISIBLE);
            binding.SignatureEdtingId.setVisibility(View.VISIBLE);
        });
        binding.rotateLeft.setOnClickListener(v -> {
            float degrees = 90;//rotation degree
            Matrix matrix = new Matrix();
            matrix.setRotate(degrees);
            cropImage = Bitmap.createBitmap(cropImage, 0, 0, cropImage.getWidth(), cropImage.getHeight(), matrix, true);
            mPhotoEditorView.getSource().setImageBitmap(cropImage);
            mPhotoEditor.setFilterEffect(photoFilter);

        });
        binding.rotateRight.setOnClickListener(v -> {
            float degrees = -90;//rotation degree
            Matrix matrix = new Matrix();
            matrix.setRotate(degrees);
            cropImage = Bitmap.createBitmap(cropImage, 0, 0, cropImage.getWidth(), cropImage.getHeight(), matrix, true);
            mPhotoEditorView.getSource().setImageBitmap(cropImage);
            mPhotoEditor.setFilterEffect(photoFilter);
        });

        binding.home.setOnClickListener(v -> {
            Intent mainActivity = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(mainActivity);

        });
        binding.done.setOnClickListener(v -> {
            if (saveOutput) {
                new BottomSheetForSaving(false).show(getSupportFragmentManager(), "exampleBottomSheet");
            } else {
                mPhotoEditor.saveAsBitmap(new OnSaveBitmap() {
                    @Override
                    public void onBitmapReady(Bitmap saveBitmap) {
                        cropImage = saveBitmap;
                        saveImageForMultiple();
                        finish();
                    }

                    @Override
                    public void onFailure(Exception e) {
                        e.getMessage();
                    }
                });
            }
        });


        binding.addtext.setOnClickListener(v -> {
            Dialog dialog = new Dialog(context);
            AddTextDialogBinding addTextDialogBinding = AddTextDialogBinding.inflate(LayoutInflater.from(context));
            dialog.setContentView(addTextDialogBinding.getRoot());
            addTextDialogBinding.addTextEditText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    addTextDialogBinding.counterText.setText(addTextDialogBinding.addTextEditText.getText().toString().length() + "/50");
                }
            });

            addTextDialogBinding.cancelText.setOnClickListener(view -> dialog.dismiss());
            addTextDialogBinding.addTextDoneTv.setOnClickListener(view -> {
                if (addTextDialogBinding.addTextEditText.getText().toString().equals("")) {
                    Toast.makeText(EditingActivity.this, "Please Enter Text!", Toast.LENGTH_SHORT).show();
                } else {
                    final TextStyleBuilder styleBuilder = new TextStyleBuilder();
                    styleBuilder.withTextColor(mDefaultColor);
                    styleBuilder.withTextFont(Typeface.create((Typeface) null, Typeface.BOLD));
                    styleBuilder.withTextSize(17.0f);
                    mPhotoEditor.addText(addTextDialogBinding.addTextEditText.getText().toString(), styleBuilder);
                    dialog.dismiss();
                }

            });

            addTextDialogBinding.redColor.setOnClickListener(v12 -> addTextDialogBinding.addTextEditText.setTextColor(Color.parseColor("#ff0000")));
            mDefaultColor = Color.RED;

            addTextDialogBinding.blueColor.setOnClickListener(v13 -> {
                addTextDialogBinding.addTextEditText.setTextColor(Color.parseColor("#073AD1"));
                mDefaultColor = Color.BLUE;


            });
            addTextDialogBinding.blackColor.setOnClickListener(v1 -> {
                addTextDialogBinding.addTextEditText.setTextColor(Color.parseColor("#000000"));
                mDefaultColor = Color.BLACK;

            });
            addTextDialogBinding.colorPicker.setOnClickListener(v18 -> {
                mDefaultColor = ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary);

                AmbilWarnaDialog colorPicker = new AmbilWarnaDialog(context, mDefaultColor, new AmbilWarnaDialog.OnAmbilWarnaListener() {
                    @Override
                    public void onCancel(AmbilWarnaDialog dialog1) {
                    }

                    @Override
                    public void onOk(AmbilWarnaDialog dialog1, int color) {
                        mDefaultColor = color;
                        addTextDialogBinding.addTextEditText.setTextColor(color);
                    }
                });
                colorPicker.show();


            });
            dialog.setCancelable(false);
            DisplayMetrics displayMetrics = Resources.getSystem().getDisplayMetrics();
            int widthLcl = (int) (displayMetrics.widthPixels * 0.9f);
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) addTextDialogBinding.getRoot().getLayoutParams();
            params.width = widthLcl;
            params.gravity = Gravity.CENTER;
            if (!((Activity) context).isFinishing())
                dialog.show();
            addTextDialogBinding.getRoot().setLayoutParams(params);
            Window window = dialog.getWindow();
            if (window != null) {
                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            }
        });
        binding.sign.setOnClickListener(v -> {
            Dialog dialog = new Dialog(context);
            AddSignDialogBinding addSignDialogBinding = AddSignDialogBinding.inflate(LayoutInflater.from(context));
            dialog.setContentView(addSignDialogBinding.getRoot());
            addSignDialogBinding.cancelText.setOnClickListener(view -> dialog.dismiss());
            addSignDialogBinding.addTextDoneTv.setOnClickListener(view -> {
                if (addSignDialogBinding.simpleDrawingView1.isEmpty()) {
                    Toast.makeText(EditingActivity.this, "Please Draw Sign!", Toast.LENGTH_SHORT).show();
                } else {
                    mPhotoEditor.addImage(addSignDialogBinding.simpleDrawingView1.getBitmap());
                    dialog.dismiss();
                }
            });

            addSignDialogBinding.selectColor.setOnClickListener(v19 -> {
                mDefaultColor = ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary);
                AmbilWarnaDialog colorPicker = new AmbilWarnaDialog(context, mDefaultColor, new AmbilWarnaDialog.OnAmbilWarnaListener() {
                    @Override
                    public void onCancel(AmbilWarnaDialog dialog12) {
                    }

                    @Override
                    public void onOk(AmbilWarnaDialog dialog12, int color) {
                        addSignDialogBinding.simpleDrawingView1.setSignColor(color);
                        mDefaultColor = color;


                    }
                });
                colorPicker.show();
            });
            addSignDialogBinding.resetButton.setOnClickListener(v14 -> {
                addSignDialogBinding.simpleDrawingView1.setClear(true);

            });
            addSignDialogBinding.signRed.setOnClickListener(v15 -> {
                addSignDialogBinding.simpleDrawingView1.setSignColor(Color.parseColor("#ff0000"));
                mDefaultColor = Color.RED;


            });
            addSignDialogBinding.signBlue.setOnClickListener(v16 -> {
                addSignDialogBinding.simpleDrawingView1.setSignColor(Color.parseColor("#073AD1"));
                mDefaultColor = Color.BLUE;

            });
            addSignDialogBinding.signBlack.setOnClickListener(v17 -> {
                addSignDialogBinding.simpleDrawingView1.setSignColor(Color.parseColor("#000000"));
                mDefaultColor = Color.BLACK;

            });
            dialog.setCancelable(false);
            DisplayMetrics displayMetrics = Resources.getSystem().getDisplayMetrics();
            int widthLcl = (int) (displayMetrics.widthPixels * 0.9f);
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) addSignDialogBinding.getRoot().getLayoutParams();
            params.width = widthLcl;
            params.gravity = Gravity.CENTER;
            if (!((Activity) context).isFinishing())
                dialog.show();
            addSignDialogBinding.getRoot().setLayoutParams(params);
            Window window = dialog.getWindow();
            if (window != null) {
                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            }
        });
        binding.addImage.setOnClickListener(v -> {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_REQUEST);
        });
        binding.addDate.setOnClickListener(v -> {

            final Calendar cldr = Calendar.getInstance();
            int day = cldr.get(Calendar.DAY_OF_MONTH);
            int month = cldr.get(Calendar.MONTH);
            int year = cldr.get(Calendar.YEAR);
            picker = new DatePickerDialog(context, (view, year1, month1, dayOfMonth) -> {
                String date = dayOfMonth + "/" + month1 + "/" + year1;
                final TextStyleBuilder styleBuilder = new TextStyleBuilder();
                styleBuilder.withTextColor(Color.parseColor("#FF0000"));
                styleBuilder.withTextFont(Typeface.create((Typeface) null, Typeface.BOLD));
                styleBuilder.withTextSize(16.0f);
                mPhotoEditor.addText(date, styleBuilder);
            }, year, month, day);
            try {
                picker.create();
                picker.show();
            } catch (Exception e) {
                e.getMessage();
            }
        });
        binding.discurdAll.setOnClickListener(v -> {
            Dialog dialog = new Dialog(context);
            DiscurdBoxBinding discurdBoxBinding = DiscurdBoxBinding.inflate(LayoutInflater.from(context));
            dialog.setContentView(discurdBoxBinding.getRoot());
            discurdBoxBinding.cancelButton.setOnClickListener(view -> dialog.dismiss());
            discurdBoxBinding.deleteButton.setOnClickListener(view -> {


                mPhotoEditor.clearAllViews();
                mPhotoEditor.setFilterEffect(PhotoFilter.NONE);
                photoFilter = PhotoFilter.NONE;
                dialog.dismiss();


            });
            dialog.setCancelable(false);
            DisplayMetrics displayMetrics = Resources.getSystem().getDisplayMetrics();
            int widthLcl = (int) (displayMetrics.widthPixels * 0.9f);
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) discurdBoxBinding.getRoot().getLayoutParams();
            params.width = widthLcl;
            params.gravity = Gravity.CENTER;
            if (!((Activity) context).isFinishing())
                dialog.show();
            discurdBoxBinding.getRoot().setLayoutParams(params);
            Window window = dialog.getWindow();
            if (window != null) {
                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            }
        });
        binding.filterId.setOnClickListener(v -> {
            binding.bottomButtons.setVisibility(View.INVISIBLE);
            binding.filterView.setVisibility(View.VISIBLE);
        });
        binding.changeId.setOnClickListener(v -> {
            binding.bottomButtons.setVisibility(View.INVISIBLE);
            binding.rotatedId.setVisibility(View.VISIBLE);
        });
        mSaveFileHelper = new FileSaveHelper(this);
    }

    private void pdfCreator() {
        mPhotoEditor.saveAsBitmap(new OnSaveBitmap() {
            @Override
            public void onBitmapReady(Bitmap saveBitmap) {
                cropImage = saveBitmap;
                String filename = ScannerConstants.fileName + ".pdf";
                String fileLocation;
                String folderName = context.getString(R.string.directory);
                fileLocation = getFilesDir() + File.separator + folderName + File.separator + filename;
                try {
                    File file = new File(fileLocation);
                    file.getParentFile().mkdirs();
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                binding.progressBar.setVisibility(View.VISIBLE);
                Document document = new Document(PageSize.A4);
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                stream = new ByteArrayOutputStream();
                cropImage.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byteArray = stream.toByteArray();
                try {
                    PdfWriter.getInstance(document, new FileOutputStream(fileLocation));
                    document.open();
                    Image image = Image.getInstance(byteArray);
                    image.setAbsolutePosition(0, 0);
                    image.scaleAbsolute(595, 842);
                    document.add(image);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                document.close();
                binding.progressBar.setVisibility(View.GONE);
                startActivity(new Intent(EditingActivity.this, SuccessActivity.class)
                        .putExtra("jpeg", 1)
                        .putExtra(EXTRA_URI_PATH, fileLocation));
            }

            @Override
            public void onFailure(Exception e) {
                e.getMessage();
            }
        });


    }

    private void imageCreator() {

        mPhotoEditor.saveAsBitmap(new OnSaveBitmap() {
            @Override
            public void onBitmapReady(Bitmap saveBitmap) {
                cropImage = saveBitmap;
                saveImage();
            }

            @Override
            public void onFailure(Exception e) {
                e.getMessage();
            }
        });
    }

    @Override
    public void savePdf() {
        pdfCreator();
    }

    @Override
    public void saveJpeg() {
        imageCreator();
    }

    private void saveImage() {
        if (cropImage != null) {
            File file = getSavedFolderLocation();
            if (file.exists() || file.mkdirs()) {
                String stringBuilder2 = ScannerConstants.fileName + ".png";
                savedImageUri = saveBitmap(cropImage, stringBuilder2, context);
                if (cropImage != null) {
                    cropImage.recycle();
                    cropImage = null;
                    startActivity(new Intent(EditingActivity.this, SuccessActivity.class)
                            .putExtra("jpeg", 4)
                            .putExtra(EXTRA_URI_PATH, savedImageUri.toString()));

                }

                sendBroadcast(new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE", savedImageUri));
            } else {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.create_dir_err),
                        Toast.LENGTH_LONG).show();
            }
        }
    }

    private void saveImageForMultiple() {
        if (cropImage != null) {
            File edited = new File(filePath);
            FileOutputStream fos;
            try {
                fos = new FileOutputStream(edited);
                cropImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public Uri saveBitmap(Bitmap finalBitmap, String filename, Context context) {
        String fileLocation;
        String folderName = context.getString(R.string.directory);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            fileLocation = Environment.DIRECTORY_PICTURES + File.separator + folderName + File.separator + filename;
        } else {
            fileLocation = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) +
                    File.separator + folderName + File.separator + filename;
            try {
                File file = new File(fileLocation);
                file.getParentFile().mkdirs();
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        final ContentValues values = new ContentValues();
        values.put(MediaStore.MediaColumns.DISPLAY_NAME, filename);
        values.put(MediaStore.MediaColumns.MIME_TYPE, "image/png");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            values.put(MediaStore.MediaColumns.RELATIVE_PATH, fileLocation);
        } else {
            values.put(MediaStore.MediaColumns.DATA, fileLocation);
        }

        final ContentResolver resolver = context.getContentResolver();
        Uri uri = null;

        try {
            final Uri contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
            uri = resolver.insert(contentUri, values);

            if (uri == null)
                throw new IOException("Failed to create new MediaStore record.");

            try (final OutputStream stream = resolver.openOutputStream(uri)) {
                if (stream == null)
                    throw new IOException("Failed to open output stream.");

                if (!finalBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream))
                    throw new IOException("Failed to save bitmap.");

                return uri;
            }
        } catch (IOException e) {

            if (uri != null) {
                // Don't leave an orphan entry in the MediaStore
                resolver.delete(uri, null, null);
            }
        }
        return null;
    }

    public File getSavedFolderLocation() {
        return createFolder("PDF Scanner");
    }


    @Override
    public void onEditTextChangeListener(View rootView, String text, int colorCode) {
        TextEditorDialogFragment textEditorDialogFragment =
                TextEditorDialogFragment.show(this, text, colorCode);
        textEditorDialogFragment.setOnTextEditorListener((inputText, colorCode1) -> {
            final TextStyleBuilder styleBuilder = new TextStyleBuilder();
            styleBuilder.withTextColor(Color.BLACK);
            mPhotoEditor.editText(rootView, inputText, styleBuilder);
        });
    }

    @Override
    public void onBackPressed() {
        if(binding.bottomButtons.getVisibility()==View.INVISIBLE)
        {
            binding.SignatureEdtingId.setVisibility(View.INVISIBLE);
            binding.bottomButtons.setVisibility(View.VISIBLE);
            binding.filterView.setVisibility(View.INVISIBLE);
            binding.rotatedId.setVisibility(View.INVISIBLE);
        }
        else {
            finish();
        }

    }

    @Override
    public void onAddViewListener(ViewType viewType, int numberOfAddedViews) {
    }

    @Override
    public void onRemoveViewListener(ViewType viewType, int numberOfAddedViews) {
    }

    @Override
    public void onStartViewChangeListener(ViewType viewType) {
    }

    @Override
    public void onStopViewChangeListener(ViewType viewType) {
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_REQUEST && resultCode == RESULT_OK && data != null) {
            Uri uri = data.getData();
            Glide.with(context).asBitmap().load(uri).into(new CustomTarget<Bitmap>() {
                @Override
                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                    mPhotoEditor.addImage(resource);
                }

                @Override
                public void onLoadCleared(@Nullable Drawable placeholder) {

                }
            });

        }
    }
}
