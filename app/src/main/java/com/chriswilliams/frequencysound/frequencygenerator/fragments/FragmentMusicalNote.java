package com.chriswilliams.frequencysound.frequencygenerator.fragments;

import android.app.Dialog;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.transition.Slide;
import androidx.transition.Transition;
import androidx.transition.TransitionManager;

import com.chriswilliams.frequencysound.frequencygenerator.R;
import com.chriswilliams.frequencysound.frequencygenerator.adapters.MusicalNoteAdapter;
import com.chriswilliams.frequencysound.frequencygenerator.audio.Audio;
import com.chriswilliams.frequencysound.frequencygenerator.databinding.FragmentMusicalNotesBinding;
import com.chriswilliams.frequencysound.frequencygenerator.models.MusicalNotes;
import com.chriswilliams.frequencysound.frequencygenerator.prefernces.MyPreferences;
import com.chriswilliams.frequencysound.frequencygenerator.utils.FrequencyUtils;
import com.lw.internalmarkiting.ui.base.BindingFragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.chriswilliams.frequencysound.frequencygenerator.utils.Constants.MAX_VOLUME;
import static com.chriswilliams.frequencysound.frequencygenerator.utils.Constants.SAWTOOTH;
import static com.chriswilliams.frequencysound.frequencygenerator.utils.Constants.SINE;
import static com.chriswilliams.frequencysound.frequencygenerator.utils.Constants.SQUARE;

public class FragmentMusicalNote extends BindingFragment<FragmentMusicalNotesBinding> {

    List<MusicalNotes> musicalNotes, musicalNotesFiltered;
    Audio audio;
    String a4;
    private boolean isPlaying = false;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_musical_notes;
    }

    @Override
    protected void onReady() {
        musicalNotes = new ArrayList<>();
        musicalNotesFiltered = new ArrayList<>();
        audio = new Audio();
        audio.level = MyPreferences.getLastVolume() / (float) MAX_VOLUME;
        initSpinner();
        initWaveForm();
        setOnClickListeners();
        binding.spinner.setSelection(0);
        binding.recyclerView.setAdapter(new MusicalNoteAdapter(requireContext(), musicalNotes, position -> {
            audio.stop();
            isPlaying = true;
            showPanel();
            binding.note.setText(musicalNotes.get(position).getNote(), TextView.BufferType.SPANNABLE);
            binding.frequency.setText(musicalNotes.get(position).getFrequency() + " Hz");
            audio.frequency = musicalNotes.get(position).getFrequency();
            audio.start();
            binding.btnPlay.setImageDrawable(getResources().getDrawable(R.drawable.ic_pause));
        }));
    }

    private void initWaveForm() {
        switch (MyPreferences.getLastWaveForm()) {
            case SINE:
                binding.wave.setImageDrawable(getResources().getDrawable(R.drawable.ic_sine_wave));
                audio.waveform = Audio.SINE;
                break;
            case SQUARE:
                binding.wave.setImageDrawable(getResources().getDrawable(R.drawable.ic_square_wave));
                audio.waveform = Audio.SQUARE;
                break;
            case SAWTOOTH:
                binding.wave.setImageDrawable(getResources().getDrawable(R.drawable.ic_sawtooth_wave));
                audio.waveform = Audio.SAWTOOTH;
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        audio.stop();
        isPlaying = false;
    }

    @Override
    public void onResume() {
        super.onResume();
        requireActivity().setTitle("Musical Notes");
    }

    private void setOnClickListeners() {
        binding.wave.setOnClickListener(v -> {
            switch (MyPreferences.getLastWaveForm()) {
                case SINE:
                    binding.wave.setImageDrawable(getResources().getDrawable(R.drawable.ic_square_wave));
                    audio.waveform = Audio.SQUARE;
                    MyPreferences.setLastWaveForm(SQUARE);
                    break;
                case SQUARE:
                    binding.wave.setImageDrawable(getResources().getDrawable(R.drawable.ic_sawtooth_wave));
                    audio.waveform = Audio.SAWTOOTH;
                    MyPreferences.setLastWaveForm(SAWTOOTH);
                    break;
                case SAWTOOTH:
                    binding.wave.setImageDrawable(getResources().getDrawable(R.drawable.ic_sine_wave));
                    audio.waveform = Audio.SINE;
                    MyPreferences.setLastWaveForm(SINE);
                    break;
            }
        });
        binding.volumeButton.setOnClickListener(v -> showVolumeDialog());
        binding.btnPlay.setOnClickListener(v -> {
            if (isPlaying) {
                binding.btnPlay.setImageResource(R.drawable.ic_play_64);
                audio.stop();
                isPlaying = false;
            } else {
                binding.btnPlay.setImageResource(R.drawable.ic_pause);
                audio.start();
                isPlaying = true;
            }
        });
        binding.btnStop.setOnClickListener(v -> {
            audio.stop();
            hidePanel();
        });
        Drawable unselected = getResources().getDrawable(R.drawable.bg_gradient);
        Drawable selected = getResources().getDrawable(R.drawable.bg_selected);
        binding.A.setOnClickListener(v -> {
            binding.A.setBackgroundDrawable(selected);
            binding.B.setBackgroundDrawable(unselected);
            binding.C.setBackgroundDrawable(unselected);
            binding.D.setBackgroundDrawable(unselected);
            binding.E.setBackgroundDrawable(unselected);
            binding.F.setBackgroundDrawable(unselected);
            binding.G.setBackgroundDrawable(unselected);
            showFilteredList("A");
        });
        binding.B.setOnClickListener(v -> {
            binding.A.setBackgroundDrawable(unselected);
            binding.B.setBackgroundDrawable(selected);
            binding.C.setBackgroundDrawable(unselected);
            binding.D.setBackgroundDrawable(unselected);
            binding.E.setBackgroundDrawable(unselected);
            binding.F.setBackgroundDrawable(unselected);
            binding.G.setBackgroundDrawable(unselected);
            showFilteredList("B");
        });
        binding.C.setOnClickListener(v -> {
            binding.A.setBackgroundDrawable(unselected);
            binding.B.setBackgroundDrawable(unselected);
            binding.C.setBackgroundDrawable(selected);
            binding.D.setBackgroundDrawable(unselected);
            binding.E.setBackgroundDrawable(unselected);
            binding.F.setBackgroundDrawable(unselected);
            binding.G.setBackgroundDrawable(unselected);
            showFilteredList("C");
        });
        binding.D.setOnClickListener(v -> {
            binding.A.setBackgroundDrawable(unselected);
            binding.B.setBackgroundDrawable(unselected);
            binding.C.setBackgroundDrawable(unselected);
            binding.D.setBackgroundDrawable(selected);
            binding.E.setBackgroundDrawable(unselected);
            binding.F.setBackgroundDrawable(unselected);
            binding.G.setBackgroundDrawable(unselected);
            showFilteredList("D");
        });
        binding.E.setOnClickListener(v -> {
            binding.A.setBackgroundDrawable(unselected);
            binding.B.setBackgroundDrawable(unselected);
            binding.C.setBackgroundDrawable(unselected);
            binding.D.setBackgroundDrawable(unselected);
            binding.E.setBackgroundDrawable(selected);
            binding.F.setBackgroundDrawable(unselected);
            binding.G.setBackgroundDrawable(unselected);
            showFilteredList("E");
        });
        binding.F.setOnClickListener(v -> {
            binding.A.setBackgroundDrawable(unselected);
            binding.B.setBackgroundDrawable(unselected);
            binding.C.setBackgroundDrawable(unselected);
            binding.D.setBackgroundDrawable(unselected);
            binding.E.setBackgroundDrawable(unselected);
            binding.F.setBackgroundDrawable(selected);
            binding.G.setBackgroundDrawable(unselected);
            showFilteredList("F");
        });
        binding.G.setOnClickListener(v -> {
            binding.A.setBackgroundDrawable(unselected);
            binding.B.setBackgroundDrawable(unselected);
            binding.C.setBackgroundDrawable(unselected);
            binding.D.setBackgroundDrawable(unselected);
            binding.E.setBackgroundDrawable(unselected);
            binding.F.setBackgroundDrawable(unselected);
            binding.G.setBackgroundDrawable(selected);
            showFilteredList("G");
        });
        binding.clear.setOnClickListener(v -> {
            clearSelection();
            binding.recyclerView.setAdapter(new MusicalNoteAdapter(requireContext(), musicalNotes, position -> {
                audio.stop();
                isPlaying = true;
                showPanel();
                binding.note.setText(musicalNotes.get(position).getNote(), TextView.BufferType.SPANNABLE);
                binding.frequency.setText(musicalNotes.get(position).getFrequency() + " Hz");
                audio.frequency = musicalNotes.get(position).getFrequency();
                audio.start();
                binding.btnPlay.setImageDrawable(getResources().getDrawable(R.drawable.ic_pause));
            }));
        });
    }

    private void showVolumeDialog() {
        Dialog audioDialog = new Dialog(requireContext());
        audioDialog.setContentView(R.layout.dialog_audio);

        ConstraintLayout mainLayout = audioDialog.findViewById(R.id.mainLayout);
        TextView volume = audioDialog.findViewById(R.id.volumeTxt);
        SeekBar volumeBar = audioDialog.findViewById(R.id.volumeBar);
        TextView btnOk = audioDialog.findViewById(R.id.btnOK);

        volumeBar.setMax(MAX_VOLUME);
        String vol = String.format(Locale.getDefault(), "%1.0f", (float) MyPreferences.getLastVolume()) + "%";
        volume.setText(vol);
        volumeBar.setProgress(MyPreferences.getLastVolume());
        volumeBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                audio.level = progress / (double) MAX_VOLUME;
                String vol = String.format(Locale.getDefault(), "%1.0f", (float) progress) + "%";
                MyPreferences.setLastVolume(progress);
                volume.setText(vol);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        volumeBar.setProgress(MyPreferences.getLastVolume());

        btnOk.setOnClickListener(v -> audioDialog.dismiss());
        audioDialog.setCancelable(false);

        DisplayMetrics displayMetrics = Resources.getSystem().getDisplayMetrics();
        int widthLcl = (int) (displayMetrics.widthPixels * 0.9f);
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) mainLayout.getLayoutParams();
        params.width = widthLcl;
        params.gravity = Gravity.CENTER;
        audioDialog.show();
        mainLayout.setLayoutParams(params);
        Window window = audioDialog.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    private void clearSelection() {
        Drawable unselected = getResources().getDrawable(R.drawable.bg_gradient);
        binding.A.setBackgroundDrawable(unselected);
        binding.B.setBackgroundDrawable(unselected);
        binding.C.setBackgroundDrawable(unselected);
        binding.D.setBackgroundDrawable(unselected);
        binding.E.setBackgroundDrawable(unselected);
        binding.F.setBackgroundDrawable(unselected);
        binding.G.setBackgroundDrawable(unselected);
    }

    private void showFilteredList(String a) {
        musicalNotesFiltered.clear();
        for (int i = 0; i < musicalNotes.size(); i++) {
            if (musicalNotes.get(i).getNote().toString().startsWith(a)) {
                musicalNotesFiltered.add(musicalNotes.get(i));
            }
        }
        binding.recyclerView.setAdapter(new MusicalNoteAdapter(requireContext(), musicalNotesFiltered, position -> {
            audio.stop();
            isPlaying = true;
            showPanel();
            binding.note.setText(musicalNotesFiltered.get(position).getNote(), TextView.BufferType.SPANNABLE);
            binding.frequency.setText(musicalNotesFiltered.get(position).getFrequency() + " Hz");
            audio.frequency = musicalNotesFiltered.get(position).getFrequency();
            audio.start();
            binding.btnPlay.setImageDrawable(getResources().getDrawable(R.drawable.ic_pause));
        }));
    }

    private void initSpinner() {
        ArrayAdapter<CharSequence> createFromResource = ArrayAdapter.createFromResource(requireContext(), R.array.tunings_array, R.layout.spinner_item);
        createFromResource.setDropDownViewResource(R.layout.spinner_item);
        binding.spinner.setAdapter(createFromResource);
        binding.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        _432Hz();
                        a4 = "432";
                        break;
                    case 1:
                        _434Hz();
                        a4 = "434";
                        break;
                    case 2:
                        _436Hz();
                        a4 = "436";
                        break;
                    case 3:
                        _438Hz();
                        a4 = "438";
                        break;
                    case 4:
                        _440Hz();
                        a4 = "440";
                        break;
                    case 5:
                        _442Hz();
                        a4 = "442";
                        break;
                    case 6:
                        _444Hz();
                        a4 = "444";
                        break;
                    case 7:
                        _446Hz();
                        a4 = "446";
                        break;
                }
                clearSelection();
                binding.recyclerView.setAdapter(new MusicalNoteAdapter(requireContext(), musicalNotes, Position -> {
                    audio.stop();
                    isPlaying = true;
                    showPanel();
                    binding.note.setText(musicalNotes.get(Position).getNote(), TextView.BufferType.SPANNABLE);
                    binding.frequency.setText(musicalNotes.get(Position).getFrequency() + " Hz");
                    audio.frequency = musicalNotes.get(Position).getFrequency();
                    audio.start();
                    binding.btnPlay.setImageDrawable(getResources().getDrawable(R.drawable.ic_pause));
                }));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    void showPanel() {
        Slide slide = new Slide();
        slide.setSlideEdge(Gravity.BOTTOM);
        slide.setDuration(300);
        binding.parent.setVisibility(View.VISIBLE);
        TransitionManager.beginDelayedTransition(binding.parent, slide);
        binding.touchLayout.setVisibility(View.VISIBLE);
    }

    public void hidePanel() {
        Slide slide = new Slide();
        slide.setSlideEdge(Gravity.BOTTOM);
        slide.setDuration(300);
        slide.addListener(new Transition.TransitionListener() {
            @Override
            public void onTransitionStart(@NonNull Transition transition) {
            }

            @Override
            public void onTransitionEnd(@NonNull Transition transition) {
                binding.parent.setVisibility(View.GONE);
            }

            @Override
            public void onTransitionCancel(@NonNull Transition transition) {
            }

            @Override
            public void onTransitionPause(@NonNull Transition transition) {
            }

            @Override
            public void onTransitionResume(@NonNull Transition transition) {
            }
        });
        TransitionManager.beginDelayedTransition(binding.parent, slide);
        binding.touchLayout.setVisibility(View.GONE);
    }

    public void _432Hz() {
        musicalNotes.clear();
        musicalNotes.addAll(FrequencyUtils._432Hz());
    }

    public void _434Hz() {
        musicalNotes.clear();
        musicalNotes.addAll(FrequencyUtils._434Hz());
    }

    public void _436Hz() {
        musicalNotes.clear();
        musicalNotes.addAll(FrequencyUtils._436Hz());
    }

    public void _438Hz() {
        musicalNotes.clear();
        musicalNotes.addAll(FrequencyUtils._438Hz());
    }

    public void _440Hz() {
        musicalNotes.clear();
        musicalNotes.addAll(FrequencyUtils._440Hz());
    }

    public void _442Hz() {
        musicalNotes.clear();
        musicalNotes.addAll(FrequencyUtils._442Hz());
    }

    public void _444Hz() {
        musicalNotes.clear();
        musicalNotes.addAll(FrequencyUtils._444Hz());
    }

    public void _446Hz() {
        musicalNotes.clear();
        musicalNotes.addAll(FrequencyUtils._446Hz());
    }
}
