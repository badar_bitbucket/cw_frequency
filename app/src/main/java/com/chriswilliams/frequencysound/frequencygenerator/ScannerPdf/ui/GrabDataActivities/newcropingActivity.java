package com.chriswilliams.frequencysound.frequencygenerator.ScannerPdf.ui.GrabDataActivities;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.chriswilliams.frequencysound.frequencygenerator.R;
import com.chriswilliams.frequencysound.frequencygenerator.databinding.ActivityNewcropingBinding;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.text.FirebaseVisionText;
import com.google.firebase.ml.vision.text.FirebaseVisionTextDetector;
import com.lw.internalmarkiting.ui.activities.BaseActivity;
import java.io.File;
import java.util.List;
public class newcropingActivity extends BaseActivity<ActivityNewcropingBinding> {
    public static final String EXTRA_FILE = "extra_file";
    public static final String EXTRA_SAVE_OUTPUT = "extra_save_output";
    private static final String EXTRA_BUSINESS_CARD = "businessCard";
    private static final String EXTRA_FROM_GALLERY = "extra_from_gallery";
    private static final int CAMERA_REQUEST = 1888;
    final String NAME_REGEX = "^([A-Z]([a-z]*|\\.) *){1,2}([A-Z][a-z]+-?)+$";
    final String EMAIL_REGEX = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    final String NUMBER = "(^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\\s\\./0-9]*$)";
    boolean saveOutput;
    boolean fromGallery;
    int businessCard;
    String text;
    private Bitmap croppedImage;
    private File imageFile;
    Data data;
    public static void start(Context context, String path, boolean saveOutput, boolean fromGallery) {
        Intent starter = new Intent(context, newcropingActivity.class);
        starter.putExtra(EXTRA_FILE, path);
        starter.putExtra(EXTRA_SAVE_OUTPUT, saveOutput);
        starter.putExtra(EXTRA_FROM_GALLERY, fromGallery);
        context.startActivity(starter);
    }

    public static void start(Context context, String path, boolean saveOutput, int businessCard) {
        Intent starter = new Intent(context, newcropingActivity.class);
        starter.putExtra(EXTRA_FILE, path);
        starter.putExtra(EXTRA_SAVE_OUTPUT, saveOutput);
        starter.putExtra(EXTRA_BUSINESS_CARD, businessCard);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onReady() {
        data = new Data();
        String filePath = getIntent().getStringExtra(EXTRA_FILE);
        imageFile = new File(filePath);
        saveOutput = getIntent().getBooleanExtra(EXTRA_SAVE_OUTPUT, true);
        fromGallery = getIntent().getBooleanExtra(EXTRA_FROM_GALLERY, false);
        businessCard = getIntent().getIntExtra(EXTRA_BUSINESS_CARD, -1);
        if (fromGallery) {
            imageFile = new File(getBatchDirectoryName(), System.currentTimeMillis() + ".png");
            Glide.with(context).asBitmap().load(Uri.parse(filePath)).into(new CustomTarget<Bitmap>() {
                @Override
                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                    croppedImage = resource;
                    binding.cropImageView.setImageBitmap(resource);
                }

                @Override
                public void onLoadCleared(@Nullable Drawable placeholder) {
                }
            });
        } else {
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            croppedImage = BitmapFactory.decodeFile(imageFile.getAbsolutePath(), bmOptions);
            binding.cropImageView.setImageBitmap(croppedImage);
        }
        binding.fillView.setOnClickListener(v -> binding.cropImageView.resetCropRect());
        binding.next.setOnClickListener(v -> DetectTextFromImage( binding.cropImageView.getCroppedImage()));
        binding.back.setOnClickListener(v -> finish());
    }


    @Override
    protected int getResId() {
        return R.layout.activity_newcroping;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK && data != null) {
            Uri uri = data.getData();
            Glide.with(getBaseContext()).asBitmap().load(uri).into(new CustomTarget<Bitmap>() {
                @Override
                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                    croppedImage = resource;
                    binding.cropImageView.setImageBitmap(croppedImage);
                }

                @Override
                public void onLoadCleared(@Nullable Drawable placeholder) {

                }
            });
        }
    }

    private void DetectTextFromImage(Bitmap croppedImage) {
        FirebaseVisionImage firebaseVisionImage = FirebaseVisionImage.fromBitmap(croppedImage);
        FirebaseVisionTextDetector textDetector = FirebaseVision.getInstance().getVisionTextDetector();
        textDetector.detectInImage(firebaseVisionImage).addOnSuccessListener(firebaseVisionText -> {
            List<FirebaseVisionText.Block> blockList = firebaseVisionText.getBlocks();
            if (blockList.size() == 0) {
                Toast.makeText(newcropingActivity.this, "No Text Found in image", Toast.LENGTH_LONG).show();
            } else {
                for (FirebaseVisionText.Block block : firebaseVisionText.getBlocks()) {

                    text = block.getText();
                    if (text.matches(EMAIL_REGEX)) {
                        data.setEmail(text);
                    } else if (text.matches(NAME_REGEX)) {
                        data.setName(text);
                    }
                    if (text.matches(NUMBER)) {

                        data.setNumber(text);
                    } else
                        Log.i("DetectText ", "text: " + text);
                }
                Intent FatchData = new Intent(newcropingActivity.this, FatchActivity.class);
                FatchData.putExtra("data", data);
                startActivity(FatchData);
            }
        }).addOnFailureListener(e -> Toast.makeText(newcropingActivity.this, "Something Went Wrong", Toast.LENGTH_LONG).show());
    }

    public String getBatchDirectoryName() {
        File dir = new File(getFilesDir().getPath() + "/pdf");
        if (!dir.exists() && !dir.mkdirs()) {
        }
        return dir.getAbsolutePath();
    }
}