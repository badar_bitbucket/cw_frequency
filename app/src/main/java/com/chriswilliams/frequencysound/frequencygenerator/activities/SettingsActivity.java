package com.chriswilliams.frequencysound.frequencygenerator.activities;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import androidx.fragment.app.FragmentTransaction;
import com.chriswilliams.frequencysound.frequencygenerator.R;
import com.chriswilliams.frequencysound.frequencygenerator.databinding.ActivitySettingsBinding;
import com.chriswilliams.frequencysound.frequencygenerator.dialog.SetRangeDialog;
import com.chriswilliams.frequencysound.frequencygenerator.dialog.SetStepDialog;
import com.chriswilliams.frequencysound.frequencygenerator.prefernces.MyPreferences;
import com.lw.internalmarkiting.ui.activities.BaseActivity;
import java.text.DecimalFormat;
public class SettingsActivity extends BaseActivity<ActivitySettingsBinding> {
    private final DecimalFormat decimalFormat = new DecimalFormat();
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, SettingsActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected int getResId() {
        return R.layout.activity_settings;
    }

    @Override
    protected void onReady() {
        setTitle("Settings");
        String range = decimalFormat.format(MyPreferences.getMinFrequency())
                + " - " + decimalFormat.format(MyPreferences.getMaxFrequency()) + " Hz";
        binding.prefSetRangeSummary.setText(range);
        String step = decimalFormat.format(MyPreferences.getStep()) + " Hz";
        binding.prefSetStepSummary.setText(step);
        binding.rangeCardView.setOnClickListener(this::showRangeDialog);
        binding.stepCardView.setOnClickListener(this::showStepDialog);
    }

    public void showRangeDialog(View view) {
        SetRangeDialog setRangeDialog = new SetRangeDialog();
        if (!setRangeDialog.isStateSaved() && !setRangeDialog.isVisible()) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.add(setRangeDialog, "set_range_dialog");
            transaction.commit();
        }
    }

    public void showStepDialog(View view) {
        SetStepDialog stepDialog = new SetStepDialog();
        if (!stepDialog.isStateSaved() && !stepDialog.isVisible()) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.add(stepDialog, "set_step_dialog");
            transaction.commit();
        }
    }
}