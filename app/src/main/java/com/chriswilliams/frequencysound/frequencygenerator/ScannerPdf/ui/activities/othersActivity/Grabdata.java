package com.chriswilliams.frequencysound.frequencygenerator.ScannerPdf.ui.activities.othersActivity;

import android.content.Intent;
import android.os.Bundle;

import com.chriswilliams.frequencysound.frequencygenerator.R;
import com.chriswilliams.frequencysound.frequencygenerator.ScannerPdf.ui.GrabDataActivities.cameraActivityCardInfo;
import com.chriswilliams.frequencysound.frequencygenerator.databinding.ActivityGrabdataBinding;
import com.lw.internalmarkiting.ui.activities.BaseActivity;


public class Grabdata extends BaseActivity<ActivityGrabdataBinding> {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handling();
    }

    @Override
    protected void onReady() {}

    @Override
    protected int getResId() {
        return R.layout.activity_grabdata;
    }
    public void handling()
    {
        binding.grabData.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), cameraActivityCardInfo.class);
            startActivity(intent);
        });
        binding.bCard.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), CardCameraActivity.class);
            startActivity(intent);
        });
        binding.bback.setOnClickListener(v -> finish());
    }
}