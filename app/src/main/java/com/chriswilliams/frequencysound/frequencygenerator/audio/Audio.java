package com.chriswilliams.frequencysound.frequencygenerator.audio;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.media.audiofx.BassBoost;

public class Audio implements Runnable {
    public static final int SINE = 0;
    public static final int SQUARE = 1;
    public static final int SAWTOOTH = 2;
    public int waveform;
    public boolean mute;
    public double frequency;
    public double level;
    private Thread thread;

    public Audio() {
        frequency = 440.0;
        level = 16384;
    }

    public void start() {
        thread = new Thread(this, "Audio");
        thread.start();
    }

    public void stop() {
        Thread t = thread;
        thread = null;
        while (t != null && t.isAlive())
            Thread.yield();
    }

    public void run() {
        processAudio();
    }

    private void processAudio() {
        short[] buffer;
        int rate = AudioTrack.getNativeOutputSampleRate(AudioManager.STREAM_MUSIC);
        int minSize = AudioTrack.getMinBufferSize(rate, AudioFormat.CHANNEL_OUT_MONO,
                AudioFormat.ENCODING_PCM_16BIT);
        int[] sizes = {1024, 2048, 4096, 8192, 16384, 32768};
        int size = 0;
        for (int s : sizes) {
            if (s > minSize) {
                size = s;
                break;
            }
        }

        final double K = 2.0 * Math.PI / rate;
        AudioTrack audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC, rate,
                AudioFormat.CHANNEL_OUT_MONO,
                AudioFormat.ENCODING_PCM_16BIT,
                size, AudioTrack.MODE_STREAM);
        int state = audioTrack.getState();
        if (state != AudioTrack.STATE_INITIALIZED) {
            audioTrack.release();
            return;
        }

        audioTrack.play();
        buffer = new short[size];
        double f = frequency;
        double l = 0.0;
        double q = 0.0;
        while (thread != null) {
            for (int i = 0; i < buffer.length; i++) {
                f += (frequency - f) / 4096.0;
                l += ((mute ? 0.0 : level) * 16384.0 - l) / 4096.0;
                q += (q < Math.PI) ? f * K : (f * K) - (2.0 * Math.PI);
                switch (waveform) {
                    case SINE:
                        buffer[i] = (short) Math.round(Math.sin(q) * l);
                        break;
                    case SQUARE:
                        buffer[i] = (short) ((q > 0.0) ? l : -l);
                        break;
                    case SAWTOOTH:
                        buffer[i] = (short) Math.round((q / Math.PI) * l);
                        break;
                }
            }
            audioTrack.write(buffer, 0, buffer.length);
        }
        audioTrack.stop();
        audioTrack.release();
    }
}
