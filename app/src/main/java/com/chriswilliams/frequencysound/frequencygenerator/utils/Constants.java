package com.chriswilliams.frequencysound.frequencygenerator.utils;

public class Constants {
    public static final String LOCK = "SigGen:lock";
    public static final int MAX_VOLUME = 100;
    public static final String SINE = "sine";
    public static final String SQUARE = "square";
    public static final String SAWTOOTH = "sawtooth";
    public static final String LAST_WAVE_FORM = "last_waveform";
    public static final String LAST_WAVE_FORM_1 = "last_waveform_1";
    public static final String LAST_WAVE_FORM_2 = "last_waveform_2";
    public static final String LAST_WAVE_FORM_3 = "last_waveform_3";
    public static final String LAST_VOLUME = "last_volume";
    public static final String SLIDER_RANGE_FROM = "slider_range_from";
    public static final String SLIDER_RANGE_FROM_1 = "slider_range_from_1";
    public static final String SLIDER_RANGE_FROM_2 = "slider_range_from_2";
    public static final String SLIDER_RANGE_FROM_3 = "slider_range_from_3";
    public static final String SLIDER_RANGE_TO = "slider_range_to";
    public static final String SLIDER_RANGE_TO_1 = "slider_range_to_1";
    public static final String SLIDER_RANGE_TO_2 = "slider_range_to_2";
    public static final String SLIDER_RANGE_TO_3 = "slider_range_to_3";
    public static final String LAST_FREQUENCY = "last_frequency";
    public static final String LAST_FREQUENCY_1 = "last_frequency_1";
    public static final String LAST_FREQUENCY_2 = "last_frequency_2";
    public static final String LAST_FREQUENCY_3 = "last_frequency_3";
    public static final String THEME = "theme";
    public static final String STEP = "pref_step";
    public static final String DECIMAL_PRECISION = "pref_decimal_precision";
    public static final String SWEEP_DURATION = "sweep_duration";
    public static final String START_FREQUENCY = "start_frequency";
    public static final String END_FREQUENCY = "end_frequency";
    public static final String SWEEP_REPEAT = "sweep_repeat";
}