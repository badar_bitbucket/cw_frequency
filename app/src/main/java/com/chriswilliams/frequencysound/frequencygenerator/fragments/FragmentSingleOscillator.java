package com.chriswilliams.frequencysound.frequencygenerator.fragments;

import android.app.Dialog;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.method.DigitsKeyListener;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.chriswilliams.frequencysound.frequencygenerator.R;
import com.chriswilliams.frequencysound.frequencygenerator.audio.Audio;
import com.chriswilliams.frequencysound.frequencygenerator.databinding.FragmentSingleOscillatorBinding;
import com.chriswilliams.frequencysound.frequencygenerator.prefernces.MyPreferences;
import com.chriswilliams.frequencysound.frequencygenerator.utils.DecimalDigitInputFilter;
import com.chriswilliams.frequencysound.frequencygenerator.utils.EditTextInputChecker;
import com.chriswilliams.frequencysound.frequencygenerator.utils.Utils;
import com.lw.internalmarkiting.ui.base.BindingFragment;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import static com.chriswilliams.frequencysound.frequencygenerator.utils.Constants.MAX_VOLUME;
import static com.chriswilliams.frequencysound.frequencygenerator.utils.Constants.SAWTOOTH;
import static com.chriswilliams.frequencysound.frequencygenerator.utils.Constants.SINE;
import static com.chriswilliams.frequencysound.frequencygenerator.utils.Constants.SQUARE;

public class FragmentSingleOscillator extends BindingFragment<FragmentSingleOscillatorBinding> implements SeekBar.OnSeekBarChangeListener {
    String lastWaveForm;
    Audio audio;
    Handler handler;
    Spannable spannableString;
    private boolean isPlaying = true;
    private Runnable seekbarHider = () -> binding.volumeBar.setVisibility(View.GONE);

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_single_oscillator;
    }

    @Override
    protected void onReady() {
        setHasOptionsMenu(true);
        handler = new Handler();
        initViews();
        setOnclickListeners();
    }

    @Override
    public void onResume() {
        super.onResume();
        requireActivity().setTitle("Single-Oscillator");
        initFrequencyBar();
        audio.frequency = MyPreferences.getLastFrequency();
        audio.level = MyPreferences.getLastVolume() / (float) MAX_VOLUME;
        String vol = String.format(Locale.getDefault(), "%1.0f", (float) MyPreferences.getLastVolume()) + "%";
        binding.volume.setText(vol);
        binding.volumeBar.setProgress(MyPreferences.getLastVolume());
    }

    @Override
    public void onPause() {
        super.onPause();
        binding.playStop.setImageResource(R.drawable.ic_play);
        audio.stop();
        isPlaying = true;
    }

    private void initViews() {
        audio = new Audio();
        initWaveForm();
        binding.volumeBar.setMax(MAX_VOLUME);
        binding.volumeBar.setOnSeekBarChangeListener(this);
    }

    private void initWaveForm() {
        lastWaveForm = MyPreferences.getLastWaveForm();
        switch (lastWaveForm) {
            case SINE:
                binding.sine.setImageDrawable(getResources().getDrawable(R.drawable.ic_sine_wave));
                audio.waveform = Audio.SINE;
                break;
            case SQUARE:
                binding.sine.setImageDrawable(getResources().getDrawable(R.drawable.ic_square_wave));
                audio.waveform = Audio.SQUARE;
                break;
            case SAWTOOTH:
                binding.sine.setImageDrawable(getResources().getDrawable(R.drawable.ic_sawtooth_wave));
                audio.waveform = Audio.SAWTOOTH;
                break;
        }
    }

    private void setOnclickListeners() {
        binding.playStop.setOnClickListener(view -> {
            if (!isPlaying) {
                binding.playStop.setImageResource(R.drawable.ic_play);
                audio.stop();
                isPlaying = true;
            } else {
                binding.playStop.setImageResource(R.drawable.ic_stop_black_24dp);
                audio.start();
                isPlaying = false;
            }
        });
        binding.lower.setOnClickListener(view -> frequencyDown());
        binding.higher.setOnClickListener(view -> frequencyUp());
        binding.sine.setOnClickListener(view -> {
            switch (MyPreferences.getLastWaveForm()) {
                case SINE:
                    binding.sine.setImageDrawable(getResources().getDrawable(R.drawable.ic_square_wave));
                    audio.waveform = Audio.SQUARE;
                    MyPreferences.setLastWaveForm(SQUARE);
                    break;
                case SQUARE:
                    binding.sine.setImageDrawable(getResources().getDrawable(R.drawable.ic_sawtooth_wave));
                    audio.waveform = Audio.SAWTOOTH;
                    MyPreferences.setLastWaveForm(SAWTOOTH);
                    break;
                case SAWTOOTH:
                    binding.sine.setImageDrawable(getResources().getDrawable(R.drawable.ic_sine_wave));
                    audio.waveform = Audio.SINE;
                    MyPreferences.setLastWaveForm(SINE);
                    break;
            }
        });
        binding.volumeButton.setOnClickListener(view -> {
            binding.volumeBar.setVisibility(View.VISIBLE);
            hideSeekbar();
        });
    }

    private void frequencyUp() {
        int progress = (int) (binding.frequencyBar.getProgress() + MyPreferences.getMinFrequency());
        float step = MyPreferences.getStep();
        if ((progress + step) > MyPreferences.getMaxFrequency()) {
            binding.frequencyBar.setProgress((int) (MyPreferences.getMaxFrequency() - MyPreferences.getMinFrequency()));
        } else {
            binding.frequencyBar.setProgress((int) (progress + step - MyPreferences.getMinFrequency()));
        }
    }

    private void frequencyDown() {
        int progress = (int) (binding.frequencyBar.getProgress() + MyPreferences.getMinFrequency());
        float step = MyPreferences.getStep();
        if ((progress - step) < MyPreferences.getMinFrequency()) {
            binding.frequencyBar.setProgress(0);
        } else {
            binding.frequencyBar.setProgress((int) (progress - step - MyPreferences.getMinFrequency()));
        }
    }

    private void hideSeekbar() {
        handler.postDelayed(seekbarHider, 2200);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        int id = seekBar.getId();
        if (audio == null)
            return;
        switch (id) {
            case R.id.frequencyBar: {
                progress = (int) (progress + MyPreferences.getMinFrequency());
                audio.frequency = progress;
                String freq = String.format(Locale.getDefault(), "%1.0f", (float) progress) + "\nHz\n\n";
                MyPreferences.setLastFrequency(progress);
                spannableString = new SpannableString(freq);
                spannableString.setSpan(new RelativeSizeSpan(2f),
                        0, freq.lastIndexOf("H"), 0);
                spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, freq.lastIndexOf("H"), 0);
                binding.frequency.setText(spannableString);
            }
            break;
            case R.id.volumeBar:
                audio.level = progress / (double) MAX_VOLUME;
                String vol = String.format(Locale.getDefault(), "%1.0f", (float) progress) + "%";
                MyPreferences.setLastVolume(progress);
                binding.volume.setText(vol);
                break;
        }
    }

    private void initFrequencyBar() {
        float max = MyPreferences.getMaxFrequency();
        float min = MyPreferences.getMinFrequency();
        setProgressBarAndFreqText(min, max);
        setMinMaxFreqText(min, max);
        binding.frequencyBar.setOnSeekBarChangeListener(this);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        if (seekBar.getId() == binding.volumeBar.getId()) {
            handler.removeCallbacks(seekbarHider);
        }
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        if (seekBar.getId() == binding.volumeBar.getId()) {
            hideSeekbar();
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.range:
                showRangeDialog();
                break;
            case R.id.step:
                showStepDialog();
                break;
        }
        return true;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.more_tab_menu, menu);
    }

    public void showRangeDialog() {
        Dialog dialog = new Dialog(requireContext());
        dialog.setContentView(R.layout.dialog_range);
        DecimalFormat decimalFormat = new DecimalFormat();

        ConstraintLayout mainLayout = dialog.findViewById(R.id.mainLayout);
        EditText rangeEditTextFrom = dialog.findViewById(R.id.pref_range_editText_from);
        EditText rangeEditTextTo = dialog.findViewById(R.id.pref_range_editText_to);
        TextView buttonSetRangeOK = dialog.findViewById(R.id.button_set_range_OK);
        TextView button_set_range_CANCEL = dialog.findViewById(R.id.button_set_range_CANCEL);
        TextView button_set_range_RESET = dialog.findViewById(R.id.button_set_range_RESET);

        rangeEditTextFrom.setFilters(new InputFilter[]{new DecimalDigitInputFilter(5, 2)});
        rangeEditTextTo.setFilters(new InputFilter[]{new DecimalDigitInputFilter(5, 2)});
        decimalFormat.setGroupingUsed(false);
        if (MyPreferences.getIsDecimalPrecision()) {
            decimalFormat.setMaximumFractionDigits(2);
            rangeEditTextFrom.setText(decimalFormat.format(MyPreferences.getMinFrequency()));
            rangeEditTextTo.setText(decimalFormat.format(MyPreferences.getMaxFrequency()));
            rangeEditTextFrom.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);
            rangeEditTextTo.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);
            char decimalSeparator = DecimalFormatSymbols.getInstance().getDecimalSeparator();
            String str = "0123456789";
            String sb = str + decimalSeparator;
            rangeEditTextFrom.setKeyListener(DigitsKeyListener.getInstance(sb));
            String sb2 = str + decimalSeparator;
            rangeEditTextTo.setKeyListener(DigitsKeyListener.getInstance(sb2));
        } else {
            decimalFormat.setMaximumFractionDigits(0);
            rangeEditTextFrom.setText(decimalFormat.format(MyPreferences.getMinFrequency()));
            rangeEditTextTo.setText(decimalFormat.format(MyPreferences.getMaxFrequency()));
        }
        buttonSetRangeOK.setOnClickListener(view1 -> {
            EditTextInputChecker inputChecker = new EditTextInputChecker();
            inputChecker.checker(rangeEditTextFrom.getText().toString(), rangeEditTextTo.getText().toString(), 1);
            EditTextInputChecker inputChecker1 = new EditTextInputChecker();
            inputChecker1.checker(rangeEditTextTo.getText().toString(), rangeEditTextFrom.getText().toString(), 2);
            if (!inputChecker.isValidInput || !inputChecker1.isValidInput) {
                if (!inputChecker.isValidInput) {
                    rangeEditTextFrom.setError(inputChecker.warning);
                }
                if (!inputChecker1.isValidInput) {
                    rangeEditTextTo.setError(inputChecker1.warning);
                }
                return;
            }
            float minFreq = Utils.FormatNumber(rangeEditTextFrom.getText().toString());
            float maxFreq = Utils.FormatNumber(rangeEditTextTo.getText().toString());
            MyPreferences.setMinFrequency(minFreq);
            MyPreferences.setMaxFrequency(maxFreq);
            setMinMaxFreqText(MyPreferences.getMinFrequency(), MyPreferences.getMaxFrequency());
            setProgressBarAndFreqText(MyPreferences.getMinFrequency(), MyPreferences.getMaxFrequency());
            dialog.dismiss();
        });
        button_set_range_CANCEL.setOnClickListener(view1 -> dialog.dismiss());
        button_set_range_RESET.setOnClickListener(view1 -> {
            rangeEditTextFrom.setText(String.valueOf(1));
            rangeEditTextTo.setText(String.valueOf(22000));
        });

        DisplayMetrics displayMetrics = Resources.getSystem().getDisplayMetrics();
        int widthLcl = (int) (displayMetrics.widthPixels * 0.9f);
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) mainLayout.getLayoutParams();
        params.width = widthLcl;
        params.gravity = Gravity.CENTER;
        dialog.show();
        mainLayout.setLayoutParams(params);
        Window window = dialog.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    public void showStepDialog() {
        Dialog dialog = new Dialog(requireContext());
        dialog.setContentView(R.layout.dialog_step);
        DecimalFormat decimalFormat = new DecimalFormat();

        ConstraintLayout mainLayout = dialog.findViewById(R.id.mainLayout);
        AppCompatRadioButton radioButtonStep_1 = dialog.findViewById(R.id.radioButton_step_1);
        AppCompatRadioButton radioButtonStep_10 = dialog.findViewById(R.id.radioButton_step_10);
        AppCompatRadioButton radioButtonStep_100 = dialog.findViewById(R.id.radioButton_step_100);
        AppCompatRadioButton radioButtonStepCustom = dialog.findViewById(R.id.radioButton_step_custom);
        EditText editTextCustomStep = dialog.findViewById(R.id.edittext_custom_step);

        TextView buttonSetStep_OK = dialog.findViewById(R.id.button_set_step_OK);
        TextView buttonSetStep_CANCEL = dialog.findViewById(R.id.button_set_step_CANCEL);

        editTextCustomStep.setFilters(new InputFilter[]{new DecimalDigitInputFilter(5, 2)});
        char decimalSeparator = DecimalFormatSymbols.getInstance().getDecimalSeparator();

        String sb = "0123456789" + decimalSeparator;
        editTextCustomStep.setKeyListener(DigitsKeyListener.getInstance(sb));
        decimalFormat.setMaximumFractionDigits(2);
        decimalFormat.setGroupingUsed(false);
        if (!MyPreferences.getIsDecimalPrecision()) {
            editTextCustomStep.setInputType(InputType.TYPE_CLASS_NUMBER);
            decimalFormat.setMaximumFractionDigits(0);
        }
        float k = MyPreferences.getStep();
        if (((double) Math.abs(k - 1.0f)) < 1.0E-5d) {
            radioButtonStep_1.setChecked(true);
        } else if (((double) Math.abs(k - 10.0f)) < 1.0E-5d) {
            radioButtonStep_10.setChecked(true);
        } else if (((double) Math.abs(k - 100.0f)) < 1.0E-5d) {
            radioButtonStep_100.setChecked(true);
        } else {
            radioButtonStepCustom.setChecked(true);
            editTextCustomStep.setText(decimalFormat.format(MyPreferences.getStep()));
            editTextCustomStep.setVisibility(View.VISIBLE);
        }
        radioButtonStepCustom.setOnClickListener(view1 -> {
            if (editTextCustomStep.getVisibility() == View.GONE) {
                editTextCustomStep.setVisibility(View.VISIBLE);
            }
        });
        radioButtonStep_1.setOnClickListener(view1 -> {
            if (editTextCustomStep.getVisibility() == View.VISIBLE) {
                editTextCustomStep.setVisibility(View.INVISIBLE);
            }
        });
        radioButtonStep_10.setOnClickListener(view1 -> {
            if (editTextCustomStep.getVisibility() == View.VISIBLE) {
                editTextCustomStep.setVisibility(View.INVISIBLE);
            }
        });
        radioButtonStep_100.setOnClickListener(view1 -> {

            if (editTextCustomStep.getVisibility() == View.VISIBLE) {
                editTextCustomStep.setVisibility(View.INVISIBLE);
            }
        });
        buttonSetStep_OK.setOnClickListener(view2 -> {
            if (radioButtonStep_1.isChecked()) {
                MyPreferences.setStep(1.0f);
            } else if (radioButtonStep_10.isChecked()) {
                MyPreferences.setStep(10.0f);
            } else if (radioButtonStep_100.isChecked()) {
                MyPreferences.setStep(100.0f);
            } else if (radioButtonStepCustom.isChecked()) {
                EditTextInputChecker checker = new EditTextInputChecker();
                checker.inputChecker(editTextCustomStep.getText().toString());
                if (!checker.isValidInput) {
                    editTextCustomStep.setError(checker.warning);
                    return;
                }
                MyPreferences.setStep(Utils.FormatNumber(editTextCustomStep.getText().toString()));
            }
            dialog.dismiss();
        });
        buttonSetStep_CANCEL.setOnClickListener(view1 -> dialog.dismiss());

        DisplayMetrics displayMetrics = Resources.getSystem().getDisplayMetrics();
        int widthLcl = (int) (displayMetrics.widthPixels * 0.9f);
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) mainLayout.getLayoutParams();
        params.width = widthLcl;
        params.gravity = Gravity.CENTER;
        dialog.show();
        mainLayout.setLayoutParams(params);
        Window window = dialog.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    private void setMinMaxFreqText(float minFrequency, float maxFrequency) {
        String minFreq = String.format(Locale.getDefault(), "%1.0f", minFrequency) + "Hz";
        binding.freqStart.setText(minFreq);
        String maxFreq = String.format(Locale.getDefault(), "%1.0f", maxFrequency) + "Hz";
        binding.freqEnd.setText(maxFreq);
    }

    private void setProgressBarAndFreqText(float min, float max) {
        float freq;
        if (MyPreferences.getLastFrequency() > max)
            freq = max;
        else freq = Math.max(MyPreferences.getLastFrequency(), min);
        binding.frequencyBar.setMax((int) (max - min));
        binding.frequencyBar.setProgress((int) (freq - min));
        String sFreq = String.format(Locale.getDefault(), "%1.0f", freq) + "\nHz\n\n";
        spannableString = new SpannableString(sFreq);
        spannableString.setSpan(new RelativeSizeSpan(2f),
                0, sFreq.lastIndexOf("H"), 0);
        spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, sFreq.lastIndexOf("H"), 0);
        binding.frequency.setText(spannableString);
    }
}