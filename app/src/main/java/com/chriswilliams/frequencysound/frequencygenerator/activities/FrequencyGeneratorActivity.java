package com.chriswilliams.frequencysound.frequencygenerator.activities;
import static com.lw.internalmarkiting.ui.activities.ExitActivity.CODE_EXIT;
import android.content.Context;
import android.content.Intent;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;
import com.chriswilliams.frequencysound.frequencygenerator.R;
import com.chriswilliams.frequencysound.frequencygenerator.databinding.ContentMainNewBinding;
import com.chriswilliams.frequencysound.frequencygenerator.fragments.FragmentMultiOscillator;
import com.chriswilliams.frequencysound.frequencygenerator.fragments.FragmentMusicalNote;
import com.chriswilliams.frequencysound.frequencygenerator.fragments.FragmentSingleOscillator;
import com.chriswilliams.frequencysound.frequencygenerator.fragments.FragmentSweepGenerator;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.lw.internalmarkiting.ui.activities.BaseActivity;
import java.util.ArrayList;
import java.util.List;
public class FrequencyGeneratorActivity extends BaseActivity<ContentMainNewBinding> {
    TabLayoutMediator tabLayoutMediator;
    TabLayout tabLayout;
    ViewPager2 viewPager;

    public static void start(Context context) {
        Intent starter = new Intent(context, FrequencyGeneratorActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected void onReady() {
        setTabLayoutAndViewPager();
        binding.bback.setOnClickListener(v -> finish());
    }

    @Override
    protected int getResId() {
        return R.layout.content_main_new;
    }

    private void setTabLayoutAndViewPager() {
        List<String> names = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            names.add("option " + i);
        }
        tabLayout = binding.tabLayout;
        viewPager = binding.viewpager;
        viewPager.setUserInputEnabled(false);
        viewPager.setOffscreenPageLimit(names.size());
        viewPager.setAdapter(new SectionsPagerAdapter(activity, names));
        tabLayoutMediator = new TabLayoutMediator(tabLayout, viewPager, (tab, position) -> {
            switch (position) {
                case 0:
                    tab.setIcon(getResources().getDrawable(R.drawable.ic_sine_wave));
                    break;
                case 1:
                    tab.setIcon(getResources().getDrawable(R.drawable.ic_multioscillator));
                    break;
                case 2:
                    tab.setIcon(getResources().getDrawable(R.drawable.ic_musical_notes));
                    break;
                case 3:
                    tab.setIcon(getResources().getDrawable(R.drawable.ic_sweep));
                    break;
            }
        });
        tabLayoutMediator.attach();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CODE_EXIT) {
            if (resultCode == RESULT_OK) {
                finishAffinity();
            }
        }
    }

    public static class SectionsPagerAdapter extends FragmentStateAdapter {
        List<String> appsName;

        public SectionsPagerAdapter(FragmentActivity fragmentActivity, List<String> apps) {
            super(fragmentActivity);
            appsName = apps;
        }

        @NonNull
        @Override
        public Fragment createFragment(int position) {
            switch (position) {
                default:
                case 0:
                    return new FragmentSingleOscillator();
                case 1:
                    return new FragmentMultiOscillator();
                case 2:
                    return new FragmentMusicalNote();
                case 3:
                    return new FragmentSweepGenerator();

            }
        }

        @Override
        public int getItemCount() {
            return appsName.size();
        }
    }

}
