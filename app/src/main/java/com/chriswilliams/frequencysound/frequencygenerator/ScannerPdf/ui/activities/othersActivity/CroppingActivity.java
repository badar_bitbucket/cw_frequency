package com.chriswilliams.frequencysound.frequencygenerator.ScannerPdf.ui.activities.othersActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.chriswilliams.frequencysound.frequencygenerator.ScannerPdf.ui.bottomSaving_Activity.BottomSheetForSaving;
import com.chriswilliams.frequencysound.frequencygenerator.ScannerPdf.utils.ScannerConstants;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.lw.internalmarkiting.task.BaseObserver;
import com.lw.internalmarkiting.task.ObservableProvider;
import com.chriswilliams.frequencysound.frequencygenerator.R;
import com.chriswilliams.frequencysound.frequencygenerator.databinding.ActivityCroppingBinding;
import com.lw.internalmarkiting.ui.activities.BaseActivity;


import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import io.reactivex.ObservableOnSubscribe;


public class CroppingActivity extends BaseActivity<ActivityCroppingBinding> implements BottomSheetForSaving.SavingInterface {

    public static final String EXTRA_FILE = "extra_file";
    public static final String EXTRA_SAVE_OUTPUT = "extra_save_output";
    private static final String EXTRA_BUSINESS_CARD = "businessCard";
    private static final String EXTRA_FROM_GALLERY = "extra_from_gallery";
    boolean saveOutput;
    boolean fromGallery;
    int businessCard;
    private Bitmap croppedImage;
    private File imageFile;
    public static void start(Context context, String path, boolean saveOutput, boolean fromGallery) {
        Intent starter = new Intent(context, CroppingActivity.class);
        starter.putExtra(EXTRA_FILE, path);
        starter.putExtra(EXTRA_SAVE_OUTPUT, saveOutput);
        starter.putExtra(EXTRA_FROM_GALLERY, fromGallery);
        context.startActivity(starter);
    }
    public static void start(Context context, String path, boolean saveOutput, int businessCard) {
        Intent starter = new Intent(context, CroppingActivity.class);
        starter.putExtra(EXTRA_FILE, path);
        starter.putExtra(EXTRA_SAVE_OUTPUT, saveOutput);
        starter.putExtra(EXTRA_BUSINESS_CARD, businessCard);
        context.startActivity(starter);
    }
    @Override
    public void onReady() {

        String filePath = getIntent().getStringExtra(EXTRA_FILE);

        imageFile = new File(filePath);
        saveOutput = getIntent().getBooleanExtra(EXTRA_SAVE_OUTPUT, true);
        fromGallery = getIntent().getBooleanExtra(EXTRA_FROM_GALLERY, false);
        businessCard = getIntent().getIntExtra(EXTRA_BUSINESS_CARD, -1);
        if (fromGallery) {
            imageFile = new File(getBatchDirectoryName(), System.currentTimeMillis() + ".png");
            Glide.with(context).asBitmap().load(Uri.parse(filePath)).into(new CustomTarget<Bitmap>() {
                @Override
                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                    croppedImage = resource;
                    binding.cropImageView.setImageBitmap(resource);
                }

                @Override
                public void onLoadCleared(@Nullable Drawable placeholder) {
                }
            });
        } else {
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            croppedImage = BitmapFactory.decodeFile(imageFile.getAbsolutePath(), bmOptions);
            binding.cropImageView.setImageBitmap(croppedImage);
        }
        binding.fillView.setOnClickListener(v -> binding.cropImageView.resetCropRect());

        binding.next.setOnClickListener(v -> updateFile(binding.cropImageView.getCroppedImage()));

        binding.home.setOnClickListener(v -> finish());
    }
    @Override
    protected int getResId() {
        return R.layout.activity_cropping;
    }
    public void updateFile(Bitmap toBeUpdated) {
        binding.progressBar.setVisibility(View.VISIBLE);
        ObservableProvider.getSmartObservable((ObservableOnSubscribe<Boolean>) emitter -> {
            try (FileOutputStream out = new FileOutputStream(imageFile)) {
                toBeUpdated.compress(Bitmap.CompressFormat.PNG, 100, out);
            } catch (IOException e) {
                e.printStackTrace();
            }
            emitter.onComplete();
        }).subscribe(new BaseObserver<Boolean>() {
            @Override
            public void onError(@NotNull Throwable e) {
                super.onError(e);
            }

            @Override
            public void onComplete() {
                if (!saveOutput && businessCard == 4) {
                    EditingActivity.start(context, imageFile.getAbsolutePath(), saveOutput);
                    finish();
                } else {
                    if (businessCard == -1) {
                        binding.progressBar.setVisibility(View.GONE);
                        EditingActivity.start(context, imageFile.getAbsolutePath(), saveOutput);
                        finish();
                    } else if (businessCard == 2) {
                        binding.progressBar.setVisibility(View.GONE);
                        new BottomSheetForSaving(false).show(getSupportFragmentManager(), "exampleBottomSheet");
                    }
                }
            }
        });
    }
    @Override
    public void savePdf() throws IOException, DocumentException {
        pdfCreator();
    }
    @Override
    public void saveJpeg() {
        imageCreator();
    }
    private void imageCreator() {
        try {

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath(), options);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            File savedPhoto = new File(getFilesDir(), ScannerConstants.fileName + ".jpeg");
            ScannerConstants.uriForCurrent = getFilesDir() + "/" + ScannerConstants.fileName + ".jpeg";
            try {
                FileOutputStream outputStream = new FileOutputStream(savedPhoto.getPath());
                outputStream.write(byteArray);
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            binding.progressBar.setVisibility(View.GONE);
            startActivity(new Intent(CroppingActivity.this, SuccessActivity.class).putExtra("jpeg", 1));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public String getBatchDirectoryName() {
        File dir = new File(getFilesDir().getPath() + "/pdf");
        if (!dir.exists() && !dir.mkdirs()) {

        }
        return dir.getAbsolutePath();
    }
    private void pdfCreator() {
        try {
            ByteArrayOutputStream stream;
            Document document = new Document(PageSize.A4);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath(), options);
            stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            PdfWriter.getInstance(document, new FileOutputStream(new File(getBatchDirectoryName(), ScannerConstants.fileName + ".pdf")));
            document.open();
            ScannerConstants.uriForCurrent = getBatchDirectoryName() + "/" + ScannerConstants.fileName + ".pdf";
            Image image = Image.getInstance(byteArray);
            int indentation = 0;
            float scaler = ((document.getPageSize().getWidth() - document.leftMargin() - document.rightMargin() - indentation) / image.getWidth()) * 100;
            image.scalePercent(scaler);
            document.add(image);
            document.close();
            binding.progressBar.setVisibility(View.GONE);
            startActivity(new Intent(CroppingActivity.this, SuccessActivity.class));
            finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}