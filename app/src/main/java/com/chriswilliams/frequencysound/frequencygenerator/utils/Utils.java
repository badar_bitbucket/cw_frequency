package com.chriswilliams.frequencysound.frequencygenerator.utils;

import android.content.res.Resources;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.RelativeSizeSpan;
import android.text.style.SuperscriptSpan;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

public class Utils {
    public static float FormatNumber(String str) {
        try {
            return NumberFormat.getInstance(Locale.getDefault()).parse(str).floatValue();
        } catch (ParseException e) {
            e.printStackTrace();
            return 0.0f;
        }
    }
    public static SpannableStringBuilder setSpan(String str) {
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(str);
        spannableStringBuilder.setSpan(new RelativeSizeSpan(0.75f), 1, 2, 33);
        return spannableStringBuilder;
    }

    public static SpannableStringBuilder setSpan2(String str) {
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(str);
        spannableStringBuilder.setSpan(new RelativeSizeSpan(0.75f), 1, 2, 33);
        spannableStringBuilder.setSpan(new RelativeSizeSpan(0.75f), 2, 3, 33);
        return spannableStringBuilder;
    }

    public static SpannableStringBuilder setSpan3(String str) {
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(str);
        spannableStringBuilder.setSpan(new RelativeSizeSpan(0.75f), 1, 2, 18);
        spannableStringBuilder.setSpan(new SuperscriptSpan(), 2, 3, 18);
        spannableStringBuilder.setSpan(new RelativeSizeSpan(0.75f), 2, 3, 18);
        spannableStringBuilder.setSpan(new RelativeSizeSpan(0.75f), 5, 6, 33);
        spannableStringBuilder.setSpan(new SuperscriptSpan(), 6, 7, 33);
        spannableStringBuilder.setSpan(new RelativeSizeSpan(0.75f), 6, 7, 33);
        return spannableStringBuilder;
    }

    public static SpannableStringBuilder setSpan4(String str) {
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(str);
        spannableStringBuilder.setSpan(new RelativeSizeSpan(0.75f), 1, 2, 18);
        spannableStringBuilder.setSpan(new RelativeSizeSpan(0.75f), 2, 3, 18);
        spannableStringBuilder.setSpan(new SuperscriptSpan(), 3, 4, 18);
        spannableStringBuilder.setSpan(new RelativeSizeSpan(0.75f), 3, 4, 18);
        spannableStringBuilder.setSpan(new RelativeSizeSpan(0.75f), 6, 7, 33);
        spannableStringBuilder.setSpan(new RelativeSizeSpan(0.75f), 7, 8, 33);
        spannableStringBuilder.setSpan(new SuperscriptSpan(), 8, 9, 33);
        spannableStringBuilder.setSpan(new RelativeSizeSpan(0.75f), 8, 9, 33);
        return spannableStringBuilder;
    }
    public static float dpToPx(int dp) {
        return dpToPx((float) dp);
    }

    public static float dpToPx(float dp) {
        return (dp * Resources.getSystem().getDisplayMetrics().density);
    }

}
