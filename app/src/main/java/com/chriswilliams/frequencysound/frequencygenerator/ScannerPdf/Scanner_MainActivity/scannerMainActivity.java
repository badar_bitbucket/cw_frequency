package com.chriswilliams.frequencysound.frequencygenerator.ScannerPdf.Scanner_MainActivity;
import static com.lw.internalmarkiting.ads.InterstitialHelper.showAd;
import android.content.Intent;
import android.os.Bundle;
import com.chriswilliams.frequencysound.frequencygenerator.R;
import com.chriswilliams.frequencysound.frequencygenerator.ScannerPdf.ui.activities.othersActivity.CameraActivity;
import com.chriswilliams.frequencysound.frequencygenerator.ScannerPdf.ui.activities.othersActivity.Grabdata;
import com.chriswilliams.frequencysound.frequencygenerator.ScannerPdf.ui.activities.savefile_Doucment_Activity.SavefileMainactivity;
import com.chriswilliams.frequencysound.frequencygenerator.databinding.ActivityScannerMainActyivityBinding;
import com.lw.internalmarkiting.ui.activities.BaseActivity;
public class scannerMainActivity extends BaseActivity<ActivityScannerMainActyivityBinding> {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding.cvidCard.setOnClickListener(v -> {
            Intent intent = new Intent(scannerMainActivity.this, Grabdata.class);
            startActivity(intent);
        });
        binding.doucmentScannerId.setOnClickListener(v -> {
            Intent intent = new Intent(scannerMainActivity.this, CameraActivity.class);
            startActivity(intent);
        });
        binding.cvSaveFile.setOnClickListener(v -> showAd(() -> {
            Intent intent = new Intent(getApplicationContext(), SavefileMainactivity.class);
            startActivity(intent);
        }));
        binding.bback.setOnClickListener(v -> {
            finish();
        });
    }

    @Override
    protected void onReady() {

    }

    @Override
    protected int getResId() {
        return R.layout.activity_scanner_main_actyivity;
    }
}