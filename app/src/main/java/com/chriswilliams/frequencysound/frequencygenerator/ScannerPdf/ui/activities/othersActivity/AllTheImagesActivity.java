package com.chriswilliams.frequencysound.frequencygenerator.ScannerPdf.ui.activities.othersActivity;

import static com.chriswilliams.frequencysound.frequencygenerator.ScannerPdf.ui.activities.othersActivity.EditingActivity.EXTRA_URI_PATH;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.CompositePageTransformer;
import androidx.viewpager2.widget.MarginPageTransformer;

import com.chriswilliams.frequencysound.frequencygenerator.ScannerPdf.ui.bottomSaving_Activity.BottomSheetForSaving;
import com.chriswilliams.frequencysound.frequencygenerator.ScannerPdf.utils.ScannerConstants;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.lw.internalmarkiting.ui.activities.BaseActivity;
import com.lw.internalmarkiting.ui.view.GlideHelper;
import com.chriswilliams.frequencysound.frequencygenerator.R;
import com.chriswilliams.frequencysound.frequencygenerator.databinding.ActivityAllTheImagesBinding;

import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class AllTheImagesActivity extends BaseActivity<ActivityAllTheImagesBinding> implements BottomSheetForSaving.SavingInterface {
    private static final String EXTRA_BUSINESS_CARD = "businessCard";
    private static final String EXTRA_FILES_LIST = "files";
    ArrayList<String> files;
    ImageItemAdapter adapter;
    int index = -1;
    String fileLocation;

    public static void start(Context context, int businessCard, ArrayList<String> files) {
        Intent starter = new Intent(context, AllTheImagesActivity.class);
        starter.putExtra(EXTRA_FILES_LIST, files);
        starter.putExtra(EXTRA_BUSINESS_CARD, businessCard);
        context.startActivity(starter);
    }

    @Override
    protected int getResId() {
        return R.layout.activity_all_the_images;
    }

    @Override
    public void onReady() {
        files = getIntent().getStringArrayListExtra("list");
        if (files == null) {
            files = getIntent().getStringArrayListExtra(EXTRA_FILES_LIST);
        }
        adapter = new ImageItemAdapter(files);
        binding.viewPager.setAdapter(adapter);
        binding.viewPager.setClipToPadding(false);
        binding.viewPager.setClipChildren(false);
        binding.viewPager.setOffscreenPageLimit(3);
        binding.viewPager.getChildAt(0).setOverScrollMode(RecyclerView.OVER_SCROLL_NEVER);
        CompositePageTransformer compositePageTransformer = new CompositePageTransformer();
        compositePageTransformer.addTransformer(new MarginPageTransformer(40));
        compositePageTransformer.addTransformer((page, position) -> {
            float r = 1 - Math.abs(position);
            page.setScaleY(0.85f + r * 0.15f);
        });
        binding.viewPager.setPageTransformer(compositePageTransformer);
        binding.delete.setOnClickListener(v -> {

            File fdelete = new File(files.get(binding.viewPager.getCurrentItem()));
            if (fdelete.exists()) {
                if (fdelete.delete()) {
                    Toast.makeText(context, "Image deleted", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(context, "Couldn't Delete Image", Toast.LENGTH_LONG).show();
                }
            }
            adapter.notifier(binding.viewPager.getCurrentItem());
        });
        binding.edit.setOnClickListener(v -> {
            index = binding.viewPager.getCurrentItem();
            if (getIntent().getIntExtra(EXTRA_BUSINESS_CARD, -1) == 4) {
                CroppingActivity.start(context, files.get(index), false, 4);
            } else {
                CroppingActivity.start(context, files.get(index), false, false);
            }
        });


        binding.homeChecking.setOnClickListener(v -> {
            startActivity(new Intent(AllTheImagesActivity.this, MainActivity.class));
            finish();

        });

        binding.next.setOnClickListener(v ->
                new BottomSheetForSaving(true).show(getSupportFragmentManager(), "exampleBottomSheet"));

    }



    @Override
    protected void onResume() {
        super.onResume();
        if (index != -1) {
            adapter.notifyItemChanged(index);
        }
    }

    public void savingFile() {
        binding.progressBar.setVisibility(View.VISIBLE);
        Document document = new Document(PageSize.A4);
        String folderName;
        try {
            String filename = ScannerConstants.fileName + ".pdf";
            if (getIntent().getIntExtra(EXTRA_BUSINESS_CARD, -1) == 4)
                folderName = context.getString(R.string.directory_card);
            else {
                folderName = context.getString(R.string.directory_pdf);
            }
            fileLocation = getFilesDir() + File.separator + getString(R.string.directory) + File.separator + folderName + File.separator + filename;
            try {
                File file = new File(fileLocation);
                file.getParentFile().mkdirs();
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            PdfWriter.getInstance(document, new FileOutputStream(fileLocation));
        } catch (DocumentException | FileNotFoundException e) {
            e.printStackTrace();
        }
        document.open();

        if (getIntent().getIntExtra(EXTRA_BUSINESS_CARD, -1) == 4) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            Bitmap bitmap = BitmapFactory.decodeFile(files.get(0), options);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            Image image = null;
            try {
                image = Image.getInstance(byteArray);
            } catch (BadElementException | IOException e) {
                e.printStackTrace();
            }
            PdfPTable main = new PdfPTable(1);
            PdfPCell cell1, cell2, cell3;
            cell1 = new PdfPCell();
            cell1.setBorder(Rectangle.NO_BORDER);
            cell1.addElement(image);
            main.addCell(cell1);
            options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            bitmap = BitmapFactory.decodeFile(files.get(1), options);
            stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byteArray = stream.toByteArray();
            try {
                image = Image.getInstance(byteArray);
            } catch (BadElementException | IOException e) {
                e.printStackTrace();
            }

            cell3 = new PdfPCell();
            cell3.setBorder(Rectangle.NO_BORDER);
            cell3.addElement(image);

            main.addCell(cell3);
            try {
                document.add(main);
            } catch (DocumentException e) {
                e.printStackTrace();
            }

        } else {
            for (int i = 0; i < files.size(); i++) {
                try {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                    Bitmap bitmap = BitmapFactory.decodeFile(files.get(i), options);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    byte[] byteArray = stream.toByteArray();
                    Image image = Image.getInstance(byteArray);
                    image.setAbsolutePosition(0, 0);
                    image.scaleAbsolute(595, 842);
                    document.newPage();
                    document.add(image);
                } catch (DocumentException | IOException e) {
                    e.printStackTrace();
                }
            }
        }
        document.close();
        binding.progressBar.setVisibility(View.GONE);
        startActivity(new Intent(AllTheImagesActivity.this, SuccessActivity.class)
                .putExtra("jpeg", 1)
                .putExtra(EXTRA_URI_PATH, fileLocation));
//        finish();
    }

    @Override
    public void savePdf() {
        binding.progressBar.setVisibility(View.VISIBLE);
        savingFile();

    }

    @Override
    public void saveJpeg() {
    }

    public class ImageItemAdapter extends RecyclerView.Adapter<ImageItemAdapter.ViewHolder> {
        private final ArrayList<String> data;

        public ImageItemAdapter(ArrayList<String> data) {
            this.data = data;
        }

        public void notifier(int pos) {
            data.remove(pos);
            notifyItemRemoved(pos);
        }

        @NotNull
        @Override
        public ImageItemAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View rowItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_item_rv, parent, false);
            return new ViewHolder(rowItem);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            if (index != -1) {
                index = -1;
                GlideHelper.loadImgWithout(context, data.get(position), holder.image);
            } else
                GlideHelper.loadImg(context, data.get(position), holder.image);

        }

        @Override
        public int getItemCount() {
            return this.data.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            private ImageView image;

            public ViewHolder(View view) {
                super(view);
                view.setOnClickListener(this);
                this.image = view.findViewById(R.id.imageItem);
            }

            @Override
            public void onClick(View view) {
            }
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}