package com.chriswilliams.frequencysound.frequencygenerator;

import com.chriswilliams.frequencysound.frequencygenerator.prefernces.MyPreferences;
import com.lw.internalmarkiting.AdsApp;

public class App extends AdsApp {
    @Override
    public void onCreate() {
        super.onCreate();
        MyPreferences.init(this);
    }

    @Override
    protected boolean enableAppOpenAd() {
        return true;
    }
}