package com.chriswilliams.frequencysound.frequencygenerator.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;
import android.text.method.DigitsKeyListener;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.chriswilliams.frequencysound.frequencygenerator.R;
import com.chriswilliams.frequencysound.frequencygenerator.prefernces.MyPreferences;
import com.chriswilliams.frequencysound.frequencygenerator.utils.DecimalDigitInputFilter;
import com.chriswilliams.frequencysound.frequencygenerator.utils.EditTextInputChecker;
import com.chriswilliams.frequencysound.frequencygenerator.utils.Utils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class SetRangeDialog extends DialogFragment {

    private View view;
    private EditText range_editText_from;
    private EditText range_editText_to;
    private TextView pref_set_range_summary;
    private DecimalFormat decimalFormat = new DecimalFormat();

    @Override
    public void onStart() {
        super.onStart();
        try {
            requireDialog().getWindow().setBackgroundDrawableResource(R.drawable.seekbar_bg);
            double d = getResources().getDisplayMetrics().widthPixels;
            requireDialog().getWindow().setLayout((int) (d * 0.9d), -2);
        } catch (Exception unused) {
            view.setBackground(null);
            view.setBackgroundColor(requireActivity().getResources().getColor(R.color.black));
        }
    }

    private void save() {
        EditTextInputChecker inputChecker = new EditTextInputChecker();
        inputChecker.checker(range_editText_from.getText().toString(), range_editText_to.getText().toString(), 1);
        EditTextInputChecker inputChecker1 = new EditTextInputChecker();
        inputChecker1.checker(range_editText_to.getText().toString(), range_editText_from.getText().toString(), 2);
        if (!inputChecker.isValidInput || !inputChecker1.isValidInput) {
            if (!inputChecker.isValidInput) {
                range_editText_from.setError(inputChecker.warning);
            }
            if (!inputChecker1.isValidInput) {
                range_editText_to.setError(inputChecker1.warning);
            }
            return;
        }
        float minFreq = Utils.FormatNumber(range_editText_from.getText().toString());
        float maxFreq = Utils.FormatNumber(range_editText_to.getText().toString());
        MyPreferences.setMinFrequency(minFreq);
        MyPreferences.setMaxFrequency(maxFreq);
        String sb = decimalFormat.format(minFreq) + " - " + decimalFormat.format(maxFreq) + " Hz";
        if (pref_set_range_summary != null)
            pref_set_range_summary.setText(sb);
        requireDialog().cancel();
    }

    private void cancel() {
        requireDialog().cancel();
    }

    private void reset() {
        range_editText_from.setText(String.valueOf(1));
        range_editText_to.setText(String.valueOf(22000));
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Dialog dialog = new Dialog(requireActivity());
        dialog.requestWindowFeature(1);
        view = requireActivity().getLayoutInflater().inflate(R.layout.dialog_range, null);
        dialog.setContentView(view);
        range_editText_from = view.findViewById(R.id.pref_range_editText_from);
        range_editText_to = view.findViewById(R.id.pref_range_editText_to);
        TextView button_set_range_OK = view.findViewById(R.id.button_set_range_OK);
        TextView button_set_range_CANCEL = view.findViewById(R.id.button_set_range_CANCEL);
        TextView button_set_range_RESET = view.findViewById(R.id.button_set_range_RESET);
        pref_set_range_summary = requireActivity().findViewById(R.id.pref_set_range_summary);
        range_editText_from.setFilters(new InputFilter[]{new DecimalDigitInputFilter(5, 2)});
        range_editText_to.setFilters(new InputFilter[]{new DecimalDigitInputFilter(5, 2)});
        decimalFormat.setGroupingUsed(false);
        if (MyPreferences.getIsDecimalPrecision()) {
            decimalFormat.setMaximumFractionDigits(2);
            range_editText_from.setText(decimalFormat.format(MyPreferences.getMinFrequency()));
            range_editText_to.setText(decimalFormat.format(MyPreferences.getMaxFrequency()));
            range_editText_from.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);
            range_editText_to.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);
            char decimalSeparator = DecimalFormatSymbols.getInstance().getDecimalSeparator();
            String str = "0123456789";
            String sb = str + decimalSeparator;
            range_editText_from.setKeyListener(DigitsKeyListener.getInstance(sb));
            String sb2 = str + decimalSeparator;
            range_editText_to.setKeyListener(DigitsKeyListener.getInstance(sb2));
        } else {
            decimalFormat.setMaximumFractionDigits(0);
            range_editText_from.setText(decimalFormat.format(MyPreferences.getMinFrequency()));
            range_editText_to.setText(decimalFormat.format(MyPreferences.getMaxFrequency()));
        }
        button_set_range_OK.setOnClickListener(view1 -> save());
        button_set_range_CANCEL.setOnClickListener(view1 -> cancel());
        button_set_range_RESET.setOnClickListener(view1 -> reset());
        return dialog;
    }
}