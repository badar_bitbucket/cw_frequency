
package com.chriswilliams.frequencysound.frequencygenerator.ScannerPdf.utils;
import android.graphics.Bitmap;

public class ScannerConstants {
    public static Bitmap selectedImageBitmap;
    public static String fileName = null;
    public static String uriForCurrent = null;
    public static String cropText = "KIRP", backText = "KAPAT",
            imageError = " Image did not load.",
            cropError = "did not crop.";

}
