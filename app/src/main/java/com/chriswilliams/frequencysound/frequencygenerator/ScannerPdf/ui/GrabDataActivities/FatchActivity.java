package com.chriswilliams.frequencysound.frequencygenerator.ScannerPdf.ui.GrabDataActivities;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.chriswilliams.frequencysound.frequencygenerator.R;
public class FatchActivity extends AppCompatActivity {
    EditText Email;
    EditText number;
    EditText name;
    Data data;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fatch);
        handling();
    }
    public void email(View view) {
        if (Email.getText().toString().equals("")) {
            Toast.makeText(FatchActivity.this, "Email not Found!", Toast.LENGTH_SHORT).show();

        } else {
            Intent it = new Intent(Intent.ACTION_SEND);
            it.putExtra(Intent.EXTRA_EMAIL, new String[]{Email.getText().toString()});
            it.setType("message/rfc822");
            startActivity(Intent.createChooser(it, "Choose Mail App"));
        }
    }
    public void contact(View view) {
        if (Email.getText().toString().equals("") && name.getText().toString().equals("") && number.getText().toString().equals("")) {
            Toast.makeText(FatchActivity.this, "Data Not Found!", Toast.LENGTH_SHORT).show();
        } else {
            Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
            intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
            intent.putExtra(ContactsContract.Intents.Insert.PHONE, data.getNumber());
            intent.putExtra(ContactsContract.Intents.Insert.EMAIL, data.getEmail());
            intent.putExtra(ContactsContract.Intents.Insert.NAME, data.getName());
            startActivity(intent);
        }
    }
    public void call(View view) {

            if (number.getText().toString().equals("")) {
                Toast.makeText(FatchActivity.this, "Number Not Found!", Toast.LENGTH_SHORT).show();
            } else {
                Intent i = new Intent(Intent.ACTION_DIAL);
                i.setData(Uri.parse("tel:" + number.getText().toString()));
                startActivity(i);
            }
    }
    public void handling()
    {
        Email = findViewById(R.id.Email_id);
        name = findViewById(R.id.Name_id);
        number = findViewById(R.id.Number_id);
        data = getIntent().getParcelableExtra("data");
        Email.setText(data.getEmail());
        name.setText(data.getName());
        number.setText(data.getNumber());
    }

    public void back(View view) { finish(); }

    @Override
    public void onBackPressed() {
        finish();
    }

}