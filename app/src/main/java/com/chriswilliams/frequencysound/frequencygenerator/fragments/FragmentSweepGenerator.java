package com.chriswilliams.frequencysound.frequencygenerator.fragments;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.app.Dialog;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.InputFilter;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.chriswilliams.frequencysound.frequencygenerator.R;
import com.chriswilliams.frequencysound.frequencygenerator.audio.Audio;
import com.chriswilliams.frequencysound.frequencygenerator.databinding.FragmentSweepGeneratorBinding;
import com.chriswilliams.frequencysound.frequencygenerator.prefernces.MyPreferences;
import com.chriswilliams.frequencysound.frequencygenerator.utils.DecimalDigitInputFilter;
import com.chriswilliams.frequencysound.frequencygenerator.utils.EditTextInputChecker;
import com.chriswilliams.frequencysound.frequencygenerator.utils.Utils;
import com.lw.internalmarkiting.ui.base.BindingFragment;

import java.text.DecimalFormat;
import java.util.Locale;

import static com.chriswilliams.frequencysound.frequencygenerator.utils.Constants.MAX_VOLUME;
import static com.chriswilliams.frequencysound.frequencygenerator.utils.Constants.SAWTOOTH;
import static com.chriswilliams.frequencysound.frequencygenerator.utils.Constants.SINE;
import static com.chriswilliams.frequencysound.frequencygenerator.utils.Constants.SQUARE;

public class FragmentSweepGenerator extends BindingFragment<FragmentSweepGeneratorBinding> implements CompoundButton.OnCheckedChangeListener {
    Audio audio;
    String lastWaveForm;
    ValueAnimator animator;
    private boolean isPlaying = true;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_sweep_generator;
    }

    @Override
    protected void onReady() {
        audio = new Audio();
        audio.frequency = MyPreferences.getLastFrequency();
        audio.level = MyPreferences.getLastVolume() / (float) MAX_VOLUME;
        initWaveForm();
        String sFreq = String.format(Locale.getDefault(), "%1.0f", MyPreferences.getStartFrequency()) + " Hz";
        binding.startFreq.setText(sFreq);
        sFreq = String.format(Locale.getDefault(), "%1.0f", MyPreferences.getEndFrequency()) + " Hz";
        binding.endFreq.setText(sFreq);
        String duration = String.format(Locale.getDefault(), "%1.0f", (float) MyPreferences.getDuration()) + " sec";
        binding.duration.setText(duration);
        setOnclickListeners();
        binding.repeatSwitch.setChecked(MyPreferences.getSweepRepeat());
        binding.repeatSwitch.setOnCheckedChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        binding.playStop.setImageResource(R.drawable.ic_play);
        audio.stop();
        isPlaying = true;
    }

    @Override
    public void onResume() {
        super.onResume();
        requireActivity().setTitle("Sweep Generator");
    }

    private void setOnclickListeners() {
        binding.wave.setOnClickListener(view -> {
            switch (MyPreferences.getLastWaveForm()) {
                case SINE:
                    binding.wave.setImageDrawable(getResources().getDrawable(R.drawable.ic_square_wave));
                    audio.waveform = Audio.SQUARE;
                    MyPreferences.setLastWaveForm(SQUARE);
                    break;
                case SQUARE:
                    binding.wave.setImageDrawable(getResources().getDrawable(R.drawable.ic_sawtooth_wave));
                    audio.waveform = Audio.SAWTOOTH;
                    MyPreferences.setLastWaveForm(SAWTOOTH);
                    break;
                case SAWTOOTH:
                    binding.wave.setImageDrawable(getResources().getDrawable(R.drawable.ic_sine_wave));
                    audio.waveform = Audio.SINE;
                    MyPreferences.setLastWaveForm(SINE);
                    break;
            }
        });
        binding.playStop.setOnClickListener(view -> {
            if (!isPlaying) {
                binding.playStop.setImageResource(R.drawable.ic_play);
                audio.stop();
                if (animator != null)
                    animator.end();
                isPlaying = true;
            } else {
                binding.playStop.setImageResource(R.drawable.ic_stop_black_24dp);
                startSweep(MyPreferences.getStartFrequency(), MyPreferences.getEndFrequency(), MyPreferences.getDuration());
                audio.start();
                isPlaying = false;
            }
        });
        binding.durationContainer.setOnClickListener(view -> showDurationDialog());
        binding.startFreqContainer.setOnClickListener(view -> showDurationDialog());
        binding.endFreqContainer.setOnClickListener(view -> showDurationDialog());
        binding.volumeButton.setOnClickListener(view -> showVolumeDialog());
    }

    private void showVolumeDialog() {
        Dialog audioDialog = new Dialog(requireContext());
        audioDialog.setContentView(R.layout.dialog_audio);

        ConstraintLayout mainLayout = audioDialog.findViewById(R.id.mainLayout);
        TextView volume = audioDialog.findViewById(R.id.volumeTxt);
        SeekBar volumeBar = audioDialog.findViewById(R.id.volumeBar);
        TextView btnOk = audioDialog.findViewById(R.id.btnOK);

        volumeBar.setMax(MAX_VOLUME);
        String vol = String.format(Locale.getDefault(), "%1.0f", (float) MyPreferences.getLastVolume()) + "%";
        volume.setText(vol);
        volumeBar.setProgress(MyPreferences.getLastVolume());
        volumeBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                audio.level = progress / (double) MAX_VOLUME;
                String vol = String.format(Locale.getDefault(), "%1.0f", (float) progress) + "%";
                MyPreferences.setLastVolume(progress);
                volume.setText(vol);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        volumeBar.setProgress(MyPreferences.getLastVolume());

        btnOk.setOnClickListener(v -> audioDialog.dismiss());
        audioDialog.setCancelable(false);

        DisplayMetrics displayMetrics = Resources.getSystem().getDisplayMetrics();
        int widthLcl = (int) (displayMetrics.widthPixels * 0.9f);
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) mainLayout.getLayoutParams();
        params.width = widthLcl;
        params.gravity = Gravity.CENTER;
        audioDialog.show();
        mainLayout.setLayoutParams(params);
        Window window = audioDialog.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    private void startSweep(float startFrequency, float endFrequency, long duration) {
        animator = ValueAnimator.ofFloat(startFrequency, endFrequency);
        animator.setDuration(duration * 1000);
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                binding.startFreqContainer.setEnabled(false);
                binding.endFreqContainer.setEnabled(false);
                binding.durationContainer.setEnabled(false);
                binding.repeatSwitch.setEnabled(false);
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                binding.startFreqContainer.setEnabled(true);
                binding.endFreqContainer.setEnabled(true);
                binding.durationContainer.setEnabled(true);
                binding.repeatSwitch.setEnabled(true);
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        animator.addUpdateListener(animation -> {
            audio.frequency = (float) animation.getAnimatedValue();
            String sFreq = String.format(Locale.getDefault(), "%1.0f", animation.getAnimatedValue()) + " Hz";
            SpannableString spannableString = new SpannableString(sFreq);
            spannableString.setSpan(new RelativeSizeSpan(1f),
                    0, sFreq.lastIndexOf("H"), 0);
            spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, sFreq.lastIndexOf("H"), 0);
            binding.currentFrequency.setText(spannableString);
        });
        if (MyPreferences.getSweepRepeat())
            animator.setRepeatCount(ValueAnimator.INFINITE);
        else animator.setRepeatCount(0);
        animator.start();
    }

    private void showDurationDialog() {
        Dialog dialog = new Dialog(requireContext());
        dialog.setContentView(R.layout.dialog_duration);
        DecimalFormat decimalFormat = new DecimalFormat();
        String sFrequency;
        ConstraintLayout mainLayout = dialog.findViewById(R.id.mainLayout);
        AppCompatRadioButton radioButtonDuration_10_s = dialog.findViewById(R.id.radioButton_duration_10_s);
        AppCompatRadioButton radioButton_duration_20_s = dialog.findViewById(R.id.radioButton_duration_20_s);
        AppCompatRadioButton radioButton_duration_30_s = dialog.findViewById(R.id.radioButton_duration_30_s);
        AppCompatRadioButton radioButtonDurationCustom = dialog.findViewById(R.id.radioButton_duration_custom);
        EditText startRangeEditText = dialog.findViewById(R.id.startRangeEditText);
        EditText endRangeEditText = dialog.findViewById(R.id.endRangeEditText);
        EditText editTextCustomDuration = dialog.findViewById(R.id.edittext_custom_duration);

        TextView buttonOK = dialog.findViewById(R.id.buttonOK);
        TextView buttonCancel = dialog.findViewById(R.id.buttonCancel);

        editTextCustomDuration.setFilters(new InputFilter[]{new DecimalDigitInputFilter(5, 2)});
        startRangeEditText.setFilters(new InputFilter[]{new DecimalDigitInputFilter(5, 2)});
        endRangeEditText.setFilters(new InputFilter[]{new DecimalDigitInputFilter(5, 2)});

        sFrequency = String.format(Locale.getDefault(), "%1.0f", MyPreferences.getStartFrequency());
        startRangeEditText.setText(sFrequency);
        sFrequency = String.format(Locale.getDefault(), "%1.0f", MyPreferences.getEndFrequency());
        endRangeEditText.setText(sFrequency);

        float k = MyPreferences.getDuration();
        if (k == 10) {
            radioButtonDuration_10_s.setChecked(true);
        } else if (k == 20) {
            radioButton_duration_20_s.setChecked(true);
        } else if (k == 30) {
            radioButton_duration_30_s.setChecked(true);
        } else {
            radioButtonDurationCustom.setChecked(true);
            editTextCustomDuration.setText(decimalFormat.format(MyPreferences.getDuration()));
            editTextCustomDuration.setVisibility(View.VISIBLE);
        }
        radioButtonDurationCustom.setOnClickListener(view1 -> {
            if (editTextCustomDuration.getVisibility() == View.GONE) {
                editTextCustomDuration.setVisibility(View.VISIBLE);
            }
        });
        radioButtonDuration_10_s.setOnClickListener(view1 -> {
            if (editTextCustomDuration.getVisibility() == View.VISIBLE) {
                editTextCustomDuration.setVisibility(View.GONE);
            }
        });
        radioButton_duration_20_s.setOnClickListener(view1 -> {
            if (editTextCustomDuration.getVisibility() == View.VISIBLE) {
                editTextCustomDuration.setVisibility(View.GONE);
            }
        });
        radioButton_duration_30_s.setOnClickListener(view1 -> {

            if (editTextCustomDuration.getVisibility() == View.VISIBLE) {
                editTextCustomDuration.setVisibility(View.GONE);
            }
        });
        buttonOK.setOnClickListener(view2 -> {
            if (radioButtonDuration_10_s.isChecked()) {
                MyPreferences.setDuration(10);
            } else if (radioButton_duration_20_s.isChecked()) {
                MyPreferences.setDuration(20);
            } else if (radioButton_duration_30_s.isChecked()) {
                MyPreferences.setDuration(30);
            } else if (radioButtonDurationCustom.isChecked()) {
                EditTextInputChecker checker = new EditTextInputChecker();
                checker.inputChecker(editTextCustomDuration.getText().toString());
                if (!checker.isValidInput) {
                    editTextCustomDuration.setError(checker.warning);
                    return;
                }
                MyPreferences.setDuration((long) Utils.FormatNumber(editTextCustomDuration.getText().toString()));
            }
            String duration = String.format(Locale.getDefault(), "%1.0f", (float) MyPreferences.getDuration()) + " sec";
            binding.duration.setText(duration);

            EditTextInputChecker inputChecker = new EditTextInputChecker();
            inputChecker.checker(startRangeEditText.getText().toString(), endRangeEditText.getText().toString(), 1);
            EditTextInputChecker inputChecker1 = new EditTextInputChecker();
            inputChecker1.checker(endRangeEditText.getText().toString(), startRangeEditText.getText().toString(), 2);
            if (!inputChecker.isValidInput || !inputChecker1.isValidInput) {
                if (!inputChecker.isValidInput) {
                    startRangeEditText.setError(inputChecker.warning);
                }
                if (!inputChecker1.isValidInput) {
                    endRangeEditText.setError(inputChecker1.warning);
                }
                return;
            }
            MyPreferences.setStartFrequency((long) Utils.FormatNumber(startRangeEditText.getText().toString()));
            MyPreferences.setEndFrequency((long) Utils.FormatNumber(endRangeEditText.getText().toString()));

            String sFreq = String.format(Locale.getDefault(), "%1.0f", MyPreferences.getStartFrequency()) + " Hz";
            binding.startFreq.setText(sFreq);
            sFreq = String.format(Locale.getDefault(), "%1.0f", MyPreferences.getEndFrequency()) + " Hz";
            binding.endFreq.setText(sFreq);
            dialog.dismiss();
        });
        buttonCancel.setOnClickListener(view1 -> dialog.dismiss());

        dialog.setCancelable(false);
        DisplayMetrics displayMetrics = Resources.getSystem().getDisplayMetrics();
        int widthLcl = (int) (displayMetrics.widthPixels * 0.9f);
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) mainLayout.getLayoutParams();
        params.width = widthLcl;
        params.gravity = Gravity.CENTER;
        dialog.show();
        mainLayout.setLayoutParams(params);
        Window window = dialog.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    private void initWaveForm() {
        lastWaveForm = MyPreferences.getLastWaveForm();
        switch (lastWaveForm) {
            case SINE:
                binding.wave.setImageDrawable(getResources().getDrawable(R.drawable.ic_sine_wave));
                audio.waveform = Audio.SINE;
                break;
            case SQUARE:
                binding.wave.setImageDrawable(getResources().getDrawable(R.drawable.ic_square_wave));
                audio.waveform = Audio.SQUARE;
                break;
            case SAWTOOTH:
                binding.wave.setImageDrawable(getResources().getDrawable(R.drawable.ic_sawtooth_wave));
                audio.waveform = Audio.SAWTOOTH;
                break;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if (compoundButton.getId() == R.id.repeatSwitch) {
            compoundButton.setChecked(b);
            MyPreferences.setSweepRepeat(b);
        }
    }
}