package com.chriswilliams.frequencysound.frequencygenerator.utils;

public class EditTextInputChecker {
    public boolean isValidInput;
    public String warning;

    public EditTextInputChecker() {
    }

    public EditTextInputChecker checker(String str, String str2, int i) {
        if (str.isEmpty()) {
            isValidInput = false;
            warning = "Field is empty.";
            return this;
        }
        float a = Utils.FormatNumber(str);
        if (a < 1.0f) {
            isValidInput = false;
            warning = "Value must be greater than 1";
            return this;
        } else if (a > 22000.0f) {
            isValidInput = false;
            warning = "Value must be less than 22000";
            return this;
        } else if (i == 1 && !str2.isEmpty() && a > Utils.FormatNumber(str2)) {
            isValidInput = false;
            warning = "'From' value must be less than 'To' value";
            return this;
        } else if (i != 2 || str2.isEmpty() || a >= Utils.FormatNumber(str2)) {
            isValidInput = true;
            warning = "";
            return this;
        } else {
            isValidInput = false;
            warning = "'To' value must be greater than 'From' value";
            return this;
        }
    }

    public EditTextInputChecker inputChecker(String str) {
        if (str.isEmpty()) {
            isValidInput = false;
            warning = "Field is empty.";
            return this;
        }
        float a = Utils.FormatNumber(str);
        if (a <= 0.0f) {
            isValidInput = false;
            warning = "Value must be greater than 0.";
            return this;
        } else if (a > 22000.0f) {
            isValidInput = false;
            warning = "Value must be less than 22000";
            return this;
        } else {
            isValidInput = true;
            warning = "";
            return this;
        }
    }

}