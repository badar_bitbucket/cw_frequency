package com.chriswilliams.frequencysound.frequencygenerator.ScannerPdf.ui.bottomSaving_Activity;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.chriswilliams.frequencysound.frequencygenerator.ScannerPdf.utils.ScannerConstants;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.radiobutton.MaterialRadioButton;
import com.itextpdf.text.DocumentException;
import com.chriswilliams.frequencysound.frequencygenerator.R;

import java.io.IOException;
import java.util.Calendar;

public class BottomSheetForSaving extends BottomSheetDialogFragment {
    SavingInterface savingInterface;
    String extraName;
    boolean multiple;

    public BottomSheetForSaving(boolean multiple) {
        this.multiple = multiple;
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            savingInterface = (SavingInterface) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement LogoutUser");
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        extraName = Calendar.getInstance().getTimeInMillis() + "";
        extraName = extraName.substring(7, 12);
        View view = inflater.inflate(R.layout.bottom_sheet_for_saving, container, false);
        EditText fileName = view.findViewById(R.id.fileName);

        ImageView editFileName = view.findViewById(R.id.editFileName);
        editFileName.setOnClickListener(v -> {
                fileName.setFocusable(true);
                fileName.setFocusableInTouchMode(true);
                fileName.setClickable(true);
                fileName.setCursorVisible(true);
                fileName.requestFocus();
                InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.showSoftInput(fileName, InputMethodManager.SHOW_IMPLICIT);
                fileName.setText("");

        });
        MaterialButton cancel = view.findViewById(R.id.cancel);
        cancel.setOnClickListener(v -> this.dismiss());
        MaterialRadioButton pdfCheck = view.findViewById(R.id.pdfCheck);
        MaterialRadioButton jpegCheck = view.findViewById(R.id.jpegCheck);
        TextView save = view.findViewById(R.id.save);
        if (multiple) {
            jpegCheck.setClickable(false);

        }


        jpegCheck.setOnCheckedChangeListener((buttonView, isChecked) -> {
            String nameToSet;
            if (isChecked) {
                nameToSet = "JPEG_" + extraName;
            } else {
                nameToSet = "PDF_" + extraName;
            }


            fileName.setText(nameToSet);
        });


        save.setOnClickListener(v -> {
            if(fileName.getText().toString().isEmpty())
            {
                Toast.makeText(save.getContext(), "Please Enter File Name!", Toast.LENGTH_SHORT).show();
            }
            else
            {
                this.dismiss();

                ScannerConstants.fileName = (fileName.getText().toString());

                if (pdfCheck.isChecked()) {
                    try {
                        savingInterface.savePdf();
                    } catch (IOException | DocumentException e) {
                        e.printStackTrace();
                    }

                } else if (jpegCheck.isChecked()) {
                    savingInterface.saveJpeg();

                }
            }
            

        });

        return view;


    }
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme);
    }
    public interface SavingInterface {
        void savePdf() throws IOException, DocumentException;

        void saveJpeg();

    }
}
