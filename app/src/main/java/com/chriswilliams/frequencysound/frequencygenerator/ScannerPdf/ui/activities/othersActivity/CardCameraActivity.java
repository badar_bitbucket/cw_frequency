package com.chriswilliams.frequencysound.frequencygenerator.ScannerPdf.ui.activities.othersActivity;
import static com.chriswilliams.frequencysound.frequencygenerator.ScannerPdf.utils.FileSaveHelper.storagePermission;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.camera.core.Camera;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageCaptureException;
import androidx.camera.core.Preview;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.camera.view.PreviewView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.LifecycleOwner;
import com.chriswilliams.frequencysound.frequencygenerator.ScannerPdf.utils.ScannerConstants;
import com.google.common.util.concurrent.ListenableFuture;
import com.chriswilliams.frequencysound.frequencygenerator.R;
import com.chriswilliams.frequencysound.frequencygenerator.databinding.ActivityCardCameraBinding;
import com.lw.internalmarkiting.ui.activities.BaseActivity;
import com.zxy.tiny.Tiny;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class CardCameraActivity extends BaseActivity<ActivityCardCameraBinding> {
    private static final String EXTRA_SOURCE = "source";
    private final String[] REQUIRED_PERMISSIONS = new String[]{"android.permission.CAMERA", storagePermission};
    private final Executor executor = Executors.newSingleThreadExecutor();
    private final int REQUEST_CODE_PERMISSIONS = 1001;
    PreviewView mPreviewView;
    ImageView captureImage;
    ArrayList<String> files;

    @Override
    protected int getResId() {
        return R.layout.activity_card_camera;
    }

    @Override
    public void onReady() {
        mPreviewView = binding.camera;
        captureImage = binding.capture;
        files = new ArrayList<>();

        binding.home.setOnClickListener(v -> {
            onBackPressed();
        });
        binding.next.setOnClickListener(v -> {
            Intent i = new Intent(context, AllTheImagesActivity.class);
            i.putStringArrayListExtra("list", files);
            startActivity(i);
        });

        if (allPermissionsGranted()) {
            startCamera(); //start camera if permission has been granted by user
        } else {
            ActivityCompat.requestPermissions(this, REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS);
        }

        if (getIntent().getIntExtra(EXTRA_SOURCE, -1) == 4) {

            new Handler(Looper.myLooper()).postDelayed(() -> binding.frontMessage.setVisibility(View.GONE), 2000);
        } else {
            binding.frontMessage.setVisibility(View.GONE);
        }

    }

    private void startCamera() {

        final ListenableFuture<ProcessCameraProvider> cameraProviderFuture = ProcessCameraProvider.getInstance(this);
        cameraProviderFuture.addListener(() -> {
            try {

                ProcessCameraProvider cameraProvider = cameraProviderFuture.get();
                bindPreview(cameraProvider);

            } catch (ExecutionException | InterruptedException e) {
            }
        }, ContextCompat.getMainExecutor(this));
    }

    void bindPreview(@NonNull ProcessCameraProvider cameraProvider) {

        Preview preview = new Preview.Builder()
                .build();

        CameraSelector cameraSelector = new CameraSelector.Builder()
                .requireLensFacing(CameraSelector.LENS_FACING_BACK)
                .build();

        ImageAnalysis imageAnalysis = new ImageAnalysis.Builder()
                .build();

        ImageCapture.Builder builder = new ImageCapture.Builder().setFlashMode(ImageCapture.FLASH_MODE_AUTO);


        final ImageCapture imageCapture = builder
                .setTargetRotation(this.getWindowManager().getDefaultDisplay().getRotation())
                .build();

        preview.setSurfaceProvider(mPreviewView.getSurfaceProvider());

        Camera camera = cameraProvider.bindToLifecycle((LifecycleOwner) this, cameraSelector, preview, imageAnalysis, imageCapture);

        captureImage.setOnClickListener(v -> {
            if (files.size() == 2) {
                binding.capture.setClickable(false);
            }
            ScannerConstants.selectedImageBitmap = null;
            SimpleDateFormat mDateFormat = new SimpleDateFormat("yyyyMMddHHmmss", Locale.US);
            File file = new File(getBatchDirectoryName(), mDateFormat.format(new Date()) + ".jpg");
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            ImageCapture.OutputFileOptions outputFileOptions = new ImageCapture.OutputFileOptions.Builder(file).build();
            imageCapture.takePicture(outputFileOptions, executor, new ImageCapture.OnImageSavedCallback() {
                @Override
                public void onImageSaved(@NonNull ImageCapture.OutputFileResults outputFileResults) {
                    new Handler(Looper.getMainLooper()).post(() -> {

                        Tiny.FileCompressOptions options = new Tiny.FileCompressOptions();
                        Tiny.getInstance().source(file.getAbsolutePath()).asFile().withOptions(options).compress((isSuccess, outfile) -> {


                            files.add(outfile);
                            if (files.size() == 1) {
                                binding.capture.setClickable(true);
                                binding.backMessage.setVisibility(View.VISIBLE);
                                new Handler(Looper.myLooper()).postDelayed(() -> binding.backMessage.setVisibility(View.GONE), 2000);
                            } else if (files.size() == 2) {
                                AllTheImagesActivity.start(context, 4, files);
                                files.clear();
                            }


                        });

                    });


                }

                @Override
                public void onError(@NonNull ImageCaptureException error) {
                    error.printStackTrace();
                }
            });
        });
    }

    public String getBatchDirectoryName() {

        String app_folder_path;
        app_folder_path = getFilesDir().getPath() + "/images";
        File dir = new File(app_folder_path);
        if (!dir.exists() && !dir.mkdirs()) {

        }
        return app_folder_path;
    }

    private boolean allPermissionsGranted() {

        for (String permission : REQUIRED_PERMISSIONS) {
            if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    @Override
    protected void onResume() {
        binding.frontMessage.setVisibility(View.VISIBLE);
        new Handler(Looper.myLooper()).postDelayed(() -> binding.frontMessage.setVisibility(View.GONE), 2000);
        super.onResume();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_PERMISSIONS) {
            if (allPermissionsGranted()) {
                startCamera();
            } else {
                Toast.makeText(this, "Permissions not granted by the user.", Toast.LENGTH_SHORT).show();
                this.finish();
            }
        }
    }

}