package com.chriswilliams.frequencysound.frequencygenerator.models;

import android.text.SpannableStringBuilder;

public final class MusicalNotes {

    private final double frequency;
    private final SpannableStringBuilder note;

    public MusicalNotes(SpannableStringBuilder note, double frequency) {
        this.frequency = frequency;
        this.note = note;
    }

    public final double getFrequency() {
        return this.frequency;
    }

    public final SpannableStringBuilder getNote() {
        return this.note;
    }
}
