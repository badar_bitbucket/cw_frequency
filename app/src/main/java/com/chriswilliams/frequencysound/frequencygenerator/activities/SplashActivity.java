package com.chriswilliams.frequencysound.frequencygenerator.activities;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import com.chriswilliams.frequencysound.frequencygenerator.R;
import com.lw.internalmarkiting.ads.InterstitialHelper;
public class SplashActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int SPLASH_DISPLAY_LENGTH = 4000;
        setContentView(R.layout.activity_splash);
        new Handler().postDelayed(() -> InterstitialHelper.showAd(() -> {
            Intent mainIntent = new Intent(SplashActivity.this, MainActivityNew.class);
            startActivity(mainIntent);
            SplashActivity.this.finish();
        }), SPLASH_DISPLAY_LENGTH);
    }
}