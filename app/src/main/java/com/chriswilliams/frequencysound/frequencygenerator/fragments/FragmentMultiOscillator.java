package com.chriswilliams.frequencysound.frequencygenerator.fragments;

import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.SeekBar;

import androidx.annotation.NonNull;

import com.chriswilliams.frequencysound.frequencygenerator.R;
import com.chriswilliams.frequencysound.frequencygenerator.audio.Audio;
import com.chriswilliams.frequencysound.frequencygenerator.databinding.FragmentMultiOscillatorBinding;
import com.chriswilliams.frequencysound.frequencygenerator.prefernces.MyPreferences;
import com.lw.internalmarkiting.ui.base.BindingFragment;

import java.util.Locale;

import static com.chriswilliams.frequencysound.frequencygenerator.utils.Constants.MAX_VOLUME;
import static com.chriswilliams.frequencysound.frequencygenerator.utils.Constants.SAWTOOTH;
import static com.chriswilliams.frequencysound.frequencygenerator.utils.Constants.SINE;
import static com.chriswilliams.frequencysound.frequencygenerator.utils.Constants.SQUARE;

public class FragmentMultiOscillator extends BindingFragment<FragmentMultiOscillatorBinding> implements SeekBar.OnSeekBarChangeListener {
    String lastWaveForm1, lastWaveForm2, lastWaveForm3;
    Audio audio1, audio2, audio3;
    SpannableString spannableString;
    private boolean isPlaying1 = true;
    private boolean isPlaying2 = true;
    private boolean isPlaying3 = true;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_multi_oscillator;
    }

    @Override
    protected void onReady() {
        initViews();
        setOnclickListeners();
    }

    @Override
    public void onResume() {
        super.onResume();
        requireActivity().setTitle("Multi-Oscillator");
        initFrequencyBar();
    }

    @Override
    public void onPause() {
        super.onPause();
        binding.playStop1.setImageResource(R.drawable.ic_play);
        binding.playStop2.setImageResource(R.drawable.ic_play);
        binding.playStop3.setImageResource(R.drawable.ic_play);
        audio1.stop();
        audio2.stop();
        audio3.stop();
        isPlaying1 = true;
        isPlaying2 = true;
        isPlaying3 = true;
    }

    private void initFrequencyBar() {
        float max1 = MyPreferences.getMaxFrequency1();
        float min1 = MyPreferences.getMinFrequency1();
        float max2 = MyPreferences.getMaxFrequency2();
        float min2 = MyPreferences.getMinFrequency2();
        float max3 = MyPreferences.getMaxFrequency3();
        float min3 = MyPreferences.getMinFrequency3();
        setProgressBarAndFreqText(1, min1, max1);
        setProgressBarAndFreqText(2, min2, max2);
        setProgressBarAndFreqText(3, min3, max3);
        binding.frequencyBar1.setOnSeekBarChangeListener(this);
        binding.frequencyBar2.setOnSeekBarChangeListener(this);
        binding.frequencyBar3.setOnSeekBarChangeListener(this);
    }

    private void setProgressBarAndFreqText(int which, float min, float max) {
        float freq;
        String sFreq;
        switch (which) {
            case 1:
                if (MyPreferences.getLastFrequency1() > max)
                    freq = max;
                else freq = Math.max(MyPreferences.getLastFrequency1(), min);
                binding.frequencyBar1.setMax((int) (max - min));
                binding.frequencyBar1.setProgress((int) (freq - min));
                sFreq = String.format(Locale.getDefault(), "%1.0f", freq) + " Hz";
                spannableString = new SpannableString(sFreq);
                spannableString.setSpan(new RelativeSizeSpan(1f),
                        0, sFreq.lastIndexOf("H"), 0);
                spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, sFreq.lastIndexOf("H"), 0);
                binding.frequency1.setText(spannableString);
                break;
            case 2:
                if (MyPreferences.getLastFrequency2() > max)
                    freq = max;
                else freq = Math.max(MyPreferences.getLastFrequency2(), min);
                binding.frequencyBar2.setMax((int) (max - min));
                binding.frequencyBar2.setProgress((int) (freq - min));
                sFreq = String.format(Locale.getDefault(), "%1.0f", freq) + " Hz";
                spannableString = new SpannableString(sFreq);
                spannableString.setSpan(new RelativeSizeSpan(1f),
                        0, sFreq.lastIndexOf("H"), 0);
                spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, sFreq.lastIndexOf("H"), 0);
                binding.frequency2.setText(spannableString);
                break;
            case 3:
                if (MyPreferences.getLastFrequency3() > max)
                    freq = max;
                else freq = Math.max(MyPreferences.getLastFrequency3(), min);
                binding.frequencyBar3.setMax((int) (max - min));
                binding.frequencyBar3.setProgress((int) (freq - min));
                sFreq = String.format(Locale.getDefault(), "%1.0f", freq) + " Hz";
                spannableString = new SpannableString(sFreq);
                spannableString.setSpan(new RelativeSizeSpan(1f),
                        0, sFreq.lastIndexOf("H"), 0);
                spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, sFreq.lastIndexOf("H"), 0);
                binding.frequency3.setText(spannableString);
                break;
        }
    }

    private void initViews() {
        audio1 = new Audio();
        audio2 = new Audio();
        audio3 = new Audio();
        audio1.frequency = MyPreferences.getLastFrequency1();
        audio2.frequency = MyPreferences.getLastFrequency2();
        audio3.frequency = MyPreferences.getLastFrequency3();
        audio1.level = MyPreferences.getLastVolume() / (float) MAX_VOLUME;
        audio2.level = MyPreferences.getLastVolume() / (float) MAX_VOLUME;
        audio3.level = MyPreferences.getLastVolume() / (float) MAX_VOLUME;
        initWaveForm1();
        initWaveForm2();
        initWaveForm3();
    }

    private void setOnclickListeners() {
        binding.playStop1.setOnClickListener(view -> {
            if (!isPlaying1) {
                binding.playStop1.setImageResource(R.drawable.ic_play);
                audio1.stop();
                isPlaying1 = true;
            } else {
                binding.playStop1.setImageResource(R.drawable.ic_stop_black_24dp);
                audio1.start();
                isPlaying1 = false;
            }
        });
        binding.lower1.setOnClickListener(view -> frequencyDown(1));
        binding.higher1.setOnClickListener(view -> frequencyUp(1));
        binding.wave1.setOnClickListener(view -> {
            switch (MyPreferences.getLastWaveForm1()) {
                case SINE:
                    binding.wave1.setImageDrawable(getResources().getDrawable(R.drawable.ic_square_wave));
                    audio1.waveform = Audio.SQUARE;
                    MyPreferences.setLastWaveForm1(SQUARE);
                    break;
                case SQUARE:
                    binding.wave1.setImageDrawable(getResources().getDrawable(R.drawable.ic_sawtooth_wave));
                    audio1.waveform = Audio.SAWTOOTH;
                    MyPreferences.setLastWaveForm1(SAWTOOTH);
                    break;
                case SAWTOOTH:
                    binding.wave1.setImageDrawable(getResources().getDrawable(R.drawable.ic_sine_wave));
                    audio1.waveform = Audio.SINE;
                    MyPreferences.setLastWaveForm1(SINE);
                    break;
            }
        });

        binding.playStop2.setOnClickListener(view -> {
            if (!isPlaying2) {
                binding.playStop2.setImageResource(R.drawable.ic_play);
                audio2.stop();
                isPlaying2 = true;
            } else {
                binding.playStop2.setImageResource(R.drawable.ic_stop_black_24dp);
                audio2.start();
                isPlaying2 = false;
            }
        });
        binding.lower2.setOnClickListener(view -> frequencyDown(2));
        binding.higher2.setOnClickListener(view -> frequencyUp(2));
        binding.wave2.setOnClickListener(view -> {
            switch (MyPreferences.getLastWaveForm2()) {
                case SINE:
                    binding.wave2.setImageDrawable(getResources().getDrawable(R.drawable.ic_square_wave));
                    audio2.waveform = Audio.SQUARE;
                    MyPreferences.setLastWaveForm2(SQUARE);
                    break;
                case SQUARE:
                    binding.wave2.setImageDrawable(getResources().getDrawable(R.drawable.ic_sawtooth_wave));
                    audio2.waveform = Audio.SAWTOOTH;
                    MyPreferences.setLastWaveForm2(SAWTOOTH);
                    break;
                case SAWTOOTH:
                    binding.wave2.setImageDrawable(getResources().getDrawable(R.drawable.ic_sine_wave));
                    audio2.waveform = Audio.SINE;
                    MyPreferences.setLastWaveForm2(SINE);
                    break;
            }
        });

        binding.playStop3.setOnClickListener(view -> {
            if (!isPlaying3) {
                binding.playStop3.setImageResource(R.drawable.ic_play);
                audio3.stop();
                isPlaying3 = true;
            } else {
                binding.playStop3.setImageResource(R.drawable.ic_stop_black_24dp);
                audio3.start();
                isPlaying3 = false;
            }
        });
        binding.lower3.setOnClickListener(view -> frequencyDown(3));
        binding.higher3.setOnClickListener(view -> frequencyUp(3));
        binding.wave3.setOnClickListener(view -> {
            switch (MyPreferences.getLastWaveForm3()) {
                case SINE:
                    binding.wave3.setImageDrawable(getResources().getDrawable(R.drawable.ic_square_wave));
                    audio3.waveform = Audio.SQUARE;
                    MyPreferences.setLastWaveForm3(SQUARE);
                    break;
                case SQUARE:
                    binding.wave3.setImageDrawable(getResources().getDrawable(R.drawable.ic_sawtooth_wave));
                    audio3.waveform = Audio.SAWTOOTH;
                    MyPreferences.setLastWaveForm3(SAWTOOTH);
                    break;
                case SAWTOOTH:
                    binding.wave3.setImageDrawable(getResources().getDrawable(R.drawable.ic_sine_wave));
                    audio3.waveform = Audio.SINE;
                    MyPreferences.setLastWaveForm3(SINE);
                    break;
            }
        });
    }

    private void frequencyUp(int which) {
        float step = MyPreferences.getStep();
        int progress;
        switch (which) {
            case 1:
                progress = (int) (binding.frequencyBar1.getProgress() + MyPreferences.getMinFrequency1());
                if ((progress + step) > MyPreferences.getMaxFrequency1()) {
                    binding.frequencyBar1.setProgress((int) (MyPreferences.getMaxFrequency1() - MyPreferences.getMinFrequency1()));
                } else {
                    binding.frequencyBar1.setProgress((int) (progress + step - MyPreferences.getMinFrequency1()));
                }
                break;
            case 2:
                progress = (int) (binding.frequencyBar2.getProgress() + MyPreferences.getMinFrequency2());
                if ((progress + step) > MyPreferences.getMaxFrequency2()) {
                    binding.frequencyBar2.setProgress((int) (MyPreferences.getMaxFrequency2() - MyPreferences.getMinFrequency2()));
                } else {
                    binding.frequencyBar2.setProgress((int) (progress + step - MyPreferences.getMinFrequency2()));
                }
                break;
            case 3:
                progress = (int) (binding.frequencyBar3.getProgress() + MyPreferences.getMinFrequency3());
                if ((progress + step) > MyPreferences.getMaxFrequency3()) {
                    binding.frequencyBar3.setProgress((int) (MyPreferences.getMaxFrequency3() - MyPreferences.getMinFrequency3()));
                } else {
                    binding.frequencyBar3.setProgress((int) (progress + step - MyPreferences.getMinFrequency3()));
                }
                break;
        }
    }

    private void frequencyDown(int which) {
        int progress;
        float step = MyPreferences.getStep();
        switch (which) {
            case 1:
                progress = (int) (binding.frequencyBar1.getProgress() + MyPreferences.getMinFrequency1());
                if ((progress - step) < MyPreferences.getMinFrequency1()) {
                    binding.frequencyBar1.setProgress(0);
                } else {
                    binding.frequencyBar1.setProgress((int) (progress - step - MyPreferences.getMinFrequency1()));
                }
                break;
            case 2:
                progress = (int) (binding.frequencyBar2.getProgress() + MyPreferences.getMinFrequency2());
                if ((progress - step) < MyPreferences.getMinFrequency2()) {
                    binding.frequencyBar2.setProgress(0);
                } else {
                    binding.frequencyBar2.setProgress((int) (progress - step - MyPreferences.getMinFrequency2()));
                }
                break;
            case 3:
                progress = (int) (binding.frequencyBar3.getProgress() + MyPreferences.getMinFrequency3());
                if ((progress - step) < MyPreferences.getMinFrequency3()) {
                    binding.frequencyBar3.setProgress(0);
                } else {
                    binding.frequencyBar3.setProgress((int) (progress - step - MyPreferences.getMinFrequency3()));
                }
                break;
        }
    }

    private void initWaveForm1() {
        lastWaveForm1 = MyPreferences.getLastWaveForm1();
        switch (lastWaveForm1) {
            case SINE:
                binding.wave1.setImageDrawable(getResources().getDrawable(R.drawable.ic_sine_wave));
                audio1.waveform = Audio.SINE;
                break;
            case SQUARE:
                binding.wave1.setImageDrawable(getResources().getDrawable(R.drawable.ic_square_wave));
                audio1.waveform = Audio.SQUARE;
                break;
            case SAWTOOTH:
                binding.wave1.setImageDrawable(getResources().getDrawable(R.drawable.ic_sawtooth_wave));
                audio1.waveform = Audio.SAWTOOTH;
                break;
        }
    }

    private void initWaveForm2() {
        lastWaveForm2 = MyPreferences.getLastWaveForm2();
        switch (lastWaveForm2) {
            case SINE:
                binding.wave2.setImageDrawable(getResources().getDrawable(R.drawable.ic_sine_wave));
                audio2.waveform = Audio.SINE;
                break;
            case SQUARE:
                binding.wave2.setImageDrawable(getResources().getDrawable(R.drawable.ic_square_wave));
                audio2.waveform = Audio.SQUARE;
                break;
            case SAWTOOTH:
                binding.wave2.setImageDrawable(getResources().getDrawable(R.drawable.ic_sawtooth_wave));
                audio2.waveform = Audio.SAWTOOTH;
                break;
        }
    }

    private void initWaveForm3() {
        lastWaveForm3 = MyPreferences.getLastWaveForm3();
        switch (lastWaveForm3) {
            case SINE:
                binding.wave3.setImageDrawable(getResources().getDrawable(R.drawable.ic_sine_wave));
                audio3.waveform = Audio.SINE;
                break;
            case SQUARE:
                binding.wave3.setImageDrawable(getResources().getDrawable(R.drawable.ic_square_wave));
                audio3.waveform = Audio.SQUARE;
                break;
            case SAWTOOTH:
                binding.wave3.setImageDrawable(getResources().getDrawable(R.drawable.ic_sawtooth_wave));
                audio3.waveform = Audio.SAWTOOTH;
                break;
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
        int id = seekBar.getId();
        String freq;
        if (audio1 == null || audio2 == null || audio3 == null)
            return;
        switch (id) {
            case R.id.frequencyBar1:
                progress = (int) (progress + MyPreferences.getMinFrequency1());
                audio1.frequency = progress;
                freq = String.format(Locale.getDefault(), "%1.0f", (float) progress) + " Hz";
                MyPreferences.setLastFrequency1(progress);
                spannableString = new SpannableString(freq);
                spannableString.setSpan(new RelativeSizeSpan(1f),
                        0, freq.lastIndexOf("H"), 0);
                spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, freq.lastIndexOf("H"), 0);
                binding.frequency1.setText(spannableString);
                break;
            case R.id.frequencyBar2:
                progress = (int) (progress + MyPreferences.getMinFrequency2());
                audio2.frequency = progress;
                freq = String.format(Locale.getDefault(), "%1.0f", (float) progress) + " Hz";
                MyPreferences.setLastFrequency2(progress);
                spannableString = new SpannableString(freq);
                spannableString.setSpan(new RelativeSizeSpan(1f),
                        0, freq.lastIndexOf("H"), 0);
                spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, freq.lastIndexOf("H"), 0);
                binding.frequency2.setText(spannableString);
                break;
            case R.id.frequencyBar3:
                progress = (int) (progress + MyPreferences.getMinFrequency3());
                audio3.frequency = progress;
                freq = String.format(Locale.getDefault(), "%1.0f", (float) progress) + " Hz";
                MyPreferences.setLastFrequency3(progress);
                spannableString = new SpannableString(freq);
                spannableString.setSpan(new RelativeSizeSpan(1f),
                        0, freq.lastIndexOf("H"), 0);
                spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, freq.lastIndexOf("H"), 0);
                binding.frequency3.setText(spannableString);
                break;
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
    }
}