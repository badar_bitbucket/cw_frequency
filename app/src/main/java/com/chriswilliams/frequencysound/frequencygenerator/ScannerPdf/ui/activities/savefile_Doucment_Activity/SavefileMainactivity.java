package com.chriswilliams.frequencysound.frequencygenerator.ScannerPdf.ui.activities.savefile_Doucment_Activity;
import android.content.Intent;
import com.chriswilliams.frequencysound.frequencygenerator.R;
import com.chriswilliams.frequencysound.frequencygenerator.databinding.ActivitySavefileMainactivityBinding;
import com.lw.internalmarkiting.ads.InterstitialHelper;
import com.lw.internalmarkiting.ui.activities.BaseActivity;

public class SavefileMainactivity extends BaseActivity<ActivitySavefileMainactivityBinding> {

    @Override
    protected void onReady() {
        handling();
    }


    public void handling() {
        binding.bback.setOnClickListener(v -> onBackPressed());
        binding.docFile.setOnClickListener(v -> {
            InterstitialHelper.showAd(() -> {
                Intent intent = new Intent(getApplicationContext(), Doucment_file_activity.class);
                intent.putExtra("isCard", false);
                startActivity(intent);
            });
        });
        binding.bCard.setOnClickListener(v -> {
            InterstitialHelper.showAd(() -> {
                Intent intent = new Intent(getApplicationContext(), Doucment_file_activity.class);
                intent.putExtra("isCard", true);
                startActivity(intent);
            });

        });
    }

    @Override
    protected int getResId() {
        return R.layout.activity_savefile_mainactivity;
    }
}