package com.chriswilliams.frequencysound.frequencygenerator.ScannerPdf.ui.activities.savefile_Doucment_Activity;

import android.graphics.Bitmap;
import android.net.Uri;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.chriswilliams.frequencysound.frequencygenerator.ScannerPdf.fragmentsviewmodel.DocsViewModel;
import com.chriswilliams.frequencysound.frequencygenerator.ScannerPdf.ui.fragments.ListFragment;
import com.chriswilliams.frequencysound.frequencygenerator.ScannerPdf.ui.fragments.ThumbnailFragment;
import com.lw.internalmarkiting.ui.activities.BaseActivity;
import com.chriswilliams.frequencysound.frequencygenerator.R;
import com.chriswilliams.frequencysound.frequencygenerator.databinding.ActivityDoucmentFileBinding;


import java.util.ArrayList;

public class Doucment_file_activity extends BaseActivity<ActivityDoucmentFileBinding> {
    ArrayList<String> files = null;
    SectionsPagerAdapter adapter;
    ArrayList<Uri> mArrayUri;
    ArrayList<Bitmap> bitmaps;
    boolean isCard;

    @Override
    protected int getResId() {
        return R.layout.activity_doucment_file;
    }

    @Override
    protected void onReady() {
        mArrayUri = new ArrayList<>();
        files = new ArrayList<>();
        bitmaps = new ArrayList<>();
        isCard = getIntent().getBooleanExtra("isCard", false);
        binding.viewPager.setOffscreenPageLimit(3);
        binding.viewPager.setUserInputEnabled(false);
        adapter = new SectionsPagerAdapter(activity, 2);
        binding.viewPager.setAdapter(adapter);
        settingTabLayoutClickListeners();
        DocsViewModel viewModel = new ViewModelProvider(this).get(DocsViewModel.class);
        viewModel.loadFiles(context, isCard);
    }

    private void settingTabLayoutClickListeners() {
        binding.bback.setOnClickListener(v -> onBackPressed());
    }

    private class SectionsPagerAdapter extends FragmentStateAdapter {
        int size;

        public SectionsPagerAdapter(FragmentActivity activity, int size) {
            super(activity);
            this.size = size;
        }

        @Override
        public Fragment createFragment(int position) {
            if (position == 1) {
                return new ListFragment();
            }
            return new ThumbnailFragment();
        }

        @Override
        public int getItemCount() {
            return size;
        }
    }
}