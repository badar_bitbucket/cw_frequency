package com.chriswilliams.frequencysound.frequencygenerator.ScannerPdf.ui.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.FileProvider;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.chriswilliams.frequencysound.frequencygenerator.ScannerPdf.fragmentsviewmodel.DocsViewModel;
import com.lw.internalmarkiting.ui.base.BindingFragment;
import com.chriswilliams.frequencysound.frequencygenerator.R;
import com.chriswilliams.frequencysound.frequencygenerator.databinding.FragmentListBinding;

import java.io.File;
import java.util.List;

public class ListFragment extends BindingFragment<FragmentListBinding> {
    public DocsViewModel viewModel;
    @Override
    protected int getLayoutId() {
        return R.layout.fragment_list;
    }
    @Override
    protected void onReady() {
        viewModel = new ViewModelProvider(requireActivity()).get(DocsViewModel.class);
        viewModel.liveData.observe(this, files -> setAdapter(files));
    }
    private void setAdapter(List<File> files) {
        ListItemAdapter adapter = new ListItemAdapter(files, requireContext());
        binding.listItemRecyclerView.setAdapter(adapter);
    }

    private AlertDialog AskOption(int position) {
        return new AlertDialog.Builder(requireContext())
                .setTitle("Delete")
                .setMessage("Do you want to Delete")
                .setIcon(R.drawable.ic_delete)
                .setPositiveButton("Delete", (dialog, whichButton) -> {
                    //your deleting code
                        viewModel.delete(position);
                    dialog.dismiss();
                })
                .setNegativeButton("cancel", (dialog, which) -> dialog.dismiss())
                .create();
    }

    public class ListItemAdapter extends RecyclerView.Adapter<ListItemAdapter.BaseHolder> {

        List<File> list;
        Context context;
        int HEADER = 0, CONTENT = 1, FOOTER = 2;


        public ListItemAdapter(List<File> list, Context context) {
            this.list = list;
            this.context = context;
        }

        @Override
        public int getItemViewType(int position) {
            if (position == 0) {
                return HEADER;
            } else if (position == getItemCount() - 1) {
                return FOOTER;
            } else {
                return CONTENT;
            }
        }

        @NonNull
        @Override
        public BaseHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = null;
            if (viewType == HEADER) {
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.header_layout, parent, false);
            } else if (viewType == FOOTER) {
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.footer_layout, parent, false);
            } else if (viewType == CONTENT) {
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_recycler_view, parent, false);
                return new ContentHolder(v);
            }
            return new BaseHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull BaseHolder holder, int position) {
            if (holder instanceof ContentHolder) {
                ContentHolder contentHolder = (ContentHolder) holder;
                if (contentHolder.fileName != null) {
                    contentHolder.deleteIcon.setOnClickListener(v -> {
                        AlertDialog diaBox = AskOption(position - 1);
                        diaBox.show();
                    });
                    contentHolder.shareIcon.setOnClickListener(v -> {
                        Intent share = new Intent(Intent.ACTION_SEND);
                        Uri uri = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", list.get(position - 1));
                        share.setType("application/pdf");
                        share.putExtra(Intent.EXTRA_STREAM, uri);
                        context.startActivity(share);
                    });

                    contentHolder.editIcon.setOnClickListener(v -> {
                        AlertDialog.Builder alert = new AlertDialog.Builder(context);
                        final EditText edittext = new EditText(context);
                        alert.setMessage("Enter a new Name");
                        alert.setView(edittext);
                        alert.setPositiveButton("Save", (dialog, whichButton) -> {

                        });

                        alert.setNegativeButton("Cancel", (dialog, whichButton) -> {
                        });

                        alert.show();
                    });
                    contentHolder.itemView.setOnClickListener(v -> {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        Uri uri = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", list.get(position - 1));
                        intent.setDataAndType(uri, "application/pdf");
                        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        context.startActivity(intent);
                    });
                }
            }
        }

        @Override
        public int getItemCount() {
            return list.size() + 2;
        }

        public class BaseHolder extends RecyclerView.ViewHolder {
            BaseHolder(View v) {
                super(v);
            }

        }

        public class ContentHolder extends BaseHolder {
            AppCompatTextView fileName;
            AppCompatImageView shareIcon;
            AppCompatImageView deleteIcon;
            AppCompatImageView editIcon;

            ContentHolder(View v) {
                super(v);
                fileName = v.findViewById(R.id.listFileName);
                shareIcon = v.findViewById(R.id.icShare);
                deleteIcon = v.findViewById(R.id.icDelete);
                editIcon = v.findViewById(R.id.icEdit);

            }
        }


    }
}