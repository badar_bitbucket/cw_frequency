package com.chriswilliams.frequencysound.frequencygenerator.prefernces;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.preference.PreferenceManager;

import com.chriswilliams.frequencysound.frequencygenerator.R;

import static com.chriswilliams.frequencysound.frequencygenerator.utils.Constants.DECIMAL_PRECISION;
import static com.chriswilliams.frequencysound.frequencygenerator.utils.Constants.END_FREQUENCY;
import static com.chriswilliams.frequencysound.frequencygenerator.utils.Constants.LAST_FREQUENCY;
import static com.chriswilliams.frequencysound.frequencygenerator.utils.Constants.LAST_FREQUENCY_1;
import static com.chriswilliams.frequencysound.frequencygenerator.utils.Constants.LAST_FREQUENCY_2;
import static com.chriswilliams.frequencysound.frequencygenerator.utils.Constants.LAST_FREQUENCY_3;
import static com.chriswilliams.frequencysound.frequencygenerator.utils.Constants.LAST_VOLUME;
import static com.chriswilliams.frequencysound.frequencygenerator.utils.Constants.LAST_WAVE_FORM;
import static com.chriswilliams.frequencysound.frequencygenerator.utils.Constants.LAST_WAVE_FORM_1;
import static com.chriswilliams.frequencysound.frequencygenerator.utils.Constants.LAST_WAVE_FORM_2;
import static com.chriswilliams.frequencysound.frequencygenerator.utils.Constants.LAST_WAVE_FORM_3;
import static com.chriswilliams.frequencysound.frequencygenerator.utils.Constants.SINE;
import static com.chriswilliams.frequencysound.frequencygenerator.utils.Constants.SLIDER_RANGE_FROM;
import static com.chriswilliams.frequencysound.frequencygenerator.utils.Constants.SLIDER_RANGE_FROM_1;
import static com.chriswilliams.frequencysound.frequencygenerator.utils.Constants.SLIDER_RANGE_FROM_2;
import static com.chriswilliams.frequencysound.frequencygenerator.utils.Constants.SLIDER_RANGE_FROM_3;
import static com.chriswilliams.frequencysound.frequencygenerator.utils.Constants.SLIDER_RANGE_TO;
import static com.chriswilliams.frequencysound.frequencygenerator.utils.Constants.SLIDER_RANGE_TO_1;
import static com.chriswilliams.frequencysound.frequencygenerator.utils.Constants.SLIDER_RANGE_TO_2;
import static com.chriswilliams.frequencysound.frequencygenerator.utils.Constants.SLIDER_RANGE_TO_3;
import static com.chriswilliams.frequencysound.frequencygenerator.utils.Constants.START_FREQUENCY;
import static com.chriswilliams.frequencysound.frequencygenerator.utils.Constants.STEP;
import static com.chriswilliams.frequencysound.frequencygenerator.utils.Constants.SWEEP_DURATION;
import static com.chriswilliams.frequencysound.frequencygenerator.utils.Constants.SWEEP_REPEAT;
import static com.chriswilliams.frequencysound.frequencygenerator.utils.Constants.THEME;

public class MyPreferences {
    private static SharedPreferences preferences;

    public static float getMinFrequency() {
        return preferences.getFloat(SLIDER_RANGE_FROM, 1.0f);
    }

    public static void setMinFrequency(double frequency) {
        preferences.edit().putFloat(SLIDER_RANGE_FROM, (float) frequency).apply();
    }

    public static float getMaxFrequency() {
        return preferences.getFloat(SLIDER_RANGE_TO, 22000.0f);
    }

    public static void setMaxFrequency(double frequency) {
        preferences.edit().putFloat(SLIDER_RANGE_TO, (float) frequency).apply();
    }

    public static float getLastFrequency() {
        return preferences.getFloat(LAST_FREQUENCY, 11000f);
    }

    public static void setLastFrequency(float f) {
        preferences.edit().putFloat(LAST_FREQUENCY, f).apply();
    }

    public static int getTheme() {
        return preferences.getInt(THEME, R.style.AppTheme);
    }

    public static void setTheme(int i) {
        preferences.edit().putInt(THEME, i).apply();
    }

    public static float getStep() {
        return preferences.getFloat(STEP, 1.0f);
    }

    public static void setStep(float f) {
        preferences.edit().putFloat(STEP, f).apply();
    }

    public static boolean getIsDecimalPrecision() {
        return preferences.getBoolean(DECIMAL_PRECISION, false);
    }

    public static void setIsDecimalPrecision(boolean z) {
        preferences.edit().putBoolean(DECIMAL_PRECISION, z).apply();
    }

    public static String getLastWaveForm() {
        return preferences.getString(LAST_WAVE_FORM, SINE);
    }

    public static void setLastWaveForm(String str) {
        preferences.edit().putString(LAST_WAVE_FORM, str).apply();
    }

    public static String getLastWaveForm1() {
        return preferences.getString(LAST_WAVE_FORM_1, SINE);
    }

    public static void setLastWaveForm1(String str) {
        preferences.edit().putString(LAST_WAVE_FORM_1, str).apply();
    }

    public static String getLastWaveForm2() {
        return preferences.getString(LAST_WAVE_FORM_2, SINE);
    }

    public static void setLastWaveForm2(String str) {
        preferences.edit().putString(LAST_WAVE_FORM_2, str).apply();
    }

    public static String getLastWaveForm3() {
        return preferences.getString(LAST_WAVE_FORM_3, SINE);
    }

    public static void setLastWaveForm3(String str) {
        preferences.edit().putString(LAST_WAVE_FORM_3, str).apply();
    }

    public static int getLastVolume() {
        return preferences.getInt(LAST_VOLUME, 50);
    }

    public static void setLastVolume(int i) {
        preferences.edit().putInt(LAST_VOLUME, i).apply();
    }

    public static void init(Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static float getMinFrequency1() {
        return preferences.getFloat(SLIDER_RANGE_FROM_1, 1.0f);
    }

    public static void setMinFrequency1(double frequency) {
        preferences.edit().putFloat(SLIDER_RANGE_FROM_1, (float) frequency).apply();
    }

    public static float getMinFrequency2() {
        return preferences.getFloat(SLIDER_RANGE_FROM_2, 1.0f);
    }

    public static void setMinFrequency2(double frequency) {
        preferences.edit().putFloat(SLIDER_RANGE_FROM_2, (float) frequency).apply();
    }

    public static float getMinFrequency3() {
        return preferences.getFloat(SLIDER_RANGE_FROM_3, 1.0f);
    }

    public static void setMinFrequency3(double frequency) {
        preferences.edit().putFloat(SLIDER_RANGE_FROM_3, (float) frequency).apply();
    }

    public static float getMaxFrequency1() {
        return preferences.getFloat(SLIDER_RANGE_TO_1, 22000.0f);
    }

    public static void setMaxFrequency1(double frequency) {
        preferences.edit().putFloat(SLIDER_RANGE_TO_1, (float) frequency).apply();
    }

    public static float getMaxFrequency2() {
        return preferences.getFloat(SLIDER_RANGE_TO_2, 22000.0f);
    }

    public static void setMaxFrequency2(double frequency) {
        preferences.edit().putFloat(SLIDER_RANGE_TO_2, (float) frequency).apply();
    }

    public static float getMaxFrequency3() {
        return preferences.getFloat(SLIDER_RANGE_TO_3, 22000.0f);
    }

    public static void setMaxFrequency3(double frequency) {
        preferences.edit().putFloat(SLIDER_RANGE_TO_3, (float) frequency).apply();
    }

    public static float getLastFrequency1() {
        return preferences.getFloat(LAST_FREQUENCY_1, 11000f);
    }

    public static void setLastFrequency1(float f) {
        preferences.edit().putFloat(LAST_FREQUENCY_1, f).apply();
    }

    public static float getLastFrequency2() {
        return preferences.getFloat(LAST_FREQUENCY_2, 11000f);
    }

    public static void setLastFrequency2(float f) {
        preferences.edit().putFloat(LAST_FREQUENCY_2, f).apply();
    }

    public static float getLastFrequency3() {
        return preferences.getFloat(LAST_FREQUENCY_3, 11000f);
    }

    public static void setLastFrequency3(float f) {
        preferences.edit().putFloat(LAST_FREQUENCY_3, f).apply();
    }

    public static long getDuration() {
        return preferences.getLong(SWEEP_DURATION, 10);
    }

    public static void setDuration(long duration) {
        preferences.edit().putLong(SWEEP_DURATION, duration).apply();
    }

    public static float getStartFrequency() {
        return preferences.getFloat(START_FREQUENCY, 200f);
    }

    public static void setStartFrequency(float frequency) {
        preferences.edit().putFloat(START_FREQUENCY, frequency).apply();
    }

    public static float getEndFrequency() {
        return preferences.getFloat(END_FREQUENCY, 800f);
    }

    public static void setEndFrequency(float frequency) {
        preferences.edit().putFloat(END_FREQUENCY, frequency).apply();
    }

    public static boolean getSweepRepeat() {
        return preferences.getBoolean(SWEEP_REPEAT, true);
    }

    public static void setSweepRepeat(boolean isRepeat) {
        preferences.edit().putBoolean(SWEEP_REPEAT, isRepeat).apply();
    }
}