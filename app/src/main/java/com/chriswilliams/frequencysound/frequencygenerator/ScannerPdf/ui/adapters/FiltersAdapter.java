package com.chriswilliams.frequencysound.frequencygenerator.ScannerPdf.ui.adapters;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.chriswilliams.frequencysound.frequencygenerator.databinding.ItemFilterBinding;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import ja.burhanrashid52.photoeditor.PhotoFilter;

public class FiltersAdapter extends RecyclerView.Adapter<FiltersAdapter.ViewHolder> {
    private final OnItemClickListener onItemClickListener;
    private final Context context;
    private final List<Pair<String, PhotoFilter>> mPairList = new ArrayList<>();
    private final String[] filter_Name = {"NONE","DOCUMENTARY","DUE_TONE","GRAY_SCALE","SEPIA","TINT"};
    public FiltersAdapter(Context context, OnItemClickListener onItemClickListener) {
        this.context = context;
        this.onItemClickListener = onItemClickListener;
        setupFilters();
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(ItemFilterBinding.inflate(LayoutInflater.from(context), parent, false));
    }
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind();
    }
    @Override
    public int getItemCount() {
        return mPairList.size();
    }
    private void setupFilters() {
        mPairList.add(new Pair<>("filters/filter0.png", PhotoFilter.NONE));
        mPairList.add(new Pair<>("filters/filter1.png", PhotoFilter.DOCUMENTARY));
        mPairList.add(new Pair<>("filters/filter3.png", PhotoFilter.DUE_TONE));
        mPairList.add(new Pair<>("filters/filter5.png", PhotoFilter.GRAY_SCALE));
        mPairList.add(new Pair<>("filters/filter8.png", PhotoFilter.SEPIA));
        mPairList.add(new Pair<>("filters/filter6.png", PhotoFilter.TINT));
    }
    private Bitmap getBitmapFromAsset(Context context, String strName) {
        AssetManager assetManager = context.getAssets();
        InputStream istr;
        try {
            istr = assetManager.open(strName);
            return BitmapFactory.decodeStream(istr);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public interface OnItemClickListener {
        void onFilterSelected(PhotoFilter photoFilter);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ItemFilterBinding itemFilterBinding;

        public ViewHolder(@NonNull ItemFilterBinding itemView) {
            super(itemView.getRoot());
            itemFilterBinding = itemView;

        }

        void bind() {
            itemFilterBinding.fileName.setText(filter_Name[getAbsoluteAdapterPosition()]);
            itemFilterBinding.getRoot().setOnClickListener(v ->
                    onItemClickListener.onFilterSelected(mPairList.get(getAbsoluteAdapterPosition()).second));
            Glide.with(context).load(getBitmapFromAsset(context, mPairList.get(getAbsoluteAdapterPosition()).first)).into(itemFilterBinding.filterImage);
        }
    }
}
