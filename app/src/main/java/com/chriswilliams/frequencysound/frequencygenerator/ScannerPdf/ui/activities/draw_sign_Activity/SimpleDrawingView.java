package com.chriswilliams.frequencysound.frequencygenerator.ScannerPdf.ui.activities.draw_sign_Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import androidx.annotation.Nullable;
public class SimpleDrawingView extends View {
    Paint clearPaint = new Paint();
    boolean clear;
    int width;
    int height;
    private Paint drawPaint;
    private final Path path = new Path();
    private int signColor = Color.BLACK;

    public SimpleDrawingView(Context context) {
        this(context, null);
    }
    public SimpleDrawingView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SimpleDrawingView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setFocusable(true);
        setFocusableInTouchMode(true);
        setupPaint();
        clearPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
    }

    public void setClear(boolean clear) {
        this.clear = clear;
        invalidate();
    }

    private void setupPaint() {
        drawPaint = new Paint();
        drawPaint.setColor(signColor);
        drawPaint.setAntiAlias(true);
        drawPaint.setStrokeWidth(5);
        drawPaint.setStyle(Paint.Style.STROKE);
        drawPaint.setStrokeJoin(Paint.Join.ROUND);
        drawPaint.setStrokeCap(Paint.Cap.ROUND);
    }
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float pointX = event.getX();
        float pointY = event.getY();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                path.moveTo(pointX, pointY);
                return true;
            case MotionEvent.ACTION_MOVE:
                path.lineTo(pointX, pointY);
                break;
            default:
                return false;
        }
        postInvalidate();
        return true;
    }

    public Bitmap getBitmap() {
        Bitmap bitmap;
        Canvas mCanvas;
        bitmap = Bitmap.createBitmap(width, height, Config.ARGB_8888);
        mCanvas = new Canvas(bitmap);
        drawPaint.setColor(signColor);
        mCanvas.drawPath(path, drawPaint);
        return bitmap;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        width = getWidth();
        height = getHeight();
        if (clear) {
            path.reset();

            clear = false;
        } else {
            drawPaint.setColor(signColor);
            canvas.drawPath(path, drawPaint);
        }
    }

    public void setSignColor(int signColor) {

        this.signColor = signColor;
    }
    public boolean isEmpty()
    {
        return  path.isEmpty();
    }
}

