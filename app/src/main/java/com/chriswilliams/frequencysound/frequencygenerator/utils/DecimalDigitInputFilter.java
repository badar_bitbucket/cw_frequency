package com.chriswilliams.frequencysound.frequencygenerator.utils;

import android.text.InputFilter;
import android.text.Spanned;

import java.util.regex.Pattern;

public class DecimalDigitInputFilter implements InputFilter {

    private Pattern pattern;

    public DecimalDigitInputFilter(int i, int i2) {
        String sb = "[0-9]{0," + (i - 1) + "}+(((?:\\.|,)[0-9]{0," + (i2 - 1) + "})?)||((?:\\.|,))?";
        pattern = Pattern.compile(sb);
    }

    public CharSequence filter(CharSequence charSequence, int i, int i2, Spanned spanned, int i3, int i4) {
        if (!pattern.matcher(spanned).matches()) {
            return "";
        }
        return null;
    }
}
