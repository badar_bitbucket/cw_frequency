package com.chriswilliams.frequencysound.frequencygenerator.ScannerPdf.fragmentsviewmodel;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.chriswilliams.frequencysound.frequencygenerator.R;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DocsViewModel extends ViewModel {
    List<File> backedList = new ArrayList<>();
    public MutableLiveData<List<File>> liveData = new MutableLiveData<>(backedList);

    public void loadFiles(Context context, boolean isCard) {

        String folderName = context.getString(R.string.directory);
        File dirFiles;
        if (isCard)

            dirFiles = new File(context.getFilesDir() + File.separator + folderName + File.separator + context.getString(R.string.directory_card));
        else
            dirFiles = new File(context.getFilesDir() + File.separator + folderName + File.separator + context.getString(R.string.directory_pdf));

        File[] files = dirFiles.listFiles();

        if (files != null) {
            for (File file : files) {
                if (!file.getName().contains(".jpeg")) {
                    backedList.add(file);
                }
            }
            Collections.reverse(backedList);
        }
        liveData.setValue(backedList);
    }

    public void delete(int position) {
        File file = backedList.remove(position);
        file.delete();
        liveData.setValue(backedList);
    }

    public boolean rename(File renameFile, String name, int position) {

        File dest = new File(renameFile.getParent() + "/" + name + ".pdf");

        boolean renamed = renameFile.renameTo(dest);

        backedList.set(position, dest);

        return renamed;
    }


}
