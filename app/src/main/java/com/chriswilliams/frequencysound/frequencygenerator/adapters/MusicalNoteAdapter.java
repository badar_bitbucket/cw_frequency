package com.chriswilliams.frequencysound.frequencygenerator.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.chriswilliams.frequencysound.frequencygenerator.R;
import com.chriswilliams.frequencysound.frequencygenerator.models.MusicalNotes;

import java.util.List;

public class MusicalNoteAdapter extends RecyclerView.Adapter<MusicalNoteAdapter.ViewHolder> {
    Context context;
    List<MusicalNotes> musicalNotes;
    OnItemClickListener onItemClickListener;

    public MusicalNoteAdapter(Context context, List<MusicalNotes> musicalNotes, OnItemClickListener onItemClickListener) {
        this.context = context;
        this.musicalNotes = musicalNotes;
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public MusicalNoteAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(context).inflate(R.layout.item_musical_note, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.note.setText(musicalNotes.get(position).getNote(), TextView.BufferType.SPANNABLE);
        holder.frequency.setText(musicalNotes.get(position).getFrequency() + " Hz");
        holder.itemView.setOnClickListener(v -> onItemClickListener.OnClick(position));
    }

    public interface OnItemClickListener{
        void OnClick(int position);
    }

    @Override
    public int getItemCount() {
        return musicalNotes.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView note, frequency;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            note = itemView.findViewById(R.id.note);
            frequency = itemView.findViewById(R.id.frequency);
        }
    }
}