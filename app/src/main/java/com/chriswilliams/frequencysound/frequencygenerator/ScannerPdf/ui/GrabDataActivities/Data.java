package com.chriswilliams.frequencysound.frequencygenerator.ScannerPdf.ui.GrabDataActivities;
import android.os.Parcel;
import android.os.Parcelable;

public class Data implements Parcelable
{
    String Name;
    String Email;
    String Number;

    public Data() {
    }

    protected Data(Parcel in) {
        Name = in.readString();
        Email = in.readString();
        Number = in.readString();
    }

    public static final Creator<Data> CREATOR = new Creator<Data>() {
        @Override
        public Data createFromParcel(Parcel in) {
            return new Data(in);
        }

        @Override
        public Data[] newArray(int size) {
            return new Data[size];
        }
    };

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getNumber() {
        return Number;
    }

    public void setNumber(String number) {
        Number = number;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(Name);
        parcel.writeString(Email);
        parcel.writeString(Number);
    }
}
