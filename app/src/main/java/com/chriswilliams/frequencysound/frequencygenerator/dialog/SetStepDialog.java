package com.chriswilliams.frequencysound.frequencygenerator.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;
import android.text.method.DigitsKeyListener;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.fragment.app.DialogFragment;

import com.chriswilliams.frequencysound.frequencygenerator.R;
import com.chriswilliams.frequencysound.frequencygenerator.prefernces.MyPreferences;
import com.chriswilliams.frequencysound.frequencygenerator.utils.DecimalDigitInputFilter;
import com.chriswilliams.frequencysound.frequencygenerator.utils.EditTextInputChecker;
import com.chriswilliams.frequencysound.frequencygenerator.utils.Utils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class SetStepDialog extends DialogFragment {

    private DecimalFormat decimalFormat = new DecimalFormat();
    private View view;
    private AppCompatRadioButton radioButtonStep_1;
    private AppCompatRadioButton radioButtonStep_10;
    private AppCompatRadioButton radioButtonStep_100;
    private AppCompatRadioButton radioButtonStepCustom;
    private EditText editTextCustomStep;
    private TextView prefSetStepSummary;

    @Override
    public void onStart() {
        super.onStart();
        try {
            requireDialog().getWindow().setBackgroundDrawableResource(R.drawable.seekbar_bg);
            double d = (double) getResources().getDisplayMetrics().widthPixels;
            requireDialog().getWindow().setLayout((int) (d * 0.9d), -2);
        } catch (Exception unused) {
            view.setBackground(null);
            view.setBackgroundColor(requireActivity().getResources().getColor(R.color.black));
        }
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Dialog dialog = new Dialog(requireContext());
        dialog.requestWindowFeature(1);
        view = requireActivity().getLayoutInflater().inflate(R.layout.dialog_step, null);
        dialog.setContentView(view);
        radioButtonStep_1 = view.findViewById(R.id.radioButton_step_1);
        radioButtonStep_10 = view.findViewById(R.id.radioButton_step_10);
        radioButtonStep_100 = view.findViewById(R.id.radioButton_step_100);
        radioButtonStepCustom = view.findViewById(R.id.radioButton_step_custom);
        TextView buttonSetStep_OK = view.findViewById(R.id.button_set_step_OK);
        TextView buttonSetStep_CANCEL = view.findViewById(R.id.button_set_step_CANCEL);
        editTextCustomStep = view.findViewById(R.id.edittext_custom_step);
        editTextCustomStep.setFilters(new InputFilter[]{new DecimalDigitInputFilter(5, 2)});
        char decimalSeparator = DecimalFormatSymbols.getInstance().getDecimalSeparator();
        EditText editText = editTextCustomStep;
        String sb = "0123456789" + decimalSeparator;
        editText.setKeyListener(DigitsKeyListener.getInstance(sb));
        prefSetStepSummary = requireActivity().findViewById(R.id.pref_set_step_summary);
        decimalFormat.setMaximumFractionDigits(2);
        decimalFormat.setGroupingUsed(false);
        if (!MyPreferences.getIsDecimalPrecision()) {
            editTextCustomStep.setInputType(InputType.TYPE_CLASS_NUMBER);
            decimalFormat.setMaximumFractionDigits(0);
        }
        float k = MyPreferences.getStep();
        if (((double) Math.abs(k - 1.0f)) < 1.0E-5d) {
            radioButtonStep_1.setChecked(true);
        } else if (((double) Math.abs(k - 10.0f)) < 1.0E-5d) {
            radioButtonStep_10.setChecked(true);
        } else if (((double) Math.abs(k - 100.0f)) < 1.0E-5d) {
            radioButtonStep_100.setChecked(true);
        } else {
            radioButtonStepCustom.setChecked(true);
            editTextCustomStep.setText(this.decimalFormat.format((double) MyPreferences.getStep()));
            editTextCustomStep.setVisibility(View.VISIBLE);
        }
        radioButtonStepCustom.setOnClickListener(view1 -> {
            if (editTextCustomStep.getVisibility() == View.INVISIBLE) {
                editTextCustomStep.setVisibility(View.VISIBLE);
            }
        });
        radioButtonStep_1.setOnClickListener(view1 -> setCustomStepEditTextInvisible());
        radioButtonStep_10.setOnClickListener(view1 -> setCustomStepEditTextInvisible());
        radioButtonStep_100.setOnClickListener(view1 -> setCustomStepEditTextInvisible());
        buttonSetStep_OK.setOnClickListener(view2 -> save());
        buttonSetStep_CANCEL.setOnClickListener(view1 -> cancel());
        return dialog;
    }

    private void setCustomStepEditTextInvisible() {
        if (editTextCustomStep.getVisibility() == View.VISIBLE) {
            editTextCustomStep.setVisibility(View.INVISIBLE);
        }
    }

    private void save() {
        if (this.radioButtonStep_1.isChecked()) {
            MyPreferences.setStep(1.0f);
        } else if (this.radioButtonStep_10.isChecked()) {
            MyPreferences.setStep(10.0f);
        } else if (this.radioButtonStep_100.isChecked()) {
            MyPreferences.setStep(100.0f);
        } else if (this.radioButtonStepCustom.isChecked()) {
            EditTextInputChecker checker = new EditTextInputChecker();
            checker.inputChecker(editTextCustomStep.getText().toString());
            if (!checker.isValidInput) {
                this.editTextCustomStep.setError(checker.warning);
                return;
            }
            MyPreferences.setStep(Utils.FormatNumber(editTextCustomStep.getText().toString()));
        }
        String step = decimalFormat.format((double) MyPreferences.getStep()) + " Hz";
        if (prefSetStepSummary != null)
            prefSetStepSummary.setText(step);
        requireDialog().cancel();
    }

    private void cancel() {
        requireDialog().cancel();
    }

}