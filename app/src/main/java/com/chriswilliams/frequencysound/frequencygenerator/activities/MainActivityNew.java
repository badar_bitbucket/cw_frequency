package com.chriswilliams.frequencysound.frequencygenerator.activities;
import static com.chriswilliams.frequencysound.frequencygenerator.utils.Constants.LOCK;
import static com.lw.internalmarkiting.ads.InterstitialHelper.showAd;
import static com.lw.internalmarkiting.ui.Common.CHRIS_WILLIAMS_APPS;
import static com.lw.internalmarkiting.ui.Common.moreApps;
import static com.lw.internalmarkiting.ui.Common.rateUs;
import static com.lw.internalmarkiting.ui.Common.share;
import static com.lw.internalmarkiting.ui.Common.showPolicy;
import static com.lw.internalmarkiting.ui.activities.ExitActivity.CODE_EXIT;
import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.view.GravityCompat;
import com.chriswilliams.frequencysound.frequencygenerator.R;
import com.chriswilliams.frequencysound.frequencygenerator.ScannerPdf.Scanner_MainActivity.scannerMainActivity;
import com.chriswilliams.frequencysound.frequencygenerator.databinding.ActivityMainNewBinding;
import com.lw.internalmarkiting.ui.activities.BaseActivity;
import com.lw.internalmarkiting .ui.activities.ExitActivity;
import com.lw.internalmarkiting.ui.view.StartPromoDialog;
public class MainActivityNew extends BaseActivity<ActivityMainNewBinding> {

    PowerManager.WakeLock wakeLock;
    boolean doubleBackToExitPressedOnce = false;


    public static void start(Activity activity) {
        Intent intent = new Intent(activity, MainActivityNew.class);
        activity.startActivity(intent);
    }

    @Override
    protected int getResId() {
        return R.layout.activity_main_new;
    }

    @Override
    protected void onReady() {
        PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
        if (pm != null) {
            wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, LOCK);
        }
        setSupportActionBar(binding.appBarMain.toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, binding.drawerLayout, binding.appBarMain.toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        binding.drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        setOnClickListeners();
        StartPromoDialog.show(activity);
    }

    private void setOnClickListeners() {
        binding.navQuit.setOnClickListener(view -> {
            openCloseActivity();
            binding.drawerLayout.closeDrawer(GravityCompat.START);
        });
        binding.navShare.setOnClickListener(view -> {
            String message = "Install this amazing app!!!";
            share(activity, message);
            binding.drawerLayout.closeDrawer(GravityCompat.START);
        });
        binding.RateUs.setOnClickListener(view -> {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
            rateUs(context);
        });
        binding.Settings.setOnClickListener(view ->
                showAd(() -> {
            SettingsActivity.start(activity);
            binding.drawerLayout.closeDrawer(GravityCompat.START);
        }));
        binding.Privacy.setOnClickListener(v -> showAd(() -> {
            showPolicy(context);
            binding.drawerLayout.closeDrawer(GravityCompat.START);
        }));
        binding.MoreApps.setOnClickListener(v -> {
            moreApps(context, CHRIS_WILLIAMS_APPS);
            binding.drawerLayout.closeDrawer(GravityCompat.START);
        });
        binding.appBarMain.contentMain.frequencyId.setOnClickListener(v -> FrequencyGeneratorActivity.start(context));
        binding.appBarMain.contentMain.ScannerId.setOnClickListener(v -> {
            Intent scannerIntent = new Intent(MainActivityNew.this, scannerMainActivity.class);
            startActivity(scannerIntent);

        });
        binding.Douc.setOnClickListener(v -> {
            Intent scannerIntent = new Intent(MainActivityNew.this, scannerMainActivity.class);
            startActivity(scannerIntent);
        });
        binding.Frq.setOnClickListener(v -> FrequencyGeneratorActivity.start(context));
    }

    private void openCloseActivity() {
        ExitActivity.startActivityForResult(activity, CODE_EXIT);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {

        if (doubleBackToExitPressedOnce) {
            finishAffinity();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast toast = Toast.makeText(getApplicationContext(),
                "Please click Back again to exit",
                Toast.LENGTH_SHORT);
        toast.show();
        new Handler(Looper.getMainLooper()).postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

}