package com.chriswilliams.frequencysound.frequencygenerator.ScannerPdf.ui.activities.othersActivity;
import static com.chriswilliams.frequencysound.frequencygenerator.ScannerPdf.ui.activities.othersActivity.EditingActivity.EXTRA_URI_PATH;
import android.content.Intent;
import android.net.Uri;
import androidx.core.content.FileProvider;
import com.chriswilliams.frequencysound.frequencygenerator.R;
import com.chriswilliams.frequencysound.frequencygenerator.ScannerPdf.utils.ScannerConstants;
import com.chriswilliams.frequencysound.frequencygenerator.activities.MainActivityNew;
import com.chriswilliams.frequencysound.frequencygenerator.databinding.ActivitySuccessBinding;
import com.lw.internalmarkiting.ui.activities.BaseActivity;
import java.io.File;
public class SuccessActivity extends BaseActivity<ActivitySuccessBinding> {
    private String fileUri;

    @Override
    public void onReady() {
        fileUri = getIntent().getStringExtra(EXTRA_URI_PATH);
        binding.fileName.setText(ScannerConstants.fileName);
        binding.openButton.setOnClickListener(v -> {
            if (getIntent().getIntExtra("jpeg", 4) == 1) {
                File file = new File(fileUri);
                Intent intent = new Intent(Intent.ACTION_VIEW);
                Uri uri = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", file);
                intent.setDataAndType(uri, "application/pdf");
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(intent);
            } else {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.parse(fileUri), "image/*");
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(intent);
            }
        });
        binding.shareButton.setOnClickListener(v -> {

            if (getIntent().getIntExtra("jpeg", 4) != 1) {
                File file = new File(fileUri);
                Intent share = new Intent(Intent.ACTION_SEND);
                Uri uri = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", file);
                share.setType("application/pdf");
                share.putExtra(Intent.EXTRA_STREAM, uri);
                activity.startActivity(share);
            } else {
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("application/image");
                share.putExtra(Intent.EXTRA_STREAM, Uri.parse(fileUri));
                activity.startActivity(share);
            }
        });
        binding.backToHome.setOnClickListener(v -> {
            finish();
            startActivity(new Intent(SuccessActivity.this, MainActivityNew.class));
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected int getResId() {
        return R.layout.activity_success;
    }


}