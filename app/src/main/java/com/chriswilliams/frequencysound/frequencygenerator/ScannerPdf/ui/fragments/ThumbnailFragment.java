package com.chriswilliams.frequencysound.frequencygenerator.ScannerPdf.ui.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.PopupMenu;
import androidx.core.content.FileProvider;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chriswilliams.frequencysound.frequencygenerator.ScannerPdf.fragmentsviewmodel.DocsViewModel;
import com.lw.internalmarkiting.ui.base.BindingFragment;
import com.chriswilliams.frequencysound.frequencygenerator.R;
import com.chriswilliams.frequencysound.frequencygenerator.databinding.FragmentThumbnailBinding;

import java.io.File;
import java.util.List;

public class ThumbnailFragment extends BindingFragment<FragmentThumbnailBinding> {
    DocsViewModel viewModel;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_thumbnail;
    }

    @Override
    protected void onReady() {
        viewModel = new ViewModelProvider(requireActivity()).get(DocsViewModel.class);
        viewModel.liveData.observe(this, this::setAdapter);
    }
    private void setAdapter(List<File> files) {
        ThumbnailItemAdapter adapter = new ThumbnailItemAdapter(files, requireContext());
        GridLayoutManager mLayoutManager = new GridLayoutManager(requireContext(), 2);
        mLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (position == 0 || position == adapter.getItemCount() - 1) {
                    return 2;
                } else {
                    return 1;
                }
            }
        });
        binding.rvThumbnail.setLayoutManager(mLayoutManager);
        binding.rvThumbnail.setAdapter(adapter);
    }

    private AlertDialog AskOption(int position) {
        return new AlertDialog.Builder(requireContext())
                // set message, title, and icon
                .setTitle("Delete!")
                .setMessage("Do you want to delete?")
                .setPositiveButton("delete", (dialog, whichButton) -> {
                    //your deleting code
                    viewModel.delete(position);
                    dialog.dismiss();
                })
                .setNegativeButton("cancel", (dialog, which) -> dialog.dismiss())
                .create();
    }

    public class ThumbnailItemAdapter extends RecyclerView.Adapter<ThumbnailItemAdapter.BaseHolder> {
        List<File> list;
        Context context;
        int HEADER = 0, CONTENT = 1, FOOTER = 2;

        public ThumbnailItemAdapter(List<File> list, Context context) {
            this.list = list;
            this.context = context;
        }

        void showPopupMenu(ContentHolder contentHolder, int position) {
            PopupMenu popup = new PopupMenu(context, contentHolder.menu);
            popup.setOnMenuItemClickListener(item -> {
                if (item.getItemId() == R.id.delete) {
                    AlertDialog diaBox = AskOption(position - 1);
                    diaBox.show();

                } else if (item.getItemId() == R.id.edit) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(context);
                    final EditText edittext = new EditText(context);
                    alert.setMessage("Enter a new Name");
                    alert.setView(edittext);
                    alert.setPositiveButton("Save", (dialog, whichButton) -> {
                        String newName = edittext.getText().toString().trim();
                        if (newName.isEmpty()) {
                            Toast.makeText(context, "Please Enter New File Name!", Toast.LENGTH_SHORT).show();
                        } else {
                            boolean renamed = viewModel.rename(list.get(position - 1), newName, position - 1);
                            if (renamed)
                                contentHolder.fileName.setText(list.get(position - 1).getName());
                        }
                    });
                    alert.setNegativeButton("Cancel", (dialog, whichButton) -> {
                    });

                    alert.show();
                } else if (item.getItemId() == R.id.share) {
                    Intent share = new Intent(Intent.ACTION_SEND);
                    Uri uri = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", list.get(position - 1));
                    share.setType("application/pdf");
                    share.putExtra(Intent.EXTRA_STREAM, uri);
                    context.startActivity(share);
                } else {
                    return false;
                }
                return true;
            });
            MenuInflater inflater = popup.getMenuInflater();
            inflater.inflate(R.menu.rv_menu, popup.getMenu());
            popup.show();
        }

        @Override
        public int getItemViewType(int position) {
            if (position == 0) {
                return HEADER;
            } else if (position == getItemCount() - 1) {
                return FOOTER;
            } else {
                return CONTENT;
            }
        }

        @NonNull
        @Override
        public BaseHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = null;
            if (viewType == HEADER) {
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.header_layout, parent, false);
            } else if (viewType == FOOTER) {
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.footer_layout, parent, false);
            } else if (viewType == CONTENT) {
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.thumbnail_item_recycler_view, parent, false);
                return new ContentHolder(v);
            }
            return new BaseHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull BaseHolder holder, int position) {
            if (holder instanceof ContentHolder) {
                ContentHolder contentHolder = (ContentHolder) holder;
                if (contentHolder.fileName != null) {
                    contentHolder.fileName.setText(list.get(position - 1).getName());
                    contentHolder.itemView.setOnClickListener(v -> {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        Uri uri = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", list.get(position - 1));
                        intent.setDataAndType(uri, "application/pdf");
                        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        context.startActivity(intent);
                    });
                    contentHolder.menu.setOnClickListener(v -> showPopupMenu(contentHolder, position));

                }

            }

        }


        @Override
        public int getItemCount() {
            return list.size() + 2;
        }

        public class BaseHolder extends RecyclerView.ViewHolder {
            BaseHolder(View v) {
                super(v);
            }

        }

        public class ContentHolder extends BaseHolder {
            AppCompatTextView fileName;
            AppCompatImageView menu;

            ContentHolder(View v) {
                super(v);
                fileName = v.findViewById(R.id.thumbnailFileName);
                menu = v.findViewById(R.id.menu);
            }
        }


    }

}